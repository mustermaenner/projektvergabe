# Getting Started with Create React App

A project like this can be bootstrapped with `yarn create vite`.

It uses [React TypeScript][] with the [airbnb styleguide][].

## Available Targets

In the project directory, you can run:

### `npm run start`

Use Vite to run the app in dev mode.

### `npm run bild`

Build the app as static content with vite.

### `npm run lint`

Test if code is formated based on [airbnb styleguide][] and [.prettierrc.json](./.prettierrc.json)

### `npm run fix`

Try to autofix eslint issues.

### `npm run format`

Autoformat code with [https://prettier.io/](prettier) based on [.prettierrc.json](./.prettierrc.json).  
This will be checked by `npm run lint`.

[airbnb styleguide]: https://airbnb.io/javascript/react/
[React TypeScript]:  https://www.typescriptlang.org/docs/handbook/react.html
