// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#f49100',
      contrastText: '#050505',
    },
    secondary: {
      main: '#f49100',
      contrastText: '#ffffff',
    },
    error: {
      main: '#8b0404',
      contrastText: '#ffffff',
    },
    warning: {
      main: '#050505',
      contrastText: '#050505',
    },
    success: {
      main: '#1a4803',
      contrastText: '#ffffff',
    },
  },
});
export default theme;
