// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

export default class AddEventDto {
  name: String;

  description: String;

  openUntil: Date;

  password: String;

  profs: Number[];

  constructor(name: String, description: String, openUntil: Date, password: String, profs: Number[]) {
    this.name = name;
    this.description = description;
    this.openUntil = openUntil;
    this.password = password;
    this.profs = profs;
  }
}
