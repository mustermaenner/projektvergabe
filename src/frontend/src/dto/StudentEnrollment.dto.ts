// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

export default class StudentEnrollmentDto {
  firstName: string;

  lastName: string;

  mail: string;

  matriculationNr: number;

  studentIdentifier: string;

  eventId?: number;

  priorities?: Array<{ projectId: number; priority: number }>;

  contract: boolean;

  street: string;

  city: string;

  cityCode: number;

  country: string;

  finalProject: number;

  uuidForChanges: string;

  semester: number;

  constructor(
    firstName: string,
    lastName: string,
    mail: string,
    matriculationNr: number,
    studentIdentifier: string,
    eventId: number,
    priorities: Array<{ projectId: number; priority: number }>,
    contract: boolean,
    street: string,
    city: string,
    cityCode: number,
    country: string,
    finalProject: number,
    uuidForChanges: string,
    semester: number,
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.mail = mail;
    this.matriculationNr = matriculationNr;
    this.studentIdentifier = studentIdentifier;
    this.eventId = eventId;
    this.priorities = priorities;
    this.contract = contract;
    this.street = street;
    this.city = city;
    this.cityCode = cityCode;
    this.country = country;
    this.finalProject = finalProject;
    this.uuidForChanges = uuidForChanges;
    this.semester = semester;
  }
}
