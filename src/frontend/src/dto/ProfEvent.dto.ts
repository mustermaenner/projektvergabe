// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

export default class ProfEvent {
  id: number;

  name: String;

  description: String;

  openUntil: Date;

  password: String;

  profs: Number[];

  constructor(id: number, name: String, description: String, openUntil: Date, password: String, profs: Number[]) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.openUntil = openUntil;
    this.password = password;
    this.profs = profs;
  }
}
