// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

export default class ProfDto {
  id: number;

  name: string;

  email: string;

  roles: string;

  constructor(id: number, name: string, email: string, roles: string) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.roles = roles;
  }
}
