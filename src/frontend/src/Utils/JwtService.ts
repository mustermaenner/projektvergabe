// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import jwtDecode, { JwtPayload } from 'jwt-decode';

function jwtService(): {
  getDecodedAccessToken: (token: string) => any;
  getToken: () => string;
  checkToken: () => boolean;
  getEndpoint: (resource: string) => string;
  getHeader: () => HeadersInit | undefined;
  getBasicAuth: () => HeadersInit | undefined;
  getBearerAuth: () => string;
  renewToken: () => Promise<string | undefined>;
} {
  const BASE_URL = 'http://localhost:8080/api/v1';

  function getEndpoint(resource: string): string {
    return `${BASE_URL}/${resource}`;
  }

  function logout() {
    localStorage.removeItem('token');
    window.location.replace('/login');
  }

  function getToken(): string {
    const savedToken = localStorage.getItem('token');
    if (!savedToken) {
      return '';
    }
    return savedToken;
  }

  function renewToken(): Promise<string | undefined> {
    const headers = new Headers();
    headers.append('accept', 'application/json');
    headers.append('Authorization', `Bearer ${getToken()}`);
    return fetch(getEndpoint('profs/renewtoken'), {
      method: 'GET',
      headers,
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Token renewal failed');
        }
        return response.json();
      })
      .then(({ token }) => {
        if (token) {
          localStorage.setItem('token', token);
          return token;
        }
        return '';
      })
      .catch((error) => {
        if (error.message === 'Token renewal failed') {
          logout();
        }
        throw new Error(error);
      });
  }

  function getDecodedAccessToken(token: string): JwtPayload | undefined {
    try {
      return jwtDecode<JwtPayload>(token);
    } catch (Error) {
      // eslint-disable-next-line no-console
      console.error('Error could not decode jwt: %s', Error);
    }
    return undefined;
  }

  function checkToken(): boolean {
    let isExpired = false;
    const storedToken = localStorage.getItem('token');
    if (storedToken) {
      const decodedToken = getDecodedAccessToken(storedToken);
      if (decodedToken && decodedToken.exp) {
        const dateNow = new Date();
        const now: number = dateNow.getTime();
        const exp: number = Number(decodedToken.exp);
        if (exp < now) {
          renewToken().then(() => {
            isExpired = false;
          });
          isExpired = true;
        }
      }
    }
    return isExpired;
  }

  function getBearerAuth(): string {
    if (checkToken()) {
      return `Bearer ${getToken()}`;
    }
    return '';
  }

  function getBasicAuth(): HeadersInit {
    if (checkToken()) {
      const result: HeadersInit = new Headers();
      result.append('Content-Type', 'application/json');
      result.append('Authorization', `Basic ${localStorage.getItem('token')}`);
      return result;
    }
    // Remove the invalid token from local storage and return a new instance of Headers
    localStorage.removeItem('token');
    window.history.pushState({}, 'App', '/');
    window.location.reload();
    return new Headers();
  }

  function getHeader(): HeadersInit {
    if (checkToken()) {
      const result: HeadersInit = new Headers();
      result.append('Content-Type', 'application/json');
      result.append('accept', ' */*');
      result.append('Authorization', `Bearer ${localStorage.getItem('token')}`);

      return result;
    }

    // Remove the invalid token from local storage and redirect to the login page
    localStorage.removeItem('token');
    return new Headers();
  }

  return {
    getDecodedAccessToken,
    getToken,
    checkToken,
    getEndpoint,
    getHeader,
    getBasicAuth,
    getBearerAuth,
    renewToken,
  };
}

export default jwtService;
