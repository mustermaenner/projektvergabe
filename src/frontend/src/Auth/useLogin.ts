// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { ChangeEvent, FormEvent, useContext, useState } from 'react';
import { encode } from 'base-64';
import { useNavigate } from 'react-router';
import { TokenContext } from '../App';
import jwtService from '../Utils/JwtService';

// eslint-disable-next-line @typescript-eslint/naming-convention
type login = {
  username: string;
  password: string;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
async function doSignIn(username: string, password: string): Promise<any> {
  const credentials = encode(`${username}:${password}`);
  const URL = jwtService().getEndpoint('profs/login');
  const response = await fetch(URL, {
    method: 'GET',
    headers: {
      Authorization: `Basic ${credentials}`,
    },
  });
  return response;
}

function handleError(errorMsg: string): Map<string, string> {
  const resultMap = new Map<string, string>();
  resultMap.set('username', '');
  resultMap.set('password', errorMsg);
  return resultMap;
}

function hasValues(credentials: login): Map<string, string> {
  const resultMap = new Map<string, string>();
  if (credentials.username.length === 0) {
    resultMap.set('username', 'Username can not be empty');
  }
  if (credentials.password.length === 0) {
    resultMap.set('password', 'Password can not be empty');
  }
  return resultMap;
}

function useLogin() {
  const [login, setLogin] = useState({ username: '', password: '' });
  const [errorMsg, setErrorMsg] = useState(new Map<string, string>());
  const setToken = useContext(TokenContext)[1];
  const navigator = useNavigate();

  async function doLogin(event: FormEvent) {
    event.preventDefault();
    const errors = hasValues(login);
    if (errors.size !== 0) {
      setErrorMsg(errors);
      return;
    }
    const response = await doSignIn(login.username, login.password);
    if (response.ok) {
      const data = await response.text();
      const jsonData = await JSON.parse(data);
      localStorage.setItem('token', jsonData.token);
      setToken(jsonData.token);
      navigator('/overview');
    } else {
      const errorResponse = handleError('wrong credentials');
      setErrorMsg(errorResponse);
    }
  }

  function handleChange(event: ChangeEvent<HTMLInputElement>) {
    setLogin((prevLogin) => ({
      ...prevLogin,
      [event.target.name]: event.target.value,
    }));
  }

  return {
    doLogin,
    errorMsg,
    login,
    handleChange,
  };
}

export default useLogin;
