// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { useEffect } from 'react';
import { Container } from '@mui/material';
import * as React from 'react';
import { useNavigate } from 'react-router';
import jwtService from '../Utils/JwtService';
import MenuAppBarProf from '../Components/NavBar/MenuAppBarProf';

function Logout() {
  localStorage.removeItem('token');
  const navigate = useNavigate();

  useEffect(() => {
    if (!jwtService().checkToken()) {
      navigate('/login');
    }
  }, [navigate]);

  return (
    <Container className="p-5 mb-4 bg-light rounded-4">
      <MenuAppBarProf />
    </Container>
  );
}

export default Logout;
