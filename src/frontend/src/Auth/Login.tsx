// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { Button, FormControl, Stack } from '@mui/material';
import Box from '@mui/material/Box';
import * as React from 'react';
import { useEffect } from 'react';
import { useNavigate } from 'react-router';
import useLogin from './useLogin';
import Input from '../Components/Input';
import jwtService from '../Utils/JwtService';
import './form.css';
import MenuAppBar from '../Components/NavBar/MenuAppBar';

function Login() {
  const { doLogin, errorMsg, login, handleChange } = useLogin();
  const navigate = useNavigate();

  useEffect(() => {
    if (jwtService().checkToken()) {
      navigate('/overview');
    }
  }, []);

  return (
    <>
      <MenuAppBar />

      <form onSubmit={(event) => doLogin(event)}>
        <Box sx={{ p: 0 }} marginTop={5} style={{ width: '100%', top: '50px' }}>
          <Stack spacing={4} justifyContent="center" alignItems="center" alignContent="center">
            <img src="../thLogo.svg" alt="logo" width={350} height={150} />

            <FormControl style={{ maxWidth: '300px', width: '100%' }}>
              <Input
                style={{ maxWidth: '300px', width: '100%' }}
                label="Username"
                value={login.username}
                onChange={handleChange}
                name="username"
                type="text"
                error={errorMsg.get('username')}
              />
            </FormControl>
            <div style={{ maxWidth: '300px', width: '100%' }}>
              <Input
                style={{ maxWidth: '300px', width: '100%' }}
                label="Password"
                value={login.password}
                onChange={handleChange}
                name="password"
                type="password"
                error={errorMsg.get('password')}
              />
            </div>

            <Button color="secondary" style={{ maxWidth: '300px', width: '100%' }} variant="contained" type="submit">
              login
            </Button>
          </Stack>
        </Box>
      </form>
    </>
  );
}

export default Login;
