// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { ChangeEvent, FormEvent, useState } from 'react';
import { useNavigate } from 'react-router';
import jwtService from '../../Utils/JwtService';

type Update = {
  profId: string;
  eventId: string;
};

async function updateProf(profId: string, eventId: string) {
  const URL = jwtService().getEndpoint(`profs/${profId}/events/${eventId}`);
  const header = jwtService().getHeader();
  const requestOptions = {
    method: 'PUT',
    headers: header,
  };
  const response = await fetch(URL, requestOptions);
  return response;
}
function hasValues(information: Update): Map<string, string> {
  const resultMap = new Map<string, string>();

  if (information.profId.length === 0) {
    resultMap.set('profId', 'can not be empty');
  }
  if (information.eventId.length === 0) {
    resultMap.set('eventId', 'can not be empty');
  }
  return resultMap;
}

function validate(information: Update): Map<string, string> {
  const resultMap = hasValues(information);
  if (resultMap.size !== 0) {
    return resultMap;
  }
  if (information.profId.length < 3 || information.profId.length > 100) {
    resultMap.set('password', 'The password must be between 4 and 100 characters long.');
  }
  if (information.eventId.length < 3 || information.eventId.length > 100) {
    resultMap.set('password', 'The password must be between 4 and 100 characters long.');
  }
  return resultMap;
}

function handleError(error: string): Map<string, string> {
  const resultMap = new Map<string, string>();
  if (error.includes('password')) {
    resultMap.set('password', error);
  }
  if (error.includes('email')) {
    resultMap.set('email', error);
  }
  return resultMap;
}

function useUpdateAccount() {
  const [update, setUpdate] = useState({ profId: '', eventId: '' });
  const [errorUpdate, setError] = useState(new Map<string, string>());
  const navigate = useNavigate();

  async function updateUser(event: FormEvent) {
    event.preventDefault();
    const errors = validate(update);
    setError(errors);
    if (errors.size !== 0) {
      return;
    }
    const response = await updateProf(update.profId, update.eventId);
    if (response.ok) {
      localStorage.removeItem('token');
      navigate('/login');
    } else {
      const errorResponse = handleError("Can't Update password");
      setError(errorResponse);
    }
  }

  function handleChangeUpdate(event: ChangeEvent<HTMLInputElement>) {
    setUpdate((prevRegister) => ({
      ...prevRegister,
      [event.target.name]: event.target.value,
    }));
  }

  return { updateUser, handleChangeUpdate, update, errorUpdate };
}

export default useUpdateAccount;
