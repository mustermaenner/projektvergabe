// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { ChangeEvent, FormEvent, useState } from 'react';
import validator from 'validator';
import jwtService from '../../Utils/JwtService';

type Register = {
  name: string;
  mail: string;
  password: string;
  passwordConfirm: string;
  role: string;
};

async function doSignUp(name: string, mail: string, password: string, role: string): Promise<Response> {
  const URL = jwtService().getEndpoint('profs');
  const body = JSON.stringify({ name, mail, password, role });
  const headers = jwtService().getHeader();

  if (!headers) {
    throw new Error('Could not get headers for API request');
  }

  const response = await fetch(URL, {
    method: 'POST',
    headers,
    body,
  });
  return response;
}

function hasValues(information: Register): Map<string, string> {
  const resultMap = new Map<string, string>();
  if (information.name.length === 0) {
    resultMap.set('username', 'Username can not be empty');
  }
  if (information.mail.length === 0) {
    resultMap.set('email', 'Email can not be empty');
  }
  if (information.password.length === 0) {
    resultMap.set('password', 'Password can not be empty');
  }
  if (information.passwordConfirm.length === 0) {
    resultMap.set('passwordConfirm', 'Confirm password can not be empty');
  }
  return resultMap;
}

function validate(information: Register): Map<string, string> {
  const resultMap = hasValues(information);
  if (resultMap.size !== 0) {
    return resultMap;
  }
  if (information.name.length < 3 || information.name.length > 20) {
    resultMap.set('username', 'The username must be between 3 and 20 characters long');
  }
  if (!validator.isEmail(information.mail)) {
    resultMap.set('email', 'The email address specified is invalid. Example format: me@example.org');
  }
  if (information.password.length < 8 || information.password.length > 100) {
    resultMap.set('password', 'The password must be between 8 and 100 characters long. The more the better');
  } else if (information.password !== information.passwordConfirm) {
    resultMap.set('passwordConfirm', "Those passwords don't match! Try again.");
  }
  return resultMap;
}

function handleError(error: string): Map<string, string> {
  const resultMap = new Map<string, string>();
  if (error.includes('username') || error.includes('user')) {
    resultMap.set('username', error);
  }
  if (error.includes('mail')) {
    resultMap.set('mail', error);
  }
  return resultMap;
}

function useRegister() {
  const [register, setRegister] = useState({ name: '', mail: '', password: '', passwordConfirm: '', role: '' });
  const [errorMsg, setErrorMsg] = useState(new Map<string, string>());
  const [successOpen, setSuccessOpen] = useState(false);
  const [errorOpen, setErrorOpen] = useState(false);

  async function doRegister(event: FormEvent) {
    event.preventDefault();
    const errors = validate(register);
    setErrorMsg(errors);
    if (errors.size !== 0) {
      return;
    }
    const response = await doSignUp(register.name, register.mail, register.password, register.role);
    if (response.ok) {
      setSuccessOpen(true);
    } else {
      const error = await response.text();
      setErrorMsg(handleError(error));
      setErrorOpen(true);
    }
  }

  function handleChange(event: ChangeEvent<HTMLInputElement>) {
    setRegister((prevRegister) => ({
      ...prevRegister,
      [event.target.name]: event.target.value,
    }));
  }

  return {
    doRegister,
    errorMsg,
    setErrorMsg,
    register,
    handleChange,
    successOpen,
    setSuccessOpen,
    errorOpen,
    setErrorOpen,
  };
}

export default useRegister;
