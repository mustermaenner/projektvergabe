// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import { ReactElement, useCallback, useEffect } from 'react';
import '../form.css';
import { Alert, Button, CardContent, Divider, Grid, Snackbar } from '@mui/material';
import { useNavigate } from 'react-router';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import useRegister from './useRegister';
import jwtService from '../../Utils/JwtService';
import Input from '../../Components/Input';
import MenuAppBarProf from '../../Components/NavBar/MenuAppBarProf';

function Register(): ReactElement {
  const {
    register,
    handleChange,
    doRegister,
    errorMsg,
    setErrorMsg,
    successOpen,
    errorOpen,
    setSuccessOpen,
    setErrorOpen,
  } = useRegister();
  const navigate = useNavigate();

  const handleCloseSuccess = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
    navigate('/account');
  }, []);

  const handleCloseError = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setErrorOpen(false);
  }, []);

  useEffect(() => {
    if (!jwtService().checkToken()) {
      navigate('/login');
    }
  }, []);

  return (
    <>
      <MenuAppBarProf />
      <Card
        style={{
          maxWidth: 500,
          padding: '20px 5px',
          margin: '0 auto',
          marginTop: 20,
        }}
      >
        <CardContent>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography gutterBottom variant="h5" component="div">
                Create account:
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <Typography variant="body2" color="textSecondary" component="p" gutterBottom>
                The fields marked with * must be filled
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <form className="register" onSubmit={(event) => doRegister(event)}>
                <Grid container spacing={1}>
                  <Grid item xs={12}>
                    <Input
                      label="Username"
                      style={{}}
                      value={register.name}
                      onChange={handleChange}
                      name="name"
                      type="text"
                      error={errorMsg.get('name')}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Input
                      label="Email"
                      style={{}}
                      value={register.mail}
                      onChange={handleChange}
                      name="mail"
                      type="text"
                      error={errorMsg.get('mail')}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Input
                      label="Password"
                      style={{}}
                      value={register.password}
                      onChange={handleChange}
                      name="password"
                      type="password"
                      error={errorMsg.get('password')}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Input
                      label="Confirm Password"
                      style={{}}
                      value={register.passwordConfirm}
                      onChange={handleChange}
                      name="passwordConfirm"
                      type="password"
                      error={errorMsg.get('passwordConfirm')}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Input
                      label="Role"
                      style={{}}
                      value={register.role}
                      onChange={handleChange}
                      name="role"
                      type="text"
                      error={errorMsg.get('role')}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Divider />
                    <Button
                      style={{ maxWidth: '500px', width: '100%' }}
                      variant="contained"
                      color="primary"
                      size="small"
                      type="submit"
                    >
                      register
                    </Button>
                  </Grid>

                  <Snackbar open={errorMsg.size !== 0} autoHideDuration={6000} onClose={() => setErrorMsg(new Map())}>
                    <Alert onClose={() => setErrorMsg(new Map())} severity="error" sx={{ width: '100%' }}>
                      {[...errorMsg.values()].join(' ')}
                    </Alert>
                  </Snackbar>
                  <Snackbar open={successOpen} autoHideDuration={6000} onClose={handleCloseSuccess}>
                    <Alert onClose={handleCloseSuccess} severity="success" sx={{ width: '100%' }}>
                      Success!
                    </Alert>
                  </Snackbar>
                  <Snackbar open={errorOpen} autoHideDuration={6000} onClose={handleCloseError}>
                    <Alert onClose={handleCloseError} severity="error" sx={{ width: '100%' }}>
                      Error!
                    </Alert>
                  </Snackbar>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </>
  );
}

export default Register;
