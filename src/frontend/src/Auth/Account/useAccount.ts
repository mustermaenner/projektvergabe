// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { useNavigate } from 'react-router';
import { ChangeEvent, FormEvent, useState } from 'react';
import jwtService from '../../Utils/JwtService';

type User = {
  id: string;
};

async function deleteU(eventId: string) {
  const token = jwtService().getToken();
  const tokenInfo = jwtService().getDecodedAccessToken(token);
  const URL = jwtService().getEndpoint(`profs/${tokenInfo.id}/events/${eventId}`);
  const header = jwtService().getHeader();

  const requestOptions = {
    method: 'DELETE',
    headers: header,
  };
  const response = await fetch(URL, requestOptions);
  return response;
}

function hasValues(information: User): Map<string, string> {
  const resultMap = new Map<string, string>();
  if (information.id.length === 0) {
    resultMap.set('email', 'Email can not be empty');
  }
  return resultMap;
}

function validate(information: User): Map<string, string> {
  const resultMap = hasValues(information);
  if (resultMap.size !== 0) {
    return resultMap;
  }
  return resultMap;
}

function handleError(error: string): Map<string, string> {
  const resultMap = new Map<string, string>();
  resultMap.set('id', error);
  resultMap.set('name', error);
  return resultMap;
}

function useAccount() {
  const [user, setUser] = useState({ id: '' });
  const navigate = useNavigate();
  const [error, setError] = useState(new Map<string, string>());

  async function deleteUser(event: FormEvent) {
    event.preventDefault();
    const errors = validate(user);
    setError(errors);
    if (errors.size !== 0) {
      return;
    }
    const response = await deleteU(user.id);
    if (response.ok) {
      localStorage.removeItem('token');
      navigate('/login');
    } else {
      const msg = await response.text();
      const errorResponse = handleError(JSON.parse(msg).message);
      setError(errorResponse);
    }
  }

  function handleChange(event: ChangeEvent<HTMLInputElement>) {
    setUser((prevRegister) => ({
      ...prevRegister,
      [event.target.name]: event.target.value,
    }));
  }

  return { deleteUser, handleChange, user, error };
}

export default useAccount;
