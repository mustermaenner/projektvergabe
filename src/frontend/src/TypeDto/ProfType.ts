// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

type ProfType = {
  id: string;
  name: string;
  mail: string;
  password: string;
  roles: string;
};
export default ProfType;
