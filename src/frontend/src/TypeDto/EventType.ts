// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

type EventType = {
  id: number;
  name: string;
  description: string;
  password: string;
  openUntil: Date;
  prof: { id: number }[];
};
export default EventType;
