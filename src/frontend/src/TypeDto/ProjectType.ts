// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

type ProjectType = {
  id: number;
  eventId: number;
  name: string;
  description: string;
  customer: string;
  withContract: boolean;
};
export default ProjectType;
