// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

type StudentType = {
  id: number;
  firstName: string;
  lastName: string;
  mail: string;
  matriculationNr: number;
  studentIdentifier?: string;
  eventId: number;
  priorities?: { projectId: number; priority: number }[];
  contract: boolean;
  street: string;
  city: string;
  cityCode: string;
  country: string;
  finalProject: number;
  uuidForChanges: string;
  projectId?: number;
  priority?: number;
};
export default StudentType;
