// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import Box from '@mui/material/Box';
import { useNavigate } from 'react-router';
import { useEffect } from 'react';
import { Stack } from '@mui/material';
import EventGrid from '../Components/Grids/EventGrid';
import MenuAppBarProf from '../Components/NavBar/MenuAppBarProf';
import jwtService from '../Utils/JwtService';

function EventSettingsPage() {
  const navigate = useNavigate();

  useEffect(() => {
    if (!jwtService().checkToken()) {
      navigate('/login');
    }
  }, []);

  return (
    <>
      <MenuAppBarProf />
      <Box sx={{ p: 0 }} marginTop={10} style={{ width: '100%', top: '50px' }}>
        <Stack spacing={2} justifyContent="center" textAlign="center" alignItems="center" alignContent="center">
          <EventGrid />
        </Stack>
      </Box>
    </>
  );
}
export default EventSettingsPage;
