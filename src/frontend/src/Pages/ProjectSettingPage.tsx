// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import Box from '@mui/material/Box';
import { useEffect } from 'react';
import { useNavigate } from 'react-router';
import { Stack } from '@mui/material';
import MenuAppBarProf from '../Components/NavBar/MenuAppBarProf';
import ProjectGrid from '../Components/Grids/ProjectGrid';
import jwtService from '../Utils/JwtService';

function ProjectSettingPage() {
  const navigate = useNavigate();

  useEffect(() => {
    if (!jwtService().checkToken()) {
      navigate('/login');
    }
  }, []);

  return (
    <>
      <MenuAppBarProf />
      <Box sx={{ p: 0 }} marginTop={20} style={{ width: '100%', top: '50px' }}>
        <Stack spacing={2} justifyContent="center" textAlign="center" alignItems="center" alignContent="center">
          <ProjectGrid />
        </Stack>
      </Box>
    </>
  );
}
export default ProjectSettingPage;
