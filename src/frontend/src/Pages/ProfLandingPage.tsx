// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import Card from '@mui/material/Card';
import * as React from 'react';
import { Avatar, Badge, CardActionArea, CardContent, CardMedia, Grid, Paper } from '@mui/material';
import Typography from '@mui/material/Typography';
import { Link as RouterLink } from 'react-router-dom';
import { useEffect } from 'react';
import { useNavigate } from 'react-router';
import EventIcon from '@mui/icons-material/Event';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import GroupIcon from '@mui/icons-material/Group';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';
import jwtService from '../Utils/JwtService';
import './ProfLandingPage.css';
import MenuAppBarProf from '../Components/NavBar/MenuAppBarProf';

function ProfLandingPage() {
  const navigate = useNavigate();
  const { name, role } = jwtService().getDecodedAccessToken(jwtService().getToken()) || {};

  useEffect(() => {
    if (!jwtService().checkToken()) {
      navigate('/login');
    }
  }, []);

  return (
    <>
      <MenuAppBarProf />
      <Card
        style={{
          maxWidth: 700,
          padding: '20px 5px',
          margin: '0 auto',
          marginTop: 20,
        }}
      >
        <CardContent>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography align="center" gutterBottom variant="h5" component="div">
                <Badge color="success" badgeContent={role}>
                  <Avatar
                    sx={{
                      width: 100,
                      height: 70,
                    }}
                  >
                    {' '}
                    {name}
                  </Avatar>
                </Badge>
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Paper elevation={3}>
                <Card sx={{}}>
                  <CardActionArea component={RouterLink} to="/event">
                    <CardMedia component={EventIcon} height="60" />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="div">
                        Event & Projects Management
                      </Typography>
                      <Typography variant="body1" color="text.secondary">
                        Read, Add, Edit and Delete Events. Export Events as .csv
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Paper elevation={3}>
                <Card sx={{}}>
                  <CardActionArea component={RouterLink} to="/enrollment">
                    <CardMedia component={AssignmentTurnedInIcon} height="60" />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="div">
                        Enrollment Overview
                      </Typography>
                      <Typography variant="body1" color="text.secondary">
                        Analyze student enrollments and export them.
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Paper elevation={3}>
                <Card sx={{}}>
                  <CardActionArea component={RouterLink} to="/student">
                    <CardMedia component={GroupIcon} height="60" />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="div">
                        Student Management
                      </Typography>
                      <Typography variant="body1" color="text.secondary">
                        Read, Add, Edit and Delete Students
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Paper elevation={3}>
                <Card sx={{}}>
                  <CardActionArea component={RouterLink} to="/account">
                    <CardMedia component={ManageAccountsIcon} height="60" />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="div">
                        Account
                      </Typography>
                      <Typography variant="body1" color="text.secondary">
                        Read, Add, Edit and Delete Users
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Paper>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </>
  );
}

export default ProfLandingPage;
