// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import {
  Stack,
  Box,
  Button,
  SelectChangeEvent,
  MenuItem,
  FormControl,
  InputLabel,
  Select,
  Dialog,
  DialogTitle,
  DialogContent,
} from '@mui/material';
import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import jwtService from '../Utils/JwtService';
import MenuAppBar from '../Components/NavBar/MenuAppBar';
import Input from '../Components/Input';
import useCourseCredential from '../Hooks/useCourseCredential';
import useEvent from '../Hooks/useEvent';

function LandingPage() {
  const { getAccess, errorMsg, credentials, handleChange } = useCourseCredential();
  const { getEventData, eventData } = useEvent();
  const [name, setName] = useState('');
  const navigate = useNavigate();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [selectedEventDescription, setSelectedEventDescription] = useState('');
  const [isSelectOpen, setIsSelectOpen] = useState(false);

  useEffect(() => {
    if (!jwtService().checkToken()) {
      navigate('/');
    }
  }, []);

  useEffect(() => {
    getEventData().then(() => {});
  }, [eventData]);

  const handleChangeSelect = (event: SelectChangeEvent) => {
    credentials.id = event.target.value;
    setName(event.target.value as string);
  };

  const handleInfoClick = (eventId: any) => {
    setSelectedEventDescription(eventData.find((record) => record.id === eventId)?.description || '');
    setDialogOpen(true);
  };

  const handleClose = () => {
    setDialogOpen(false);
  };

  return (
    <>
      <MenuAppBar />

      <form className="login" onSubmit={(event) => getAccess(event)}>
        <Box sx={{ p: 0 }} marginTop={5} style={{ width: '100%', top: '50px' }}>
          <Stack spacing={2} justifyContent="center" alignItems="center" alignContent="center">
            <img src="../1920px-Logo_TH_Rosenheim_2019.svg" alt="logo" width={350} height={120} />

            <FormControl style={{ maxWidth: '300px', width: '100%' }}>
              <InputLabel id="selectEvent">Veranstaltung</InputLabel>
              <Select
                label="Veranstaltung"
                labelId="selectEvent"
                id="selectEvent"
                value={name}
                defaultValue=""
                onChange={handleChangeSelect}
                onOpen={() => setIsSelectOpen(true)}
                onClose={() => setIsSelectOpen(false)}
              >
                {eventData.map((record: any) => (
                  <MenuItem key={record.id} value={record.id}>
                    {record.name}
                    {isSelectOpen && (
                      <IconButton
                        onClick={(e) => {
                          e.stopPropagation();
                          handleInfoClick(record.id);
                        }}
                        style={{ marginLeft: 'auto' }}
                      >
                        <InfoOutlinedIcon />
                      </IconButton>
                    )}
                  </MenuItem>
                ))}
                <Dialog open={dialogOpen} onClose={handleClose}>
                  <DialogTitle>Description</DialogTitle>
                  <DialogContent>{selectedEventDescription}</DialogContent>
                </Dialog>
              </Select>
            </FormControl>

            <div style={{ maxWidth: '300px', width: '100%' }}>
              <Input
                style={{ maxWidth: '300px', width: '100%' }}
                label="Password"
                value={credentials.password}
                onChange={handleChange}
                name="password"
                type="password"
                error={errorMsg.get('password')}
              />
            </div>
            <Button color="secondary" style={{ maxWidth: '300px', width: '100%' }} type="submit" variant="contained">
              Next
            </Button>
          </Stack>
        </Box>

        <br />
      </form>
    </>
  );
}

export default LandingPage;
