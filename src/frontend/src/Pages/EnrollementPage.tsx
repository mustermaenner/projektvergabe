// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import { useNavigate } from 'react-router';
import { useEffect } from 'react';
import Box from '@mui/material/Box';
import { Stack } from '@mui/material';
import jwtService from '../Utils/JwtService';
import MenuAppBarProf from '../Components/NavBar/MenuAppBarProf';
import EnrollmentGrid from '../Components/Grids/EnrollmentGrid';

function EnrollementPage() {
  const navigate = useNavigate();

  useEffect(() => {
    if (!jwtService().checkToken()) {
      navigate('/login');
    }
  }, []);

  return (
    <>
      <MenuAppBarProf />
      <Box sx={{ p: 0 }} marginTop={20} style={{ width: '100%', top: '50px' }}>
        <Stack spacing={2} justifyContent="center" textAlign="center" alignItems="center" alignContent="center">
          <EnrollmentGrid />
        </Stack>
      </Box>
    </>
  );
}
export default EnrollementPage;
