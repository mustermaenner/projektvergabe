// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { ReactElement, useEffect } from 'react';
import { Avatar, Badge, Button, Card, CardContent, Grid } from '@mui/material';
import * as React from 'react';
import { useNavigate } from 'react-router';
import Typography from '@mui/material/Typography';
import useUpdateAccount from '../Auth/Account/useUpdateAccount';
import useAccount from '../Auth/Account/useAccount';
import Input from '../Components/Input';
import jwtService from '../Utils/JwtService';
import MenuAppBarProf from '../Components/NavBar/MenuAppBarProf';

function Account(): ReactElement {
  const { deleteUser, handleChange, user, error } = useAccount();
  const { updateUser, handleChangeUpdate, update, errorUpdate } = useUpdateAccount();
  const navigate = useNavigate();
  const { name, role } = jwtService().getDecodedAccessToken(jwtService().getToken()) || {};

  useEffect(() => {
    if (!jwtService().checkToken()) {
      navigate('/login');
    }
  }, []);

  return (
    <>
      <MenuAppBarProf />
      <form onSubmit={(event) => updateUser(event)}>
        <Grid>
          <Card
            style={{
              maxWidth: 500,
              padding: '20px 5px',
              margin: '0 auto',
              marginTop: 20,
            }}
          >
            <CardContent>
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <Typography align="center" gutterBottom variant="h5" component="div">
                    <Badge color="success" badgeContent={role}>
                      <Avatar
                        sx={{
                          width: 100,
                          height: 70,
                        }}
                      >
                        {' '}
                        {name}
                      </Avatar>
                    </Badge>
                  </Typography>
                </Grid>

                <Grid item xs={12}>
                  <Button
                    style={{ maxWidth: '500px', width: '100%' }}
                    variant="outlined"
                    onClick={() => navigate('/register')}
                  >
                    Register new User
                  </Button>
                </Grid>
                <Grid item xs={12}>
                  <Typography align="center" gutterBottom variant="h5" component="div">
                    Modify users
                  </Typography>
                </Grid>

                <Grid item xs={12}>
                  <Input
                    label="Prof Id"
                    style={{}}
                    value={update.profId}
                    onChange={handleChangeUpdate}
                    name="profId"
                    type="number"
                    error={errorUpdate.get('profId')}
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12}>
                  <Input
                    label="Event Id"
                    style={{}}
                    value={update.eventId}
                    onChange={handleChangeUpdate}
                    name="eventId"
                    type="number"
                    error={errorUpdate.get('eventID')}
                    fullWidth
                  />
                </Grid>

                <Grid item xs={12}>
                  <Button
                    style={{ maxWidth: '700px', width: '100%' }}
                    variant="outlined"
                    color="secondary"
                    type="submit"
                  >
                    change
                  </Button>
                </Grid>

                <Grid item xs={12}>
                  <Typography align="center" gutterBottom variant="h5" component="div">
                    Delete account
                  </Typography>
                </Grid>

                <Grid item xs={12}>
                  <form onSubmit={(event) => deleteUser(event)}>
                    <Grid container spacing={1}>
                      <Grid item xs={12}>
                        <Input
                          label="Prof Id"
                          style={{}}
                          value={user.id}
                          onChange={handleChange}
                          name="id"
                          type="number"
                          error={error.get('id')}
                          fullWidth
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Button
                          style={{ maxWidth: '700px', width: '100%' }}
                          variant="outlined"
                          color="error"
                          type="submit"
                        >
                          Delete Account
                        </Button>
                      </Grid>
                    </Grid>
                  </form>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </form>
    </>
  );
}

export default Account;
