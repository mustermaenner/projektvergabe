// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

/* eslint-disable react/jsx-no-bind */
import * as React from 'react';
import { FormEvent, useEffect, useState } from 'react';
import {
  Button,
  Card,
  CardContent,
  Checkbox,
  FormControlLabel,
  Grid,
  FormControl,
  Select,
  MenuItem,
  SelectChangeEvent,
  InputLabel,
  Collapse,
  Dialog,
  DialogTitle,
  DialogContent,
  Alert,
} from '@mui/material';
import Typography from '@mui/material/Typography';
import { useLocation, useNavigate } from 'react-router';
import MenuAppBar from '../Components/NavBar/MenuAppBar';
import useFormular from '../Hooks/useFormular';
import Input from '../Components/Input';
import ProjectType from '../TypeDto/ProjectType';
import jwtService from '../Utils/JwtService';

function FormularPage() {
  const { doSend, checkFormular, forms, errorMsg, postResultSuccessfully, postResultFailed, handleChange } =
    useFormular();
  const [data, setData] = useState<ProjectType[]>([]);
  const [wishProject1, setwishProject1] = useState('');
  const [wishProject2, setwishProject2] = useState('');
  const [wishProject3, setwishProject3] = useState('');
  const [wishProject4, setwishProject4] = useState('');
  const [wishProject5, setwishProject5] = useState('');
  const [contract, setContract] = useState(true);
  const [disabledPopup, setDisabledPopup] = useState(false);
  const [disabledSent, setDisabledSent] = useState(true);
  const selectedProjects = [wishProject1, wishProject2, wishProject3, wishProject4, wishProject5];
  const location = useLocation();
  const navigator = useNavigate();

  useEffect(() => {
    if (location.state.password.length === 0 || location.state.id.length === 0) {
      navigator('/');
    }
    forms.eventId = location.state.id;
    forms.eventPassword = location.state.password;
    const headers = { password: forms.eventPassword };
    fetch(jwtService().getEndpoint(`events/${forms.eventId}/projects`), { headers })
      .then((response) => response.json())
      .then((json) => setData(json));
  }, []);

  function resetForm() {
    // reset all form values
    setwishProject1('');
    setwishProject2('');
    setwishProject3('');
    setwishProject4('');
    setwishProject5('');
  }

  function handleCollapse() {
    forms.contract = contract;
    setwishProject1('');
    setwishProject2('');
    setwishProject3('');
    setwishProject4('');
    setwishProject5('');
    setContract(!contract);
  }

  function handlePopup(event: FormEvent) {
    if (!checkFormular(event)) {
      window.scrollTo(0, 0);
      return;
    }
    setDisabledSent(true);
    setDisabledPopup(!disabledPopup);
  }

  function handleSent() {
    setDisabledSent(!disabledSent);
  }

  function end() {
    navigator('/');
  }

  const handleChangeSelectWishProject1 = (event: SelectChangeEvent) => {
    forms.wishProject1 = event.target.value;
    setwishProject1(event.target.value as string);
  };

  const handleChangeSelectWishProject2 = (event: SelectChangeEvent) => {
    forms.wishProject2 = event.target.value;
    setwishProject2(event.target.value as string);
  };

  const handleChangeSelectWishProject3 = (event: SelectChangeEvent) => {
    forms.wishProject3 = event.target.value;
    setwishProject3(event.target.value as string);
  };

  const handleChangeSelectWishProject4 = (event: SelectChangeEvent) => {
    forms.wishProject4 = event.target.value;
    setwishProject4(event.target.value as string);
  };

  const handleChangeSelectWishProject5 = (event: SelectChangeEvent) => {
    forms.wishProject5 = event.target.value;
    setwishProject5(event.target.value as string);
  };

  const returnBoolean = (input: boolean) => {
    if (input) {
      return 'Ja';
    }
    return 'Nein';
  };

  const filterFunction = (input: ProjectType) => {
    if (!input.withContract) {
      return input;
    }
    if (input.withContract) {
      if (!contract) {
        return input;
      }
    }
    return undefined;
  };

  return (
    <>
      <MenuAppBar />
      <form onSubmit={(event) => handlePopup(event)}>
        <Grid>
          <Card
            style={{
              maxWidth: 700,
              padding: '20px 5px',
              margin: '0 auto',
              marginTop: 20,
            }}
          >
            <CardContent>
              <Typography gutterBottom variant="h5">
                Bewerberbogen für Projekte
              </Typography>
              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                Persönliche Angaben
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p" gutterBottom>
                Die mit * gekennzeichneten Felder müssen ausgefühlt werden
              </Typography>
              <FormControl>
                <Grid container spacing={1} alignItems="center">
                  <Grid item xs={12}>
                    <Input
                      style={{}}
                      label="Matrikelnummer"
                      value={forms.matriculation}
                      onChange={handleChange}
                      name="matriculation"
                      type="text"
                      error={errorMsg.get('matriculation')}
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid xs={12} sm={6} item>
                    <Input
                      style={{}}
                      label="Vorname"
                      value={forms.firstname}
                      onChange={handleChange}
                      name="firstname"
                      type="text"
                      error={errorMsg.get('firstname')}
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid xs={12} sm={6} item>
                    <Input
                      style={{}}
                      label="Nachname"
                      value={forms.surname}
                      onChange={handleChange}
                      name="surname"
                      type="text"
                      error={errorMsg.get('surname')}
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Input
                      style={{}}
                      label="E-Mail"
                      value={forms.email}
                      onChange={handleChange}
                      name="email"
                      type="email"
                      error={errorMsg.get('email')}
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid xs={12} sm={6} item>
                    <Input
                      style={{}}
                      label="Studentenkennung"
                      value={forms.studentID}
                      onChange={handleChange}
                      name="studentID"
                      type="text"
                      error={errorMsg.get('studentID')}
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid xs={12} sm={6} item>
                    <Input
                      style={{}}
                      label="Fachsemester"
                      value={forms.semester}
                      onChange={handleChange}
                      name="semester"
                      type="number"
                      error={errorMsg.get('semester')}
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="body2" color="textSecondary" component="p" gutterBottom>
                      Ich würde den Vertrag Unterschreiben:
                    </Typography>
                  </Grid>
                  <Grid xs={12} item>
                    <FormControlLabel onChange={handleCollapse} control={<Checkbox />} label="Ja" />
                  </Grid>
                  <Grid xs={12} item>
                    <Collapse in={!contract}>
                      <Grid container spacing={1}>
                        <Grid item xs={12}>
                          <Typography variant="body2" color="textSecondary" component="p" gutterBottom>
                            Falls ja, bitte Adresse für den Vertrag angeben:
                          </Typography>
                        </Grid>
                        <Grid xs={12} sm={6} item>
                          <Input
                            style={{}}
                            label="Straße"
                            value={forms.street}
                            onChange={handleChange}
                            name="street"
                            type="text"
                            error={errorMsg.get('street')}
                            fullWidth
                            required
                            disabled={contract}
                          />
                        </Grid>
                        <Grid xs={12} sm={6} item>
                          <Input
                            style={{}}
                            label="Hausnummer"
                            value={forms.streetNumber}
                            onChange={handleChange}
                            name="streetNumber"
                            type="text"
                            error={errorMsg.get('streetNumber')}
                            fullWidth
                            required
                            disabled={contract}
                          />
                        </Grid>
                        <Grid xs={12} sm={6} item>
                          <Input
                            style={{}}
                            label="Postleitzahl"
                            value={forms.postCode}
                            onChange={handleChange}
                            name="postCode"
                            type="text"
                            error={errorMsg.get('postCode')}
                            fullWidth
                            required
                            disabled={contract}
                          />
                        </Grid>
                        <Grid xs={12} sm={6} item>
                          <Input
                            style={{}}
                            label="Ort"
                            value={forms.location}
                            onChange={handleChange}
                            name="location"
                            type="text"
                            error={errorMsg.get('location')}
                            fullWidth
                            required
                            disabled={contract}
                          />
                        </Grid>
                        <Grid xs={12} item>
                          <Input
                            style={{}}
                            label="Land"
                            value={forms.country}
                            onChange={handleChange}
                            name="country"
                            type="text"
                            error={errorMsg.get('country')}
                            fullWidth
                            required
                            disabled={contract}
                          />
                        </Grid>
                      </Grid>
                    </Collapse>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                      Wahl der Projekte
                    </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Button type="reset" onClick={resetForm}>
                      Reset
                    </Button>
                    <FormControl style={{ maxWidth: '700', width: '100%' }}>
                      <InputLabel id="wishProjekt">1. Wunschprojekt</InputLabel>
                      <Select
                        label="1. Wunschprojekt"
                        labelId="wishProjekt"
                        id="wishProjekt1"
                        value={wishProject1}
                        onChange={handleChangeSelectWishProject1}
                      >
                        {data
                          .filter((item) => filterFunction(item))
                          .map((record: any) => (
                            <MenuItem key={record.id} value={record.id} disabled={selectedProjects.includes(record.id)}>
                              {record.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl style={{ maxWidth: '700', width: '100%' }}>
                      <InputLabel id="wishProjekt2">2. Wunschprojekt</InputLabel>
                      <Select
                        label="2. Wunschprojekt"
                        labelId="wishProjekt2"
                        id="wishProjekt2"
                        value={wishProject2}
                        onChange={handleChangeSelectWishProject2}
                      >
                        {data
                          .filter((item) => filterFunction(item))
                          .map((record: any) => (
                            <MenuItem key={record.id} value={record.id} disabled={selectedProjects.includes(record.id)}>
                              {record.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl style={{ maxWidth: '700', width: '100%' }}>
                      <InputLabel id="wishProjekt3">3. Wunschprojekt</InputLabel>
                      <Select
                        label="3. Wunschprojekt"
                        labelId="wishProjekt3"
                        id="wishProjekt3"
                        value={wishProject3}
                        onChange={handleChangeSelectWishProject3}
                      >
                        {data
                          .filter((item) => filterFunction(item))
                          .map((record: any) => (
                            <MenuItem key={record.id} value={record.id} disabled={selectedProjects.includes(record.id)}>
                              {record.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl style={{ maxWidth: '700', width: '100%' }}>
                      <InputLabel id="wishProjekt4">4. Wunschprojekt</InputLabel>
                      <Select
                        label="4. Wunschprojekt"
                        labelId="wishProjekt4"
                        id="wishProjekt4"
                        value={wishProject4}
                        onChange={handleChangeSelectWishProject4}
                      >
                        {data
                          .filter((item) => filterFunction(item))
                          .map((record: any) => (
                            <MenuItem key={record.id} value={record.id} disabled={selectedProjects.includes(record.id)}>
                              {record.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl style={{ maxWidth: '700', width: '100%' }}>
                      <InputLabel id="wishProjekt5">5. Wunschprojekt</InputLabel>
                      <Select
                        label="5. Wunschprojekt"
                        labelId="wishProjekt5"
                        id="wishProjekt5"
                        value={wishProject5}
                        onChange={handleChangeSelectWishProject5}
                      >
                        {data
                          .filter((item) => filterFunction(item))
                          .map((record: any) => (
                            <MenuItem key={record.id} value={record.id} disabled={selectedProjects.includes(record.id)}>
                              {record.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <Button style={{ maxWidth: '700px', width: '100%' }} variant="contained" onClick={handlePopup}>
                      Weiter
                    </Button>
                  </Grid>
                  <Grid item xs={12} />
                </Grid>
                <Dialog
                  open={disabledPopup}
                  onClose={handlePopup}
                  scroll="paper"
                  aria-labelledby="scroll-dialog-title"
                  aria-describedby="scroll-dialog-description"
                >
                  <DialogTitle id="scroll-dialog-title">Überprüfen Sie Ihre Angaben:</DialogTitle>
                  <DialogContent dividers>
                    <Grid container spacing={1} alignItems="center">
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          Matrikelnummer:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.matriculation}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          Vorname:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.firstname}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          Nachname:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.surname}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          E-Mail:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.email}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          Studentenkennung:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.studentID}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          Fachsemester:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.semester}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          Ich würde den Vertrag unterschreiben:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {returnBoolean(forms.contract)}
                        </Typography>
                      </Grid>
                      <Grid xs={12} item>
                        <Collapse in={!contract}>
                          <Grid container spacing={1}>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                Straße:
                              </Typography>
                            </Grid>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                {forms.street}
                              </Typography>
                            </Grid>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                Hausnummer:
                              </Typography>
                            </Grid>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                {forms.streetNumber}
                              </Typography>
                            </Grid>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                Postleitzahl:
                              </Typography>
                            </Grid>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                {forms.postCode}
                              </Typography>
                            </Grid>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                Ort:
                              </Typography>
                            </Grid>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                {forms.location}
                              </Typography>
                            </Grid>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                Land:
                              </Typography>
                            </Grid>
                            <Grid item xs={6}>
                              <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                                {forms.country}
                              </Typography>
                            </Grid>
                          </Grid>
                        </Collapse>
                      </Grid>

                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          1. Wunschprojekt:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.wishProject1}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          2. Wunschprojekt:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.wishProject2}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          3. Wunschprojekt:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.wishProject3}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          4. Wunschprojekt:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.wishProject4}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          5. Wunschprojekt:
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          {forms.wishProject5}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          Ja meine Angaben sind richtig und ich stimme der Datenschutzerklärung zu:
                        </Typography>
                      </Grid>
                      <Grid xs={6} item>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          Ja: <Checkbox onChange={handleSent} />
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Button style={{ maxWidth: '700px', width: '100%' }} variant="contained" onClick={handlePopup}>
                          Zurück
                        </Button>
                      </Grid>
                      <Grid item xs={6}>
                        <Button
                          style={{ maxWidth: '700px', width: '100%' }}
                          disabled={disabledSent}
                          variant="contained"
                          onClick={doSend}
                        >
                          Absenden
                        </Button>
                      </Grid>
                    </Grid>
                  </DialogContent>
                </Dialog>
                <Dialog open={postResultSuccessfully}>
                  <DialogContent>
                    <Grid container spacing={1} alignItems="center">
                      <Grid item xs={12}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          Die Eingabe wurde erfolgreich gesendet
                        </Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <Button style={{ maxWidth: '700px', width: '100%' }} variant="contained" onClick={end}>
                          OK
                        </Button>
                      </Grid>
                    </Grid>
                  </DialogContent>
                </Dialog>
                <Dialog open={postResultFailed}>
                  <DialogContent>
                    <Grid container spacing={1} alignItems="center">
                      <Grid item xs={12}>
                        <Typography variant="h6" color="textSecondary" component="p" gutterBottom>
                          <Alert severity="error">
                            Bei der Übertragung ist ein Fehler aufgetreten, bitte versuchen Sie es später noch einmal!
                          </Alert>
                        </Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <Button style={{ maxWidth: '700px', width: '100%' }} variant="contained" onClick={end}>
                          OK
                        </Button>
                      </Grid>
                    </Grid>
                  </DialogContent>
                </Dialog>
              </FormControl>
            </CardContent>
          </Card>
        </Grid>
      </form>
    </>
  );
}

export default FormularPage;
