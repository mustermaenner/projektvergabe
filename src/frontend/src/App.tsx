// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import React, { createContext, Dispatch, SetStateAction, useState } from 'react';
import './App.css';
import { Navigate } from 'react-router';

function checkToken(): string {
  const savedToken = localStorage.getItem('token');
  if (!savedToken) {
    return '';
  }
  return savedToken;
}

export const TokenContext = createContext<[string, Dispatch<SetStateAction<string>>]>([checkToken(), () => {}]);

function App() {
  const token = useState(checkToken());

  if (!token) {
    return (
      <TokenContext.Provider value={token}>
        <Navigate to="login" replace />
      </TokenContext.Provider>
    );
  }
  return <Navigate to="overview" replace />;
}

export default App;
