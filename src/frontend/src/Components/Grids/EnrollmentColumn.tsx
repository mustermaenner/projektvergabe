// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0
import { GridColDef } from '@mui/x-data-grid';

interface NestedGridColDef extends GridColDef {
  children?: NestedGridColDef[];
}

function EnrollmentColumn() {
  const enrollmentColumn: NestedGridColDef[] = [
    { field: 'matriculationNr', headerName: 'ID', width: 120 },
    {
      field: 'firstName',
      headerName: 'firstName',
      type: 'string',
      width: 260,
      editable: true,
    },
    {
      field: 'lastName',
      headerName: 'lastName',
      type: 'string',
      width: 300,
      editable: true,
    },
    { field: 'finalProject', headerName: 'Final Project', width: 120, editable: true, sortable: true },
    {
      field: 'priority1',
      headerName: 'Priority 1',
      width: 120,
      sortable: true,
    },
    {
      field: 'priority2',
      headerName: 'Priority 2',
      width: 120,
      sortable: true,
    },
    {
      field: 'priority3',
      headerName: 'Priority 3',
      width: 120,
      sortable: true,
    },
    {
      field: 'priority4',
      headerName: 'Priority 4',
      width: 120,
      sortable: true,
    },
    {
      field: 'priority5',
      headerName: 'Priority 5',
      width: 120,
      sortable: true,
    },
  ];

  return enrollmentColumn;
}

export default EnrollmentColumn;
