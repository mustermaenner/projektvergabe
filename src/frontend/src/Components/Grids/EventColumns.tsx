// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import { useCallback } from 'react';
import { GridColDef } from '@mui/x-data-grid';
import { Alert, Button, Snackbar, Stack } from '@mui/material';
import SaveIcon from '@mui/icons-material/Save';
import useEvent from '../../Hooks/useEvent';

function EventColumn() {
  const { updateEventData, setEventData, errorMsg, setErrorMsg, successOpen, setSuccessOpen, errorOpen, setErrorOpen } =
    useEvent();

  const handleCloseSuccess = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
  }, []);

  const handleCloseError = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setErrorOpen(false);
  }, []);

  async function handleSave(row: any) {
    const { id, name, description, password, openUntil, profs } = row;
    const response = await updateEventData(id, name, description, password, openUntil, profs);
    return response;
  }

  const eventColumns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 120 },
    {
      field: 'name',
      headerName: 'name',
      type: 'string',
      width: 260,
      editable: true,
    },

    {
      field: 'description',
      headerName: 'description',
      type: 'string',
      width: 400,
      editable: true,
    },
    {
      field: 'password',
      headerName: 'password',
      type: 'string',
      width: 160,
      editable: true,
    },
    {
      field: 'openUntil',
      headerName: 'openUntil',
      type: 'dateTime',
      width: 200,
      editable: true,
    },
    {
      field: 'Prof',
      headerName: 'Prof',
      type: 'number',
      width: 200,
      editable: true,
    },

    {
      field: 'action',
      headerName: '',
      width: 80,
      filterable: false,
      sortable: false,
      hideable: false,
      renderCell: (params) => {
        const onClick = () => {
          handleSave(params.row).then(() => setEventData(params.row));
        };

        return (
          <Stack direction="row" spacing={2}>
            <Button variant="outlined" color="warning" size="small" onClick={onClick}>
              <SaveIcon />
            </Button>
            <Snackbar open={errorMsg.size !== 0} autoHideDuration={6000} onClose={() => setErrorMsg(new Map())}>
              <Alert onClose={() => setErrorMsg(new Map())} severity="error" sx={{ width: '100%' }}>
                {[...errorMsg.values()].join(' ')}
              </Alert>
            </Snackbar>
            <Snackbar open={successOpen} autoHideDuration={6000} onClose={handleCloseSuccess}>
              <Alert onClose={handleCloseSuccess} severity="success" sx={{ width: '100%' }}>
                Success!
              </Alert>
            </Snackbar>
            <Snackbar open={errorOpen} autoHideDuration={6000} onClose={handleCloseError}>
              <Alert onClose={handleCloseError} severity="error" sx={{ width: '100%' }}>
                Error!
              </Alert>
            </Snackbar>
          </Stack>
        );
      },
    },
  ];

  return eventColumns;
}
export default EventColumn;
