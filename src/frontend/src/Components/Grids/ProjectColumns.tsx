// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import { useCallback } from 'react';
import { GridColDef } from '@mui/x-data-grid';
import { Alert, Button, Snackbar, Stack } from '@mui/material';
import SaveIcon from '@mui/icons-material/Save';
import useProject from '../../Hooks/useProject';

const ProjectColumns = () => {
  const { updateProjectData, setData, errorMsg, setErrorMsg, successOpen, setSuccessOpen, errorOpen, setErrorOpen } =
    useProject();

  const handleCloseSuccess = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
  }, []);

  const handleCloseError = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setErrorOpen(false);
  }, []);

  async function handleSave(row: any) {
    const { id, eventId, name, description, customer, contract } = row;
    // create new data
    const response = await updateProjectData(id, eventId, name, description, customer, contract);
    return response;
  }

  const projectColumns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 90 },
    {
      field: 'eventId',
      headerName: 'eventId',
      type: 'number',
      width: 160,
      editable: true,
    },

    {
      field: 'name',
      headerName: 'name',
      type: 'string',
      width: 160,
      editable: true,
    },
    {
      field: 'description',
      headerName: 'description',
      type: 'string',
      width: 400,
      editable: true,
    },
    {
      field: 'customer',
      headerName: 'customer',
      type: 'string',
      width: 160,
      editable: true,
    },
    {
      field: 'withContract',
      headerName: 'contract',
      type: 'boolean',
      width: 160,
      editable: true,
    },

    {
      field: 'action',
      headerName: '',
      width: 80,
      filterable: false,
      sortable: false,
      hideable: false,

      renderCell: (params) => {
        const onClick = () => {
          // pass the entire row object to the handleSave function
          handleSave(params.row).then(() => setData(params.row));
        };

        return (
          <Stack direction="row" spacing={2}>
            <Button variant="outlined" color="warning" size="small" onClick={onClick}>
              <SaveIcon />
            </Button>
            <Snackbar open={errorMsg.size !== 0} autoHideDuration={6000} onClose={() => setErrorMsg(new Map())}>
              <Alert onClose={() => setErrorMsg(new Map())} severity="error" sx={{ width: '100%' }}>
                {[...errorMsg.values()].join(' ')}
              </Alert>
            </Snackbar>
            <Snackbar open={successOpen} autoHideDuration={6000} onClose={handleCloseSuccess}>
              <Alert onClose={handleCloseSuccess} severity="success" sx={{ width: '100%' }}>
                Success!
              </Alert>
            </Snackbar>
            <Snackbar open={errorOpen} autoHideDuration={6000} onClose={handleCloseError}>
              <Alert onClose={handleCloseError} severity="error" sx={{ width: '100%' }}>
                Error!
              </Alert>
            </Snackbar>
          </Stack>
        );
      },
    },
  ];
  return projectColumns;
};
export default ProjectColumns;
