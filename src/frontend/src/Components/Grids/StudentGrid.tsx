// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid, GridSelectionModel } from '@mui/x-data-grid';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useLocation } from 'react-router';
import { Alert, Button, FormControl, InputLabel, Select, Snackbar, TextField } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import SearchIcon from '@mui/icons-material/Search';
import CustomToolbar from './CustomToolbar';
import useSearch from '../useSearch';
import useStudent from '../../Hooks/useStudent';
import StudentType from '../../TypeDto/StudentType';
import EventType from '../../TypeDto/EventType';
import useEvent from '../../Hooks/useEvent';
import StudentColumn from './StudentColumns';
import './StudentGrid.css';
import Input from '../Input';
import StudentEnrollmentDto from '../../dto/StudentEnrollment.dto';

function StudentGrid() {
  const [selectedId, setSelectedId] = useState(0);
  const [selectionModel, setSelectionModel] = useState<GridSelectionModel>();
  const {
    errorMsg,
    setErrorMsg,
    successOpen,
    setSuccessOpen,
    errorOpen,
    setErrorOpen,
    getStudentData,
    postStudentData,
    deleteStudentData,
    studentData,
    setStudentData,
  } = useStudent();
  const { getEventDataProf, eventData } = useEvent();
  const [open, setOpen] = useState(false);
  const [rowData, setSelectedRowData] = useState<StudentEnrollmentDto[]>();
  const { searchValue, filteredData, handleSearch } = useSearch(studentData);
  const [inputFirstName, setInputFirstName] = useState('');
  const [inputLastName, setInputLastName] = useState('');
  const [inputMail, setInputMail] = useState('');
  const [inputStudIdentifier, setInputStudIdentifier] = useState('');
  const [inputMatricel, setInputMatricel] = useState(0);
  const [inputStreet, setInputStreet] = useState('');
  const [inputCity, setInputCity] = useState('');
  const [inputCityCode, setInputCityCode] = useState(0);
  const [inputCountry, setInputCountry] = useState('');
  const [inputFinalProject, setInputFinalProject] = useState(0);
  const [inputUuidForChanges, setInputUuidForChanges] = useState('');
  const [inputSemester, setInputSemester] = useState(0);
  const [inputWithContract, setInputWithContract] = useState(true);
  const [openAddDialog, setOpenAddDialog] = useState(false);
  const [isTrue, setIsTrue] = useState(false);
  const [selectedEventPassword, setSelectedEventPassword] = useState('');
  const location = useLocation();

  useEffect(() => {
    let data: any;
    getEventDataProf().then((response) => {
      data = response;
    });
    return data;
  }, []);

  useMemo(() => {
    if (selectedId === 0 && location.state !== null) {
      setSelectedId(location.state.eventId);
    }
    getStudentData(selectedId, isTrue).then(() => {});
  }, [selectedId, isTrue]);

  const handleCloseSuccess = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
  }, []);

  const handleCloseError = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setErrorOpen(false);
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
    setSelectedRowData(rowData);
  };
  const handleAddRow = () => {
    setOpenAddDialog(true);
  };

  const handleSave = async (studentType: StudentEnrollmentDto) => {
    if (selectedId) {
      const selectedEvent = eventData.find((event) => event.id === selectedId);
      if (selectedEvent) {
        setSelectedEventPassword(selectedEvent.password);
        await postStudentData(selectedId, studentType, selectedEventPassword);
        setOpenAddDialog(false);
      }
    } else {
      setOpenAddDialog(true);
    }
  };

  const handleSaveAdd = () => {
    const studentEnrollmentDto: StudentEnrollmentDto = {
      firstName: inputFirstName,
      lastName: inputLastName,
      mail: inputMail,
      studentIdentifier: inputStudIdentifier,
      matriculationNr: inputMatricel,
      street: inputStreet,
      city: inputCity,
      cityCode: inputCityCode,
      country: inputCountry,
      finalProject: inputFinalProject,
      uuidForChanges: inputUuidForChanges,
      contract: inputWithContract,
      semester: inputSemester,
    };
    handleSave(studentEnrollmentDto);
  };

  const handleAgree = async () => {
    const selectedIDs = new Set(selectionModel);
    let selectedIDsString = '';
    selectedIDs.forEach((id) => {
      selectedIDsString += `${id}:`;
    });
    selectedIDsString = selectedIDsString.slice(0, -1);
    if (selectedIDs.size === 1) {
      await deleteStudentData(selectedId, selectedIDsString);
    } else {
      const deletePromises = selectedIDsString.split(':').map((id) => deleteStudentData(selectedId, id));
      await Promise.all(deletePromises);
    }
    // filter the event eventData to remove the deleted rows
    setStudentData((prevData) => prevData.filter((x) => !selectedIDs.has(x.matriculationNr)));
    setOpen(false);
  };

  const handleCloseAdd = () => {
    setOpenAddDialog(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Box sx={{ height: 500, width: '80%', alignItems: 'center' }}>
        <FormControl style={{ maxWidth: '300px', width: '100%' }}>
          <TextField
            label="Search"
            value={searchValue}
            onChange={handleSearch}
            InputProps={{
              startAdornment: <SearchIcon color="action" style={{ marginRight: 8 }} />,
            }}
          />
        </FormControl>

        <FormControl>
          <Select
            native
            value={selectedId.toString()}
            onChange={(event) => setSelectedId(parseInt(event.target.value, 10))}
            inputProps={{
              name: 'id',
              id: 'id',
            }}
          >
            {eventData.map((event: EventType) => (
              <option key={event.id} value={event.id}>
                {event.name}
              </option>
            ))}
          </Select>
        </FormControl>
        <FormControl>
          <InputLabel id="demo-simple-select-autowidth-label">Assigned</InputLabel>
          <Select
            native
            label="Assigned"
            value={isTrue}
            onChange={(event) => setIsTrue(event.target.value === 'true')}
            inputProps={{
              name: 'isTrue',
              id: 'isTrue',
            }}
          >
            <option value="true">True</option>
            <option value="false">False</option>
          </Select>
        </FormControl>
        <DataGrid
          autoHeight
          className="my-eventData-grid"
          rows={filteredData}
          getRowId={(row: StudentType) => row.matriculationNr}
          components={{
            Toolbar: CustomToolbar,
          }}
          columns={StudentColumn()}
          pageSize={7}
          rowsPerPageOptions={[7]}
          checkboxSelection
          onSelectionModelChange={(newSelectionModel) => {
            const selectedIDs = new Set(newSelectionModel);
            setSelectionModel(newSelectionModel);
            const selectedRowData = studentData.filter((row) => selectedIDs.has(row.matriculationNr.toString()));
            setSelectedRowData(selectedRowData);
          }}
          selectionModel={selectionModel}
          experimentalFeatures={{ newEditingApi: true }}
        />

        <Button color="secondary" variant="contained" size="medium" onClick={handleAddRow}>
          <AddIcon /> ADD
        </Button>
        <Dialog open={openAddDialog} onClose={handleCloseAdd}>
          <DialogTitle>Add new Student</DialogTitle>
          <DialogContent>
            <Input
              label="firstName"
              value={inputFirstName}
              onChange={(event) => setInputFirstName(event.target.value)}
              name="name"
              style={{}}
              type="text"
            />
            <Input
              label="lastName"
              value={inputLastName}
              onChange={(event) => setInputLastName(event.target.value)}
              name="description"
              style={{}}
              type="text"
            />
            <Input
              label="mail"
              value={inputMail}
              onChange={(event) => setInputMail(event.target.value)}
              name="mail"
              style={{}}
              type="text"
            />
            <Input
              label="matriculationNr"
              value={inputMatricel}
              onChange={(event) => setInputMatricel(parseInt(event.target.value, 10))}
              name="matriculationNr"
              style={{}}
              type="number"
            />
            <Input
              label="semester"
              value={inputSemester}
              onChange={(event) => setInputSemester(event.target.value as unknown as number)}
              name="semester"
              style={{}}
              type="number"
            />
            <Input
              label="studentIdentifier"
              value={inputStudIdentifier}
              onChange={(event) => setInputStudIdentifier(event.target.value)}
              name="studentIdentifier"
              style={{}}
              type="text"
            />
            <Input
              label="street"
              value={inputStreet}
              onChange={(event) => setInputStreet(event.target.value)}
              name="street"
              style={{}}
              type="text"
            />
            <FormControl>
              <Select
                native
                value={inputWithContract}
                onChange={(event) => setInputWithContract(event.target.value === 'true')}
                name="inputWithContract"
                style={{}}
                type="boolean"
              >
                <option value="false">No</option>
                <option value="true">Yes</option>
              </Select>
            </FormControl>
            <Input
              label="city"
              value={inputCity}
              onChange={(event) => setInputCity(event.target.value)}
              name="city"
              style={{}}
              type="text"
            />
            <Input
              label="cityCode"
              value={inputCityCode}
              onChange={(event) => setInputCityCode(event.target.value as unknown as number)}
              name="cityCode"
              style={{}}
              type="text"
            />
            <Input
              label="country"
              value={inputCountry}
              onChange={(event) => setInputCountry(event.target.value)}
              name="country"
              style={{}}
              type="text"
            />
            <Input
              label="finalProject"
              value={inputFinalProject}
              onChange={(event) => setInputFinalProject(parseInt(event.target.value, 10))}
              name="country"
              style={{}}
              type="number"
            />
            <Input
              label="uuidForChanges"
              value={inputUuidForChanges}
              onChange={(event) => setInputUuidForChanges(event.target.value)}
              name="uuidForChanges"
              style={{}}
              type="any"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseAdd}>Cancel</Button>
            <Button onClick={handleSaveAdd}>Save</Button>
          </DialogActions>
        </Dialog>
        <Button color="error" variant="outlined" size="medium" onClick={handleClickOpen}>
          <DeleteOutlinedIcon /> DELETE
        </Button>
      </Box>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Are you sure to Delete?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">{`With ID:${selectionModel}`}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button onClick={handleAgree} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar open={errorMsg.size !== 0} autoHideDuration={6000} onClose={() => setErrorMsg(new Map())}>
        <Alert onClose={() => setErrorMsg(new Map())} severity="error" sx={{ width: '100%' }}>
          {[...errorMsg.values()].join(' ')}
        </Alert>
      </Snackbar>
      <Snackbar open={successOpen} autoHideDuration={6000} onClose={handleCloseSuccess}>
        <Alert onClose={handleCloseSuccess} severity="success" sx={{ width: '100%' }}>
          Success!
        </Alert>
      </Snackbar>
      <Snackbar open={errorOpen} autoHideDuration={6000} onClose={handleCloseError}>
        <Alert onClose={handleCloseError} severity="error" sx={{ width: '100%' }}>
          Error!
        </Alert>
      </Snackbar>
    </>
  );
}

export default StudentGrid;
