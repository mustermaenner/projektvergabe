// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid, GridSelectionModel } from '@mui/x-data-grid';
import { Alert, Button, FormControl, Select, Snackbar, TextField } from '@mui/material';
import { useLocation } from 'react-router';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import AddIcon from '@mui/icons-material/Add';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import SearchIcon from '@mui/icons-material/Search';
import CustomToolbar from './CustomToolbar';
import useProject from '../../Hooks/useProject';
import useEvent from '../../Hooks/useEvent';
import useSearch from '../useSearch';
import ProjectColumns from './ProjectColumns';
import Input from '../Input';

function ProjectGrid() {
  const [selectionModel, setSelectionModel] = useState<GridSelectionModel>();
  const {
    errorMsg,
    setErrorMsg,
    successOpen,
    errorOpen,
    setErrorOpen,
    getProjectData,
    postProjectData,
    deleteProjectData,
    data,
  } = useProject();
  const { getEventData } = useEvent();
  const { searchValue, filteredData, handleSearch } = useSearch(data);
  const [selectedId, setSelectedId] = useState(0);
  const [selectedEventPassword, setSelectedEventPassword] = useState('');
  const [inputName, setInputName] = useState('');
  const [eventName, setEventName] = useState('');
  const [inputDescription, setInputDescription] = useState('');
  const [inputCustomer, setInputCustomer] = useState('');
  const [inputWithContract, setInputWithContract] = useState(false);
  const [open, setOpen] = useState(false);
  const [openAddDialog, setOpenAddDialog] = useState(false);
  const location = useLocation();

  useEffect(() => {
    getEventData().then(() => {});
  }, []);

  useEffect(() => {
    setEventName(location.state.name);
    setSelectedId(location.state.id);
    setSelectedEventPassword(location.state.password);
    if (selectedEventPassword !== '') {
      getProjectData(selectedId, selectedEventPassword).then(() => {});
    }
  }, [selectedId, openAddDialog]);

  const handleCloseError = () => {
    setErrorOpen(false);
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleAddRow = () => {
    setOpenAddDialog(true);
  };

  const handleSave = async () => {
    // call the postProjectData function with the user's input as the body
    const result = await postProjectData(selectedId, inputName, inputDescription, inputCustomer, inputWithContract);
    // update the eventData state with the latest eventData from the useEvent hook
    if (result.ok) {
      setOpenAddDialog(false);
    } else {
      setOpenAddDialog(false);
    }
  };

  const handleClose = () => {
    setOpen(false);
    setSelectionModel([]);
  };

  const handleCloseAdd = () => {
    setOpenAddDialog(false);
  };
  const handleAgree = async () => {
    const selectedIDs = new Set(selectionModel);
    let selectedIDsString = '';
    selectedIDs.forEach((id) => {
      selectedIDsString += `${id}:`;
    });
    selectedIDsString = selectedIDsString.slice(0, -1);

    if (selectedIDs.size === 1) {
      const result = await deleteProjectData(selectedIDsString);
      if (result.ok) {
        setOpen(false);
        window.location.reload();
      }
    }
    if (selectedIDs.size !== 1) {
      const deletePromises = selectedIDsString.split(':').map((id) => deleteProjectData(id));
      const result = await Promise.all(deletePromises);
      if (result) {
        setOpen(false);
        window.location.reload();
      }
    }
  };

  return (
    <>
      <Typography variant="h4" component="h4">
        {eventName}
      </Typography>
      <Box sx={{ height: 500, width: '80%', justifyContent: 'center', alignItems: 'center' }}>
        <FormControl style={{ maxWidth: '300px', width: '100%' }}>
          <TextField
            label="Search"
            value={searchValue}
            onChange={handleSearch}
            InputProps={{
              startAdornment: <SearchIcon color="action" style={{ marginRight: 8 }} />,
            }}
          />
        </FormControl>
        <DataGrid
          autoHeight
          className="my-eventData-grid"
          rows={filteredData}
          components={{
            Toolbar: CustomToolbar,
          }}
          columns={ProjectColumns()}
          pageSize={20}
          rowsPerPageOptions={[7]}
          checkboxSelection
          onSelectionModelChange={(newSelectionModel) => {
            setSelectionModel(newSelectionModel);
          }}
          selectionModel={selectionModel}
          experimentalFeatures={{ newEditingApi: true }}
        />

        <Button variant="contained" color="secondary" onClick={handleAddRow}>
          <AddIcon />
          Add
        </Button>
        <Dialog open={openAddDialog} onClose={handleCloseAdd}>
          <DialogTitle>Add new Project</DialogTitle>
          <DialogContent>
            <Input
              label="Name"
              value={inputName}
              onChange={(event) => setInputName(event.target.value)}
              name="name"
              style={{}}
              type="text"
              margin="dense"
            />
            <Input
              label="Description"
              value={inputDescription}
              onChange={(event) => setInputDescription(event.target.value)}
              name="description"
              style={{}}
              type="text"
              margin="dense"
            />
            <Input
              label="Customer"
              value={inputCustomer}
              onChange={(event) => setInputCustomer(event.target.value)}
              name="customer"
              style={{}}
              type="text"
              margin="dense"
            />
            <FormControl>
              <Select
                native
                value={inputWithContract}
                onChange={(event) => setInputWithContract(event.target.value === 'true')}
                name="inputWithContract"
                style={{}}
                type="boolean"
                margin="dense"
              >
                <option value="false">No</option>
                <option value="true">Yes</option>
              </Select>
            </FormControl>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseAdd}>Cancel</Button>
            <Button onClick={handleSave}>Save</Button>
          </DialogActions>
        </Dialog>
        <Button color="error" variant="outlined" size="medium" onClick={handleClickOpen}>
          <DeleteOutlinedIcon /> DELETE
        </Button>
      </Box>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Are you sure to Delete?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">{`With ID:${selectionModel}`}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button onClick={handleAgree} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar open={errorMsg.size !== 0} autoHideDuration={6000} onClose={() => setErrorMsg(new Map())}>
        <Alert onClose={() => setErrorMsg(new Map())} severity="error" sx={{ width: '100%' }}>
          {[...errorMsg.values()].join(' ')}
        </Alert>
      </Snackbar>
      <Snackbar open={successOpen} autoHideDuration={6000}>
        <Alert severity="success" sx={{ width: '100%' }}>
          Success!
        </Alert>
      </Snackbar>
      <Snackbar open={errorOpen} autoHideDuration={6000} onClose={handleCloseError}>
        <Alert onClose={handleCloseError} severity="error" sx={{ width: '100%' }}>
          Error!
        </Alert>
      </Snackbar>
    </>
  );
}

export default ProjectGrid;
