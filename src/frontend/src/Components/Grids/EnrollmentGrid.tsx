// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import { useEffect, useMemo, useState } from 'react';
import { useNavigate } from 'react-router';
import { DataGrid, GridSelectionModel } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import { FormControl, Select } from '@mui/material';
import CustomToolbar from './CustomToolbar';
import useStudent from '../../Hooks/useStudent';
import EventType from '../../TypeDto/EventType';
import StudentType from '../../TypeDto/StudentType';
import EnrollmentColumn from './EnrollmentColumn';
import useEvent from '../../Hooks/useEvent';
import StudentEnrollmentDto from '../../dto/StudentEnrollment.dto';
import useProject from '../../Hooks/useProject';

function EnrollmentGrid() {
  const [selectedId, setSelectedId] = useState(-1);
  const [selectionModel, setSelectionModel] = useState<GridSelectionModel>();
  const [rowData, setSelectedRowData] = useState<StudentEnrollmentDto[]>([]);
  const { getEventDataProf, eventData } = useEvent();
  const { getStudentData, studentData } = useStudent();
  const { getProjectDataProf, data } = useProject();
  const [isTrue] = useState(false);

  const navigator = useNavigate();

  useEffect(() => {
    if (selectedId !== -1) getProjectDataProf(selectedId).then();
  }, [selectedId]);

  useEffect(() => {
    if (selectedId !== -1) {
      getStudentData(selectedId, isTrue).then(() => {});
    }
  }, [selectedId, isTrue]);

  useMemo(() => {
    let memodata: any;
    getEventDataProf().then((response) => {
      memodata = response;
    });
    return memodata;
  }, []);

  const handleRowDoubleClick = (rowDatas: any) => {
    if (rowDatas != null || undefined) {
      setSelectedRowData(rowDatas);
      const matrNr = rowDatas.matriculationNr;
      const eventID = rowDatas.eventId;
      navigator(`/student`, { state: { matrNR: matrNr, eventId: eventID } });
    } else {
      setSelectedRowData(rowData);
    }
  };

  function formatData(enrollmentData: StudentEnrollmentDto[]) {
    let id = 0;
    return enrollmentData.map((student) => {
      let studentArray = {};
      if (student.priorities !== undefined) {
        const priorities = {} as { [key: string]: string };
        student.priorities.forEach((priority) => {
          const project = data.find((p) => p.id === priority.projectId);
          priorities[`priority${priority.priority}`] = project ? project.name : `Project ID: ${priority.projectId}`;
        });
        studentArray = {
          id: (id += 1),
          matriculationNr: student.matriculationNr,
          firstName: student.firstName,
          lastName: student.lastName,
          finalProject: student.finalProject,
          eventId: student.eventId,
          ...priorities,
        };
      }
      return studentArray;
    });
  }

  return (
    <Box sx={{ height: 500, width: '80%', justifyContent: 'center', alignItems: 'center' }}>
      <FormControl style={{ maxWidth: '150px', width: '100%' }}>
        <Select
          native
          value={selectedId}
          onChange={(event) => setSelectedId(parseInt(event.target.value as unknown as string, 10))}
          inputProps={{
            name: 'id',
            id: 'id',
          }}
        >
          <option value="">Select Event</option>
          {eventData.map((event: EventType) => (
            <option key={event.id} value={event.id}>
              {event.name}
            </option>
          ))}
        </Select>
      </FormControl>
      <DataGrid
        autoHeight
        rows={formatData(studentData)}
        getRowId={(row: StudentType) => row.id}
        onRowDoubleClick={(event) => handleRowDoubleClick(event.row)}
        components={{
          Toolbar: CustomToolbar,
        }}
        columns={EnrollmentColumn()}
        pageSize={8}
        rowsPerPageOptions={[8]}
        checkboxSelection
        onSelectionModelChange={(newSelectionModel) => {
          const selectedIDs = new Set(newSelectionModel);
          setSelectionModel(newSelectionModel);
          const selectedRowData = studentData.filter((row) => selectedIDs.has(row.matriculationNr.toString()));
          setSelectedRowData(selectedRowData);
        }}
        selectionModel={selectionModel}
        experimentalFeatures={{ newEditingApi: true }}
      />
    </Box>
  );
}
export default EnrollmentGrid;
