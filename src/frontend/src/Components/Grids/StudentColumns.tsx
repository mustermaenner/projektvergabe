// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import { useCallback, useState } from 'react';
import { GridColDef } from '@mui/x-data-grid';
import { Alert, Button, Snackbar, Stack } from '@mui/material';
import SaveIcon from '@mui/icons-material/Save';
import useStudent from '../../Hooks/useStudent';
import useEvent from '../../Hooks/useEvent';
import EventType from '../../TypeDto/EventType';

const StudentColumn = () => {
  const { postStudentData, setStudentData } = useStudent();
  const { eventData, errorMsg, setErrorMsg, successOpen, setSuccessOpen, errorOpen, setErrorOpen } = useEvent();
  const [mapped, setMapped] = useState('');

  const handleCloseSuccess = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
  }, []);

  const handleCloseError = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setErrorOpen(false);
  }, []);

  async function handleSave(row: any) {
    eventData.find((event: EventType) => setMapped(event.password));
    const response = await postStudentData(row.eventId, row, mapped);
    return response;
  }

  const studentColumns: GridColDef[] = [
    { field: 'matriculationNr', headerName: 'ID', width: 120 },
    {
      field: 'eventId',
      headerName: 'eventId',
      type: 'number',
      width: 100,
      editable: true,
    },
    {
      field: 'finalProject',
      headerName: 'Final Project',
      type: 'number',
      width: 110,
      editable: true,
    },
    {
      field: 'firstName',
      headerName: 'firstName',
      type: 'string',
      width: 200,
      editable: true,
    },

    {
      field: 'lastName',
      headerName: 'lastName',
      type: 'string',
      width: 200,
      editable: true,
    },
    {
      field: 'mail',
      headerName: 'mail',
      type: 'string',
      width: 160,
      editable: true,
    },
    {
      field: 'semester',
      headerName: 'semester',
      type: 'number',
      width: 100,
      editable: true,
    },
    {
      field: 'studentIdentifier',
      headerName: 'studentIdentifier',
      type: 'string',
      width: 100,
      editable: true,
    },
    {
      field: 'contract',
      headerName: 'contract',
      type: 'boolean',
      width: 100,
      editable: true,
    },
    {
      field: 'country',
      headerName: 'country',
      type: 'string',
      width: 200,
      editable: true,
    },
    {
      field: 'city',
      headerName: 'city',
      type: 'string',
      width: 200,
      editable: true,
    },
    {
      field: 'cityCode',
      headerName: 'cityCode',
      type: 'number',
      width: 100,
      editable: true,
    },
    {
      field: 'street',
      headerName: 'street',
      type: 'string',
      width: 200,
      editable: true,
    },
    {
      field: 'uuidForChanges',
      headerName: 'uuidForChanges',
      type: 'string',
      width: 80,
      editable: true,
    },
    {
      field: 'action',
      headerName: '',
      width: 80,
      filterable: false,
      sortable: false,
      hideable: false,
      renderCell: (params) => {
        const onClick = () => {
          handleSave(params.row).then(() => setStudentData(params.row));
        };

        return (
          <Stack direction="row" spacing={2}>
            <Button variant="outlined" color="warning" size="small" onClick={onClick}>
              <SaveIcon />
            </Button>
            <Snackbar open={errorMsg.size !== 0} autoHideDuration={6000} onClose={() => setErrorMsg(new Map())}>
              <Alert onClose={() => setErrorMsg(new Map())} severity="error" sx={{ width: '100%' }}>
                {[...errorMsg.values()].join(' ')}
              </Alert>
            </Snackbar>
            <Snackbar open={successOpen} autoHideDuration={6000} onClose={handleCloseSuccess}>
              <Alert onClose={handleCloseSuccess} severity="success" sx={{ width: '100%' }}>
                Success!
              </Alert>
            </Snackbar>
            <Snackbar open={errorOpen} autoHideDuration={6000} onClose={handleCloseError}>
              <Alert onClose={handleCloseError} severity="error" sx={{ width: '100%' }}>
                Error!
              </Alert>
            </Snackbar>
          </Stack>
        );
      },
    },
  ];

  return studentColumns;
};
export default StudentColumn;
