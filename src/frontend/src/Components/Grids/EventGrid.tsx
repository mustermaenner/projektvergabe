// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid, GridSelectionModel } from '@mui/x-data-grid';
import { useNavigate } from 'react-router';
import { useCallback, useEffect, useState } from 'react';
import { Alert, Button, FormControl, Snackbar, TextField } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import SearchIcon from '@mui/icons-material/Search';
import CustomToolbar from './CustomToolbar';
import EventType from '../../TypeDto/EventType';
import useSearch from '../useSearch';
import useEvent from '../../Hooks/useEvent';
import EventColumn from './EventColumns';
import AddEvent from '../Popups/AddEvent';
import AddEventDto from '../../dto/AddEvent.dto';

function EventGrid() {
  const [selectionModel, setSelectionModel] = useState<GridSelectionModel>();
  const {
    errorMsg,
    setErrorMsg,
    successOpen,
    setSuccessOpen,
    errorOpen,
    setErrorOpen,
    eventData,
    postEventData,
    deleteEventData,
    setEventData,
    getEventDataProf,
  } = useEvent();
  const [open, setOpen] = useState(false);
  const [rowData, setSelectedRowData] = useState<EventType[]>([]);
  const { searchValue, filteredData, handleSearch } = useSearch(eventData);
  const [openAddDialog, setOpenAddDialog] = useState(false);

  const navigator = useNavigate();

  useEffect(() => {
    getEventDataProf().then(() => {});
  }, []);

  const handleRowDoubleClick = (rowDatas: any) => {
    if (rowDatas != null || undefined) {
      setSelectedRowData(rowDatas);
      const eventId = rowDatas.id;
      const findName = rowDatas.name;
      const passwordRow = rowDatas.password;
      navigator(`/project`, { state: { id: eventId, name: findName, password: passwordRow } });
    }
  };

  const handleCloseSuccess = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccessOpen(false);
  }, []);

  const handleCloseError = useCallback((event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setErrorOpen(false);
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
    setSelectedRowData(rowData);
  };
  const handleAddRow = () => {
    setOpenAddDialog(true);
  };

  const handleSave = async (addEventDto: AddEventDto) => {
    // call the postProjectData function with the user's input as the body
    const result = await postEventData(addEventDto);
    // update the eventData state with the latest eventData from the useEvent hook
    if (result.ok) {
      setOpenAddDialog(false);
      window.location.reload();
    } else {
      setOpenAddDialog(true);
    }
  };

  const handleCloseAdd = () => {
    setOpenAddDialog(false);
  };

  const handleClose = () => {
    setOpen(false);
    setSelectionModel([]);
  };

  const handleAgree = async () => {
    const selectedIDs = new Set(selectionModel);
    let selectedIDsString = '';
    selectedIDs.forEach((callbackId) => {
      selectedIDsString += `${callbackId}:`;
    });
    selectedIDsString = selectedIDsString.slice(0, -1);
    if (selectedIDs.size === 1) {
      const result = await deleteEventData(selectedIDsString);
      if (result.ok) {
        window.location.reload();
        setOpen(false);
      }
    } else {
      const deletePromises = selectedIDsString.split(':').map((selectId) => deleteEventData(selectId));
      const result = await Promise.all(deletePromises);
      if (result) {
        setEventData((prevData) => prevData.filter((x) => !selectedIDs.has(x.id)));
        setOpen(false);
      }
    }
  };

  return (
    <>
      <Box sx={{ height: 500, width: '80%', justifyContent: 'center', alignItems: 'center' }}>
        <div className="container">
          <FormControl style={{ maxWidth: '300px', width: '100%' }}>
            <TextField
              label="Search"
              value={searchValue}
              onChange={handleSearch}
              InputProps={{
                startAdornment: <SearchIcon color="action" style={{ marginRight: 8 }} />,
              }}
            />
          </FormControl>
        </div>

        <DataGrid
          autoHeight
          className="my-eventData-grid"
          rows={filteredData}
          onRowDoubleClick={(event) => handleRowDoubleClick(event.row)}
          components={{
            Toolbar: CustomToolbar,
          }}
          columns={EventColumn()}
          pageSize={7}
          rowsPerPageOptions={[7]}
          checkboxSelection
          onSelectionModelChange={(newSelectionModel) => {
            const selectedIDs = new Set(newSelectionModel);
            setSelectionModel(newSelectionModel);
            const selectedRowData = eventData.filter((row) => selectedIDs.has(row.id.toString()));
            setSelectedRowData(selectedRowData);
          }}
          selectionModel={selectionModel}
          experimentalFeatures={{ newEditingApi: true }}
        />

        <Button color="secondary" variant="contained" size="medium" onClick={handleAddRow}>
          <AddIcon /> ADD
        </Button>
        <Dialog open={openAddDialog} onClose={handleCloseAdd}>
          <AddEvent handleCloseAdd={handleCloseAdd} handleSave={handleSave} />
        </Dialog>

        <Button color="error" variant="outlined" size="medium" onClick={handleClickOpen}>
          <DeleteOutlinedIcon /> DELETE
        </Button>
      </Box>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Are you sure to Delete?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">{`With ID:${selectionModel}`}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button onClick={handleAgree} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar open={errorMsg.size !== 0} autoHideDuration={6000} onClose={() => setErrorMsg(new Map())}>
        <Alert onClose={() => setErrorMsg(new Map())} severity="error" sx={{ width: '100%' }}>
          {[...errorMsg.values()].join(' ')}
        </Alert>
      </Snackbar>
      <Snackbar open={successOpen} autoHideDuration={6000} onClose={handleCloseSuccess}>
        <Alert onClose={handleCloseSuccess} severity="success" sx={{ width: '100%' }}>
          Success!
        </Alert>
      </Snackbar>
      <Snackbar open={errorOpen} autoHideDuration={6000} onClose={handleCloseError}>
        <Alert onClose={handleCloseError} severity="error" sx={{ width: '100%' }}>
          Error!
        </Alert>
      </Snackbar>
    </>
  );
}

export default EventGrid;
