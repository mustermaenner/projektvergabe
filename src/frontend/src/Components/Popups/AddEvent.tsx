// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import { useEffect, useState } from 'react';
import { Button } from '@mui/material';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import { Multiselect } from 'multiselect-react-dropdown';
import useEvent from '../../Hooks/useEvent';
import Input from '../Input';
import AddEventDto from '../../dto/AddEvent.dto';
import ProfDto from '../../dto/Prof.dto';
import getProfs from '../../Hooks/useProf';

type Props = {
  handleCloseAdd: () => void;
  handleSave: (addEventDto: AddEventDto) => void;
};

function AddEvent(props: Props) {
  const { handleCloseAdd, handleSave } = props;
  const { getEventData } = useEvent();

  const [inputName, setInputName] = useState('');
  const [inputDescription, setInputDescription] = useState('');
  const [inputPassword, setInputPassword] = useState('');
  const [inputOpenUntil, setInputOpenUntil] = useState(new Date());
  const [inputProfs, setInputProfs] = useState<ProfDto[]>([]);
  const [allProfs, setAllProfs] = useState<ProfDto[]>([]);

  useEffect(() => {
    getEventData().then(() => {});
  }, []);

  useEffect(() => {
    getProfs().then((profs) => {
      setAllProfs(profs);
    });
  }, []);
  const handleSaveAdd = () => {
    const addEventDto: AddEventDto = {
      name: inputName,
      description: inputDescription,
      password: inputPassword,
      openUntil: inputOpenUntil,
      profs: inputProfs.map((prof) => prof.id),
    };
    handleSave(addEventDto);
  };

  function addProf(selectedList: ProfDto[]) {
    setInputProfs(selectedList);
  }

  function removeProf(selectedList: ProfDto[]) {
    setInputProfs(selectedList);
  }

  return (
    <>
      <DialogTitle>Add new Event</DialogTitle>
      <DialogContent>
        <Input
          label="Name"
          required
          value={inputName}
          onChange={(event) => setInputName(event.target.value)}
          name="name"
          type="text"
          margin="dense"
        />
        <Input
          label="Description"
          value={inputDescription}
          onChange={(event) => setInputDescription(event.target.value)}
          name="description"
          type="text"
          margin="dense"
        />
        <Input
          label="Password"
          required
          value={inputPassword}
          onChange={(event) => setInputPassword(event.target.value)}
          name="password"
          type="password"
          margin="dense"
        />
        <Input
          label="OpenUntil"
          required
          value={inputOpenUntil}
          onChange={(event) => setInputOpenUntil(event.target.value as unknown as Date)}
          name="openUntil"
          type="date"
          margin="dense"
        />
        <Multiselect
          options={allProfs}
          displayValue="name"
          onSelect={(selectedList: ProfDto[]) => addProf(selectedList)}
          onRemove={(selectedList: ProfDto[]) => removeProf(selectedList)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseAdd}>Cancel</Button>
        <Button onClick={handleSaveAdd}>Save</Button>
      </DialogActions>
    </>
  );
}

export default AddEvent;
