// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import { CSSProperties } from 'styled-components';
import { ReactElement } from 'react';
import { SvgIcon } from '@mui/material';

type Props = {
  style: CSSProperties | undefined;
};

function ThIcon({ style }: Props): ReactElement {
  return <SvgIcon path="../thLogo.svg" style={style} />;
}

export default ThIcon;
