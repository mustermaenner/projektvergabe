// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Link, ListItemIcon, Menu, MenuItem } from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import PolicyIcon from '@mui/icons-material/Policy';
import LoginIcon from '@mui/icons-material/Login';

function FormAppBar() {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <img src="../1920px-Logo_TH_Rosenheim_2019-withe.png" alt="logo" width={165} height={60} />
          <Typography
            variant="h6"
            component="a"
            href="/"
            sx={{
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
              flexGrow: 1,
              textAlign: 'center',
            }}
          >
            Projektvergabe
          </Typography>

          <IconButton
            aria-controls={open ? 'basic-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
            onClick={handleClick}
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              'aria-labelledby': 'basic-button',
            }}
          >
            <Link href="/" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <HomeIcon />
                </ListItemIcon>
                Home
              </MenuItem>
            </Link>

            <Link href="/data" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <PolicyIcon />
                </ListItemIcon>
                Data privacy
              </MenuItem>
            </Link>

            <Link href="/login" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <LoginIcon fontSize="medium" />
                </ListItemIcon>
                Login
              </MenuItem>
            </Link>
          </Menu>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default FormAppBar;
