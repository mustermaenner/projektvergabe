// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import EventIcon from '@mui/icons-material/Event';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import GroupIcon from '@mui/icons-material/Group';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';
import DashboardCustomizeIcon from '@mui/icons-material/DashboardCustomize';
import HomeIcon from '@mui/icons-material/Home';
import PolicyIcon from '@mui/icons-material/Policy';
import { Link, ListItemIcon, Menu, MenuItem } from '@mui/material';
import LoginIcon from '@mui/icons-material/Login';
import { useState } from 'react';
import { useLocation } from 'react-router';

function MenuAppBarProf() {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const location = useLocation();
  const path = location.pathname;

  let page;
  if (path === '/overview') {
    page = 'Overview';
  } else if (path === '/event') {
    page = 'Events';
  } else if (path === '/project') {
    page = 'Projects';
  } else if (path === '/account') {
    page = 'Account';
  } else if (path === '/data') {
    page = 'Datenschutz';
  } else if (path === '/student') {
    page = 'Student Management';
  } else if (path === '/register') {
    page = 'Register';
  } else {
    page = 'Enrollement Overview';
  }

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar color="secondary" position="static">
        <Toolbar>
          <img src="../1920px-Logo_TH_Rosenheim_2019-withe.png" alt="logo" width={165} height={60} />
          <Typography
            variant="h4"
            component="a"
            href="overview"
            sx={{
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
              flexGrow: 1,
              textAlign: 'center',
            }}
          >
            {page}
          </Typography>
          <IconButton
            aria-controls={open ? 'basic-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
            onClick={handleClick}
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Menu
            id="account-menu"
            anchorEl={anchorEl}
            open={open}
            color="secondary"
            onClose={handleClose}
            MenuListProps={{
              'aria-labelledby': 'basic-button',
            }}
          >
            <Link href="/" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <HomeIcon />
                </ListItemIcon>
                Home
              </MenuItem>
            </Link>
            <Link href="/" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <DashboardCustomizeIcon />
                </ListItemIcon>
                Overview
              </MenuItem>
            </Link>

            <Link href="/event" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <EventIcon />
                </ListItemIcon>
                Events & Projects
              </MenuItem>
            </Link>

            <Link href="/enrollment" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <AssignmentTurnedInIcon />
                </ListItemIcon>
                Enrollment
              </MenuItem>
            </Link>

            <Link href="/student" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <GroupIcon />
                </ListItemIcon>
                Students
              </MenuItem>
            </Link>

            <Link href="/account" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <ManageAccountsIcon />
                </ListItemIcon>
                Account
              </MenuItem>
            </Link>

            <Link href="/data" underline="hover">
              <MenuItem color="black">
                <ListItemIcon>
                  <PolicyIcon />
                </ListItemIcon>
                Data privacy
              </MenuItem>
            </Link>
            <Link href="/logout" color="black" underline="hover">
              <MenuItem>
                <LoginIcon fontSize="medium" />
              </MenuItem>
            </Link>
          </Menu>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default MenuAppBarProf;
