// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

/* eslint-disable react/require-default-props */
import * as React from 'react';
import { TextField } from '@mui/material';
import { ChangeEvent, ReactElement } from 'react';
import { CSSProperties } from 'styled-components';

type Props = {
  style?: CSSProperties;
  label: string;
  value: string | number | Date;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  name: string;
  type: 'text' | 'password' | 'number' | 'email' | 'date' | 'any';
  error?: string | undefined;
  fullWidth?: boolean | undefined;
  required?: boolean | undefined;
  disabled?: boolean | undefined;
  margin?: 'none' | 'dense' | 'normal' | undefined;
};

function Input({
  style,
  label,
  name,
  value,
  onChange,
  type,
  error,
  fullWidth,
  required,
  disabled,
  margin,
}: Props): ReactElement {
  return (
    <div className="Input">
      <TextField
        style={style}
        label={`${label}`}
        variant="outlined"
        type={type}
        value={value}
        onChange={onChange}
        name={name}
        helperText={error}
        error={error !== undefined}
        fullWidth={fullWidth !== false}
        required={required !== false}
        disabled={disabled}
        margin={margin}
      />
    </div>
  );
}
export default Input;
