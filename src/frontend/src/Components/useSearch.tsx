// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { useState, useEffect, ChangeEvent } from 'react';

const useSearch = (data: any[]) => {
  const [searchValue, setSearchValue] = useState('');
  const [filteredData, setFilteredData] = useState(data);

  useEffect(() => {
    try {
      const searchResults = data.filter((row) => {
        const rowProps = Object.values(row);
        return rowProps.some((prop: any) => prop.toString().toLowerCase().includes(searchValue.toLowerCase()));
      });
      setFilteredData(searchResults);
    } catch (error) {
      /* empty */
    }
  }, [searchValue, data]);

  const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchValue(event.target.value);
  };

  return { searchValue, filteredData, handleSearch };
};

export default useSearch;
