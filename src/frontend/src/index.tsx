// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import * as React from 'react';
import { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from '@mui/system';
import CircularProgress from '@mui/material/CircularProgress';
import styled from 'styled-components';
import reportWebVitals from './reportWebVitals';
import theme from './theme';
import ErrorBoundary from './Utils/ErrorBoundary';

const ProjectSettingPage = React.lazy(() => import('./Pages/ProjectSettingPage'));
const EventSettingsPage = React.lazy(() => import('./Pages/EventSettingsPage'));
const Logout = React.lazy(() => import('./Auth/Logout'));
const Account = React.lazy(() => import('./Pages/Account'));
const Register = React.lazy(() => import('./Auth/Account/Register'));
const LandingPage = React.lazy(() => import('./Pages/LandingPage'));
const Login = React.lazy(() => import('./Auth/Login'));
const FormularPage = React.lazy(() => import('./Pages/FormularPage'));
const ProfLandingPage = React.lazy(() => import('./Pages/ProfLandingPage'));
const StudentManagement = React.lazy(() => import('./Pages/StudentManagement'));
const EnrollementPage = React.lazy(() => import('./Pages/EnrollementPage'));

const Container = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
  <ThemeProvider theme={theme}>
    <ErrorBoundary>
      <Suspense
        fallback={
          <Container>
            <CircularProgress />
          </Container>
        }
      >
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<LandingPage />} />
            <Route path="formular" element={<FormularPage />} />
            <Route path="login" element={<Login />} />
            <Route path="logout" element={<Logout />} />
            <Route path="overview" element={<ProfLandingPage />} />
            <Route path="event" element={<EventSettingsPage />} />
            <Route path="project" element={<ProjectSettingPage />} />
            <Route path="account" element={<Account />} />
            <Route path="register" element={<Register />} />
            <Route path="student" element={<StudentManagement />} />
            <Route path="enrollment" element={<EnrollementPage />} />
          </Routes>
        </BrowserRouter>
      </Suspense>
    </ErrorBoundary>
  </ThemeProvider>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
