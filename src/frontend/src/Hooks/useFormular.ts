// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { useState, ChangeEvent, FormEvent } from 'react';
import jwtService from '../Utils/JwtService';

// eslint-disable-next-line @typescript-eslint/naming-convention
type forms = {
  matriculation: string;
  firstname: string;
  surname: string;
  email: string;
  studentID: string;
  semester: number;
  contract: boolean;
  street: string;
  streetNumber: string;
  postCode: string;
  location: string;
  country: string;
  wishProject1: string;
  wishProject2: string;
  wishProject3: string;
  wishProject4: string;
  wishProject5: string;
  eventId: string;
  eventPassword: string;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function handleError(errorMsg: string): Map<string, string> {
  const resultMap = new Map<string, string>();
  resultMap.set('coursename', '');
  resultMap.set('password', errorMsg);
  return resultMap;
}

function hasValues(input: forms): Map<string, string> {
  const resultMap = new Map<string, string>();
  // eslint-disable-next-line prefer-regex-literals
  const regexMatriculation = new RegExp('^[0-9]');
  // eslint-disable-next-line prefer-regex-literals
  const regexEmail = new RegExp(
    // eslint-disable-next-line max-len
    "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
  );
  if (input.matriculation.length === 0) {
    resultMap.set('matriculation', 'can not be empty');
  } else if (!regexMatriculation.test(input.matriculation)) {
    resultMap.set('matriculation', 'matriculation is wrong');
  }
  if (input.firstname.length === 0) {
    resultMap.set('firstname', 'can not be empty');
  }
  if (input.surname.length === 0) {
    resultMap.set('surname', 'can not be empty');
  }
  if (input.studentID.length === 0) {
    resultMap.set('studentID', 'can not be empty');
  }
  if (input.semester === 0) {
    resultMap.set('semester', 'can not be null');
  }
  if (input.semester > 15) {
    resultMap.set('semester', 'semester too high');
  }
  if (input.email.length === 0) {
    resultMap.set('email', 'can not be empty');
  } else if (!regexEmail.test(input.email)) {
    resultMap.set('email', 'email is wrong');
  }
  if (input.contract === true) {
    if (input.street.length === 0) {
      resultMap.set('street', 'can not be empty');
    }
    if (input.streetNumber.length === 0) {
      resultMap.set('streetNumber', 'can not be empty');
    }
    if (input.postCode.length === 0) {
      resultMap.set('postCode', 'can not be empty');
    } else if (!regexMatriculation.test(input.postCode)) {
      resultMap.set('postCode', 'postCode is wrong');
    }
    if (input.postCode.length === 0) {
      resultMap.set('postCode', 'can not be empty');
    }
    if (input.location.length === 0) {
      resultMap.set('location', 'can not be empty');
    }
    if (input.country.length === 0) {
      resultMap.set('country', 'can not be empty');
    }
  }
  return resultMap;
}

async function doSingUp(
  matriculation: string,
  firstname: string,
  surname: string,
  email: string,
  studentID: string,
  semester: number,
  contract: boolean,
  street: string,
  streetNumber: string,
  postCode: string,
  location: string,
  country: string,
  wishProject1: string,
  wishProject2: string,
  wishProject3: string,
  wishProject4: string,
  wishProject5: string,
  eventId: string,
  eventPassword: string,
) {
  const projects: { projectId: number; priority: number }[] = [];
  if (wishProject1 !== '') {
    projects.push({ projectId: Number(wishProject1), priority: 0 });
  }
  if (wishProject2 !== '') {
    projects.push({ projectId: Number(wishProject2), priority: 1 });
  }
  if (wishProject3 !== '') {
    projects.push({ projectId: Number(wishProject3), priority: 2 });
  }
  if (wishProject4 !== '') {
    projects.push({ projectId: Number(wishProject4), priority: 3 });
  }
  if (wishProject5 !== '') {
    projects.push({ projectId: Number(wishProject4), priority: 4 });
  }
  const body = JSON.stringify({
    firstName: firstname,
    lastName: surname,
    mail: email,
    matriculationNr: Number(matriculation),
    studentIdentifier: studentID,
    eventId: Number(eventId),
    priorities: projects,
    contract,
    street: `${street} ${streetNumber}`,
    city: country,
    cityCode: Number(postCode),
    country,
    semester,
  });
  const response = await fetch(jwtService().getEndpoint(`events/${eventId}/studentenrollment`), {
    method: 'POST',
    body,
    headers: { password: eventPassword, 'Content-type': 'application/json' },
  });
  return response.ok;
}

function useFormular() {
  const [forms, setForms] = useState({
    matriculation: '',
    firstname: '',
    surname: '',
    email: '',
    studentID: '',
    semester: 0,
    contract: false,
    street: '',
    streetNumber: '',
    postCode: '',
    location: '',
    country: '',
    wishProject1: '',
    wishProject2: '',
    wishProject3: '',
    wishProject4: '',
    wishProject5: '',
    eventId: '',
    eventPassword: '',
  });
  const [errorMsg, setErrorMsg] = useState(new Map<string, string>());
  const [postResultSuccessfully, setPostresultSuccessfully] = useState(false);
  const [postResultFailed, setPostresultFailed] = useState(false);
  async function doSend(event: FormEvent) {
    event.preventDefault();
    const errors = hasValues(forms);
    if (errors.size !== 0) {
      setErrorMsg(errors);
      return false;
    }

    const response = await doSingUp(
      forms.matriculation,
      forms.firstname,
      forms.surname,
      forms.email,
      forms.studentID,
      forms.semester,
      forms.contract,
      forms.street,
      forms.streetNumber,
      forms.postCode,
      forms.location,
      forms.country,
      forms.wishProject1,
      forms.wishProject2,
      forms.wishProject3,
      forms.wishProject4,
      forms.wishProject5,
      forms.eventId,
      forms.eventPassword,
    );
    if (response) {
      setPostresultSuccessfully(true);
    }
    if (!response) {
      setPostresultFailed(true);
    }
    return false;
  }

  function checkFormular(event: FormEvent) {
    event.preventDefault();
    const errors = hasValues(forms);
    if (errors.size !== 0) {
      setErrorMsg(errors);
      return false;
    }
    return true;
  }

  function handleChange(changeEven: ChangeEvent<HTMLInputElement>) {
    setForms((prevForm) => ({
      ...prevForm,
      [changeEven.target.name]: changeEven.target.value,
    }));
  }

  return {
    doSend,
    checkFormular,
    forms,
    errorMsg,
    postResultSuccessfully,
    postResultFailed,
    handleChange,
  };
}

export default useFormular;
