// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import ProfDto from '../dto/Prof.dto';
import jwtService from '../Utils/JwtService';

async function getProfs(): Promise<ProfDto[]> {
  const URL = jwtService().getEndpoint('profs');
  const response: Response = await fetch(URL, {
    method: 'GET',
    headers: jwtService().getHeader(),
  });
  if (!response.ok) {
    return [];
  }
  return response.json();
}

export default getProfs;
