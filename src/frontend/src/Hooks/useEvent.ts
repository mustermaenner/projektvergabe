// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { useState } from 'react';
import jwtService from '../Utils/JwtService';
import EventType from '../TypeDto/EventType';
import AddEventDto from '../dto/AddEvent.dto';

async function getEvents(): Promise<Response> {
  const header = jwtService().getHeader();
  const URL = jwtService().getEndpoint('events');
  const response = await fetch(URL, {
    method: 'GET',
    headers: header,
  });
  return response;
}

async function getEventsProf(): Promise<Response> {
  const header = jwtService().getHeader();
  const URL = jwtService().getEndpoint('events/prof');
  const response = await fetch(URL, {
    method: 'GET',
    headers: header,
  });
  return response;
}

async function postEvents(addEventDto: AddEventDto): Promise<Response> {
  const header = jwtService().getHeader();
  const URL = jwtService().getEndpoint('events');
  const body = JSON.stringify(addEventDto);

  const response = await fetch(URL, {
    method: 'POST',
    headers: header,
    body,
  });
  return response;
}
async function updateEvents(
  eventId: number,
  name: string,
  description: string,
  password: string,
  openUntil: Date,
  prof: { id: number }[],
): Promise<Response> {
  const header = jwtService().getBasicAuth();
  const URL = jwtService().getEndpoint(`events/${eventId}`);
  const body = JSON.stringify({
    name,
    description,
    password,
    openUntil: new Date(openUntil).toISOString().slice(0, 10),
    prof,
  });
  const response = await fetch(URL, {
    method: 'PATCH',
    headers: header,
    body,
  });
  return response;
}
async function deleteEvent(id: string): Promise<Response> {
  const URL = jwtService().getEndpoint(`events/${id}`);
  const header = jwtService().getHeader();

  const requestOptions = {
    method: 'DELETE',
    headers: header,
  };
  const response = await fetch(URL, requestOptions);
  return response;
}

function handleError(error: string): Map<string, string> {
  const resultMap = new Map<string, string>();
  if (error.includes('eventID') || error.includes('password')) {
    resultMap.set('eventID', error);
  }
  if (error.includes('student')) {
    resultMap.set('student', error);
  }
  return resultMap;
}

function useEvent() {
  const [eventData, setEventData] = useState<EventType[]>([]);
  const [errorMsg, setErrorMsg] = useState(new Map<string, string>());
  const [successOpen, setSuccessOpen] = useState(false);
  const [errorOpen, setErrorOpen] = useState(false);

  async function deleteEventData(id: string): Promise<Response> {
    const response = await deleteEvent(id);
    if (response.ok) {
      setSuccessOpen(true);
      return response;
    }
    const error = await response.text();
    setErrorMsg(handleError(error));
    setErrorOpen(true);
    return response;
  }

  async function getEventData(): Promise<Response> {
    const response = await getEvents();
    if (response.ok) {
      const text = await response.text();
      const jsonData = await JSON.parse(text);
      setEventData(jsonData);
      return jsonData;
    }
    return response;
  }

  async function getEventDataProf() {
    try {
      const response = await getEventsProf();
      if (response.ok) {
        const text = await response.text();
        const jsonData = await JSON.parse(text);
        setEventData(jsonData);
      }
    } catch (error) {
      throw new Error('Failed to get Event Data');
    }
  }

  async function postEventData(addEventDto: AddEventDto): Promise<Response> {
    const response = await postEvents(addEventDto);
    if (response.ok) {
      const text = await response.text();
      const jsonData = await JSON.parse(text);
      setEventData(jsonData);
      setSuccessOpen(true);
      return response;
    }
    const error = await response.text();
    setErrorMsg(handleError(error));
    setErrorOpen(true);
    return response;
  }

  async function updateEventData(
    eventId: number,
    name: string,
    description: string,
    password: string,
    openUntil: Date,
    prof: { id: number }[],
  ): Promise<any> {
    const response = await updateEvents(eventId, name, description, password, openUntil, prof);
    if (response.ok) {
      const text = await response.text();
      const jsonData = await JSON.parse(text);
      setEventData(jsonData);
      setSuccessOpen(true);
    } else {
      const error = await response.text();
      setErrorMsg(handleError(error));
      setErrorOpen(true);
    }
  }
  return {
    errorMsg,
    setErrorMsg,
    successOpen,
    setSuccessOpen,
    errorOpen,
    setErrorOpen,
    getEventDataProf,
    getEventData,
    eventData,
    setEventData,
    postEventData,
    updateEventData,
    deleteEventData,
  };
}

export default useEvent;
