// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { ChangeEvent, FormEvent, useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { TokenContext } from '../App';
import useEvent from './useEvent';
import jwtService from '../Utils/JwtService';

// eslint-disable-next-line @typescript-eslint/naming-convention
type events = {
  id: string;
  password: string;
};

function handleError(errorMsg: string): Map<string, string> {
  const resultMap = new Map<string, string>();
  resultMap.set('id', '');
  resultMap.set('password', errorMsg);
  return resultMap;
}

function hasValues(credentials: events): Map<string, string> {
  const resultMap = new Map<string, string>();
  if (credentials.id.length === 0) {
    resultMap.set('coursename', 'Select');
  }
  if (credentials.password.length === 0) {
    resultMap.set('password', 'Password can not be empty');
  }
  return resultMap;
}

function useCourseCredential() {
  const { getEventData } = useEvent();
  const [credentials, setCredentials] = useState({ id: '', password: '' });
  const [errorMsg, setErrorMsg] = useState(new Map<string, string>());
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [token, setToken] = useContext(TokenContext);
  const navigator = useNavigate();

  useEffect(() => {
    getEventData().then(() => {});
  }, []);

  async function doSingUp(id: string, password: string) {
    const headers = { password };
    return fetch(jwtService().getEndpoint(`events/${id}/projects`), { headers }).then((response) => response.ok);
  }

  async function getAccess(event: FormEvent) {
    event.preventDefault();
    const errors = hasValues(credentials);
    if (errors.size !== 0) {
      setErrorMsg(errors);
      return;
    }

    const response = await doSingUp(credentials.id, credentials.password);

    if (response) {
      navigator('/formular', { state: { id: credentials.id, password: credentials.password } });
    } else {
      const error = handleError('wrong credentials');
      setErrorMsg(error);
    }
  }

  function handleChange(event: ChangeEvent<HTMLInputElement>) {
    setCredentials((prevLogin) => ({
      ...prevLogin,
      [event.target.name]: event.target.value,
    }));
  }

  return {
    getAccess,
    errorMsg,
    credentials,
    handleChange,
  };
}

export default useCourseCredential;
