// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { useState } from 'react';
import jwtService from '../Utils/JwtService';
import ProjectType from '../TypeDto/ProjectType';

async function getProjectsProf(eventId: number): Promise<Response> {
  const URL = jwtService().getEndpoint(`events/${eventId}/projects`);
  const header = jwtService().getHeader();
  const response = await fetch(URL, {
    method: 'GET',
    headers: header,
  });
  return response;
}
async function getProjects(eventId: number, password: string): Promise<Response> {
  const URL = jwtService().getEndpoint(`events/${eventId}/projects`);
  const pwd = { password };
  const response = await fetch(URL, {
    method: 'GET',
    headers: pwd,
  });
  return response;
}
async function updateProjects(
  id: number,
  eventId: number,
  name: string,
  description: string,
  customer: string,
  withContract: boolean,
): Promise<Response> {
  const header = jwtService().getHeader();
  const URL = jwtService().getEndpoint(`events/projects/${id}`);
  const body = JSON.stringify({ id, eventId, name, description, customer, withContract });
  const response = await fetch(URL, {
    method: 'PATCH',
    headers: header,
    body,
  });
  return response;
}

async function postProjects(
  eventId: number,
  name: string,
  description: string,
  customer: string,
  withContract: boolean,
): Promise<Response> {
  const header = jwtService().getHeader();
  const URL = jwtService().getEndpoint('events/projects');
  const body = JSON.stringify({ eventId, name, description, customer, withContract });
  const response = await fetch(URL, {
    method: 'POST',
    headers: header,
    body,
  });
  return response;
}

async function deleteProjects(id: string): Promise<Response> {
  const URL = jwtService().getEndpoint(`events/projects/${id}`);
  const header = jwtService().getHeader();

  const requestOptions = {
    method: 'DELETE',
    headers: header,
  };
  const response = await fetch(URL, requestOptions);
  return response;
}
function handleError(error: string): Map<string, string> {
  const resultMap = new Map<string, string>();
  if (error.includes('eventID') || error.includes('name')) {
    resultMap.set('eventID', error);
  }
  if (error.includes('name')) {
    resultMap.set('name', error);
  }
  return resultMap;
}

function useProject() {
  const [data, setData] = useState<ProjectType[]>([]);
  const [errorMsg, setErrorMsg] = useState(new Map<string, string>());
  const [successOpen, setSuccessOpen] = useState(false);
  const [errorOpen, setErrorOpen] = useState(false);

  async function getProjectData(eventId: number, password: string) {
    try {
      const response = await getProjects(eventId, password);
      if (response.ok) {
        const text = await response.text();
        const jsonData = await JSON.parse(text);
        setData(jsonData);
      }
    } catch (error) {
      throw new Error('Failed to get Project Data');
    }
  }

  async function getProjectDataProf(eventId: number): Promise<any> {
    const response = await getProjectsProf(eventId);
    if (response.ok) {
      const text = await response.text();
      const jsonData = await JSON.parse(text);
      setData(jsonData);
      return text;
    }
    return response;
  }

  async function postProjectData(
    eventId: number,
    name: string,
    description: string,
    customer: string,
    withContract: boolean,
  ): Promise<Response> {
    const response = await postProjects(eventId, name, description, customer, withContract);
    if (response.ok) {
      const text = await response.text();
      const jsonData = await JSON.parse(text);
      setData(jsonData);
      setSuccessOpen(true);
      return jsonData;
    }
    const error = await response.text();
    setErrorMsg(handleError(error));
    setErrorOpen(true);
    return response;
  }

  async function updateProjectData(
    id: number,
    eventId: number,
    name: string,
    description: string,
    customer: string,
    withContract: boolean,
  ): Promise<Response> {
    const response = await updateProjects(id, eventId, name, description, customer, withContract);
    if (response.ok) {
      const text = await response.text();
      const jsonData = await JSON.parse(text);
      setData(jsonData);
      setSuccessOpen(true);
      return jsonData;
    }
    const error = await response.text();
    setErrorMsg(handleError(error));
    setErrorOpen(true);
    return response;
  }

  async function deleteProjectData(id: string): Promise<Response> {
    const response = await deleteProjects(id);
    if (response !== undefined && response.ok) {
      setSuccessOpen(true);
      return response;
    }
    setErrorMsg(handleError('Error'));
    setErrorOpen(true);
    return response;
  }

  return {
    errorMsg,
    setErrorMsg,
    successOpen,
    setSuccessOpen,
    errorOpen,
    setErrorOpen,
    getProjectData,
    getProjectDataProf,
    postProjectData,
    updateProjectData,
    deleteProjectData,
    data,
    setData,
  };
}

export default useProject;
