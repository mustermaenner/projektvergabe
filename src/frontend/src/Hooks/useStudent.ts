// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

import { useState } from 'react';
import jwtService from '../Utils/JwtService';
import StudentEnrollmentDto from '../dto/StudentEnrollment.dto';

async function getStudents(eventID: number, assigned: boolean): Promise<Response> {
  const header = jwtService().getHeader();
  const URL = jwtService().getEndpoint(`events/${eventID}/enrollment?repo_assigned=${assigned}`);
  const response = await fetch(URL, {
    method: 'GET',
    headers: header,
  });
  return response;
}

async function postStudent(eventId: number, body: StudentEnrollmentDto, passwords: string): Promise<Response> {
  const URL = jwtService().getEndpoint(`events/${eventId}/studentenrollment`);

  const response = await fetch(URL, {
    method: 'POST',
    headers: { password: passwords, 'Content-type': 'application/json' },
    body: JSON.stringify(body),
  });
  return response;
}

async function deleteStudent(eventId: number, studentId: string): Promise<Response> {
  const header = jwtService().getHeader();
  const URL = jwtService().getEndpoint(`events/${eventId}/studentenrollment/${studentId}`);
  const response = await fetch(URL, {
    method: 'DELETE',
    headers: header,
  });
  return response;
}

function handleError(error: string): Map<string, string> {
  const resultMap = new Map<string, string>();
  if (error.includes('eventID') || error.includes('password')) {
    resultMap.set('eventID', error);
  }
  if (error.includes('student')) {
    resultMap.set('student', error);
  }
  return resultMap;
}

function useStudent() {
  const [studentData, setStudentData] = useState<StudentEnrollmentDto[]>([]);
  const [errorMsg, setErrorMsg] = useState(new Map<string, string>());
  const [successOpen, setSuccessOpen] = useState(false);
  const [errorOpen, setErrorOpen] = useState(false);

  async function getStudentData(eventID: number, assigned: boolean) {
    try {
      const response = await getStudents(eventID, assigned);
      if (response.ok) {
        const text = await response.text();
        const jsonData = await JSON.parse(text);
        setStudentData(jsonData);
      }
    } catch (error) {
      throw new Error('Failed to get Event Data');
    }
  }
  async function deleteStudentData(eventId: number, studentId: string) {
    const response = await deleteStudent(eventId, studentId);
    if (response.ok) {
      setSuccessOpen(true);
    } else {
      const error = await response.text();
      setErrorMsg(handleError(error));
      setErrorOpen(true);
    }
  }

  async function postStudentData(eventId: number, student: StudentEnrollmentDto, password: string) {
    const response = await postStudent(eventId, student, password);
    if (response.ok) {
      const text = await response.text();
      const jsonData = await JSON.parse(text);
      setStudentData(jsonData);
      setSuccessOpen(true);
    } else {
      const error = await response.text();
      setErrorMsg(handleError(error));
      setErrorOpen(true);
    }
  }

  return {
    errorMsg,
    setErrorMsg,
    successOpen,
    setSuccessOpen,
    errorOpen,
    setErrorOpen,
    getStudentData,
    postStudentData,
    deleteStudentData,
    studentData,
    setStudentData,
  };
}

export default useStudent;
