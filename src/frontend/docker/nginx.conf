# Copyright 2023 The Mustermänner Team. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

server {
  listen 80 default_server;
  listen [::]:80 default_server;
  server_name _;

  index index.html;

  keepalive_timeout 5;

  root /srv/frontend;

  location /api {
    try_files $uri @proxy_to_backend;
  }

  location / {
    try_files $uri $uri/ /index.html;
  }

  location @proxy_to_backend {
    proxy_redirect off;
    proxy_pass http://backend:8080;
  }

  error_page 500 502 503 504 /500.html;
  location = /500.html {
    root /srv/frontend;
  }
  error_page 404 /404.html;
  location = /404.html {
    root /srv/frontend;
  }
}
