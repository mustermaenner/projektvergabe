// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"fmt"
	"os"

	"backend/models"

	"github.com/gin-gonic/gin"
	"xorm.io/xorm"

	// db driver
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

func main() {
	fmt.Println("start database connection ...")
	x, err := xorm.NewEngine(os.Getenv("DATABASE_TYPE"), os.Getenv("DATABASE_CONFIG"))
	if err != nil {
		panic(err)
	}
	if err := x.Ping(); err != nil {
		panic(err)
	}

	x.Sync2(new(models.Assets),
		new(models.Enrollment),
		new(models.EnrollmentPriority),
		new(models.Event),
		new(models.Prof),
		new(models.ProfEvent),
		new(models.Project))

	// create routes
	e := gin.New()
	e.UseRawPath = true
	e.Use(gin.Recovery())

	registerHandler(x, e.Group("/api/v1"))

	fmt.Println("start server ...")
	if err := e.Run(":3333"); err != nil {
		panic(err)
	}
}
