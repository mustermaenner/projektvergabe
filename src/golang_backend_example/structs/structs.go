// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package structs

import (
	"time"

	"backend/models"
)

type (
	Event struct {
		ID          int
		Name        string
		Description string
		Password    string
		OpenUntil   time.Time
	}

	Project struct {
		ID          int
		EventId     int
		Name        string
		Description string
		Customer    string
	}

	Enrollment struct {
		FirstName         string
		LastName          string
		Mail              string
		MatriculationNr   int64
		StudentIdentifier string
		EventId           int
		Priorities        []EnrollmentPriority
		Contract          bool
		Street            string
		City              string
		CityCode          int
		Country           string
	}

	EnrollmentPriority struct {
		ProjectID int
		EventId   int
	}

	Prof struct {
		Name     string
		Mail     string
		Password string
		IsAdmin  bool
	}
)

func ToProf(m *models.Prof) *Prof {
	return &Prof{
		Name:    m.Name,
		Mail:    m.Mail,
		IsAdmin: m.IsAdmin,
	}
}

func ToEvent(m *models.Event) *Event {
	return &Event{
		ID:          m.ID,
		Name:        m.Name,
		Description: m.Description,
		OpenUntil:   m.OpenUntil,
	}
}
