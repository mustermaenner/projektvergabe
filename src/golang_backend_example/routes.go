// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"github.com/gin-gonic/gin"
	"xorm.io/xorm"
)

func registerHandler(x *xorm.Engine, rg *gin.RouterGroup) {
	events := rg.Group("/events")
	{
		events.GET("", listEvents(x))
		events.POST("")
		events.PATCH("/:eventID")
		events.DELETE("/:eventID")
	}

	projects := rg.Group("/events/:eventID/projects")
	{
		projects.GET("")
		projects.POST("")
		projects.PATCH("/:projectID")
		projects.DELETE("/:projectID")
	}

	student := rg.Group("/events/:eventID/student_enrollment")
	{
		student.GET("")
		student.POST("")
		student.DELETE("/:studentID")
	}

	enrollment := rg.Group("/events/:eventID/enrollment")
	{
		enrollment.GET("")
		enrollment.POST("")
		enrollment.PUT("")
		enrollment.DELETE("")
	}

	autoEnrollment := rg.Group("/events/:eventID/auto_enrollment")
	autoEnrollment.GET("")

	prof := rg.Group("/profs")
	{
		prof.GET("", listProfs(x))
		prof.POST("", createProf(x))
		prof.GET("/events/:eventID")
		prof.PUT("/:profID/events/:eventID")
		prof.DELETE("/:profID/events/:eventID")
	}
}
