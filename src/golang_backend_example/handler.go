// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"net/http"

	"backend/models"
	"backend/structs"

	"github.com/gin-gonic/gin"
	"xorm.io/xorm"
)

func listEvents(x *xorm.Engine) gin.HandlerFunc {
	return func(c *gin.Context) {
		events := make([]*models.Event, 0, 10)

		x.Find(&events)

		apiResp := make([]*structs.Event, len(events))
		for i := range events {
			apiResp[i] = structs.ToEvent(events[i])
		}

		c.JSON(http.StatusOK, apiResp)
	}
}

func listProfs(x *xorm.Engine) gin.HandlerFunc {
	return func(c *gin.Context) {
		profs := make([]*models.Prof, 0, 10)

		x.Find(&profs)

		apiResp := make([]*structs.Prof, len(profs))
		for i := range profs {
			apiResp[i] = structs.ToProf(profs[i])
		}

		c.JSON(http.StatusOK, apiResp)
	}
}

func createProf(x *xorm.Engine) gin.HandlerFunc {
	return func(c *gin.Context) {
		var prof structs.Prof
		if err := c.ShouldBindJSON(&prof); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		pwdHash, salt := hash(prof.Password)
		dbProf := &models.Prof{
			Name:         prof.Name,
			Mail:         prof.Mail,
			PasswordHash: pwdHash,
			PasswordSalt: salt,
		}

		if _, err := x.Insert(&dbProf); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, structs.ToProf(dbProf))
	}
}

func hash(pwd string) (string, string) {
	return "TODO", "TODO"
}
