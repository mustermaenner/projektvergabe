// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package models

import "time"

type (
	Prof struct {
		ID           int    `xorm:"pk autoincr"`
		Name         string `xorm:"NOT NULL"`
		Mail         string `xorm:"NOT NULL UNIQUE"`
		PasswordHash string `xorm:"NOT NULL"`
		PasswordSalt string `xorm:"NOT NULL"`
		IsAdmin      bool   `xorm:"NOT NULL DEFAULT false"`
	}

	Student struct {
		ID                int    `xorm:"pk autoincr"`
		FirstName         string `xorm:"NOT NULL"`
		LastName          string `xorm:"NOT NULL"`
		Mail              string `xorm:"NOT NULL"`
		MatriculationNr   int    `xorm:"NOT NULL"`
		StudentIdentifier string `xorm:"NOT NULL UNIQUE"`
		Street            string
		City              string
		CityCode          string
		Country           string
	}

	Enrollment struct {
		ID             int `xorm:"pk autoincr"`
		StudentID      int `xorm:"UNIQUE(e)"`
		EventID        int `xorm:"UNIQUE(e)"`
		Contract       bool
		FinalProjectID int
	}

	EnrollmentPriority struct {
		EnrollmentID int
		ProjectID    int
		Order        int
	}

	Project struct {
		ID       int    `xorm:"pk autoincr"`
		EventID  int    `xorm:"NOT NULL UNIQUE(p)"`
		Name     string `xorm:"NOT NULL UNIQUE(p)"`
		Customer string
	}

	Event struct {
		ID           int    `xorm:"pk autoincr"`
		Name         string `xorm:"NOT NULL UNIQUE"`
		Description  string
		PasswordHash string
		OpenUntil    time.Time
	}

	Assets struct {
		UUID      string
		EventID   int
		ProjectID int
	}

	ProfEvent struct {
		ProfID  int
		EventID int
	}
)
