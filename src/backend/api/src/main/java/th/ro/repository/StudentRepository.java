// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import th.ro.entity.Student;

/**
 * A repository for students
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@ApplicationScoped
public class StudentRepository implements PanacheRepository<Student> {
	@Inject EnrollmentRepository enrollmentRepository;

	/**
	 * Find a student by his matriculation number
	 *
	 * @param matriculationNr unique identifier of each student
	 * @return student
	 */
	public Student byStudentIdentifier(int matriculationNr) {
		return this.find("matriculationNr", matriculationNr).firstResult();
	}

	/**
	 * Update or insert a new student (if student already exists or not)
	 *
	 * @param newStudent new data of the student
	 * @return new persisted student
	 */
	public Student updateOrInsert(Student newStudent) {
		// TODO: validate/sanitize student

		var oldStudent = this.byStudentIdentifier(newStudent.getMatriculationNr());

		// if not exist, just insert
		if (oldStudent == null) {
			this.persist(newStudent);
			return newStudent;
		}

		// else just update ...
		// ... required
		oldStudent.setFirstName(newStudent.getFirstName());
		oldStudent.setLastName(newStudent.getLastName());
		oldStudent.setMail(newStudent.getMail());
		oldStudent.setMatriculationNr(newStudent.getMatriculationNr());

		// ... optional infos if set
		Optional.ofNullable(newStudent.getStreet()).ifPresent(oldStudent::setStreet);
		Optional.ofNullable(newStudent.getCity()).ifPresent(oldStudent::setCity);
		Optional.ofNullable(newStudent.getCityCode()).ifPresent(oldStudent::setCityCode);
		Optional.ofNullable(newStudent.getCountry()).ifPresent(oldStudent::setCountry);

		this.persist(oldStudent);
		return oldStudent;
	}

	/**
	 * Method for deleting a student if it´s not in an enrollment
	 *
	 * @param getMatriculationNr unique identifier of each student
	 */
	public void deleteIfUnused(int getMatriculationNr) {
		var student = this.byStudentIdentifier(getMatriculationNr);
		if (student == null) throw new NotFoundException("Student not found");

		if (enrollmentRepository.count("student_id", student.getId()) == 0) {
			this.delete(student);
		}
	}
}
