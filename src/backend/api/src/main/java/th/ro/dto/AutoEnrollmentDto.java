// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import lombok.Builder;
import lombok.Data;

/**
 * DTO for the auto enrollment
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @version %I%, %G%
 */
@Data
@Builder
public class AutoEnrollmentDto {
	ProjectDto project;
	StudentDto student;
}
