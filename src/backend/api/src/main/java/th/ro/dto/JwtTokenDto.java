// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * DTO for JwtTokens
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @version %I%, %G%
 */
@Data
@AllArgsConstructor
public class JwtTokenDto {
	String token;
}
