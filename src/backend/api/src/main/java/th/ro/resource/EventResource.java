// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import io.quarkus.security.UnauthorizedException;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.ParameterIn;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirements;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.security.SecuritySchemes;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import th.ro.common.Jwt;
import th.ro.controller.EventController;
import th.ro.dto.CreateEventDto;
import th.ro.dto.EventDto;
import th.ro.dto.EventProfDto;

/**
 * Resource class for events
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Path("/events")
@Produces(MediaType.APPLICATION_JSON) // not necessary, default value
@Consumes(MediaType.APPLICATION_JSON) // not necessary, default value
@SecuritySchemes(
		value = {
			@SecurityScheme(
					securitySchemeName = "bearer",
					type = SecuritySchemeType.HTTP,
					scheme = "bearer",
					bearerFormat = "jwt"),
		})
@Tag(name = "Event Resource", description = "Everything you can do with a event")
public class EventResource {

	@Inject EventController eventController;

	@Inject Jwt jwt;

	/**
	 * list all events
	 *
	 * @return list of all events
	 */
	@GET
	@Operation(
			description = "get list of all events \\\nThis resource is for the Students",
			summary = "Get list of events",
			operationId = "listEvents")
	@APIResponses(
			@APIResponse(
					responseCode = "200",
					content = @Content(schema = @Schema(implementation = EventDto.class))))
	public Response list() {
		var dto = eventController.listAllEvents();
		return Response.status(200).entity(dto).build();
	}

	/**
	 * list all or only open events
	 *
	 * @param withClosed all or only open events
	 * @param auth auth string
	 * @return list of events
	 */
	@GET
	@Path("/prof")
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description =
					"get list of all events with the password of the event\\\nThis resource is for the Profs",
			summary = "Get list of events for Prof",
			operationId = "listEventsProf")
	@Parameter(
			name = "withClosed",
			required = true,
			description = "If true also the closed events are shown",
			in = ParameterIn.QUERY,
			schema = @Schema(type = SchemaType.BOOLEAN))
	@APIResponse(
			responseCode = "200",
			content = @Content(schema = @Schema(implementation = EventProfDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	public Response listProf(
			@QueryParam("withClosed") boolean withClosed,
			@HeaderParam("Authorization") String auth) {
		return Response.status(200)
				.entity(eventController.listAllEvents(withClosed, getProfIdFromJwt(auth)))
				.build();
	}

	/**
	 * Add a event
	 *
	 * @param event event
	 * @param auth auth string
	 * @return new added event
	 */
	@POST
	@Transactional
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Add a new Event",
			summary = "Add new event",
			operationId = "createEvent")
	@RequestBody(
			description = "The new Event",
			required = true,
			content = @Content(schema = @Schema(implementation = CreateEventDto.class)))
	@APIResponse(
			responseCode = "200",
			content = @Content(schema = @Schema(implementation = EventProfDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "409", description = "Conflict")
	public Response addEvent(CreateEventDto event, @HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto = eventController.saveEvent(event, getProfIdFromJwt(auth));
		return Response.status(201).entity(dto).build();
	}

	/**
	 * Update an event
	 *
	 * @param eventId id of the event
	 * @param event new data for the event
	 * @param auth auth string
	 * @return updated event
	 */
	@PATCH
	@Transactional
	@Path("/{eventId}")
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Patch an existing Event",
			summary = "Patch event",
			operationId = "patchEvent")
	@RequestBody(
			description = "The patched Event",
			required = true,
			content = @Content(schema = @Schema(implementation = CreateEventDto.class)))
	@Parameter(
			name = "eventId",
			description = "The Id of the Event which should be patched",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(
			responseCode = "200",
			content = @Content(schema = @Schema(implementation = EventProfDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "403", description = "Forbidden")
	public Response updateEvent(
			@PathParam("eventId") Long eventId,
			CreateEventDto event,
			@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto = eventController.updateEvent(eventId, event, getProfIdFromJwt(auth));
		return Response.status(200).entity(dto).build();
	}

	/**
	 * Delete an event
	 *
	 * @param eventId id of the event
	 * @param auth auth string
	 * @return no contend status
	 */
	@DELETE
	@Transactional
	@Path("/{eventId}")
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Delete an existing Event",
			summary = "Delete event",
			operationId = "deleteEvent")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event which should be deleted",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(responseCode = "204", description = "Gone")
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "403", description = "Forbidden")
	public Response deleteEvent(
			@PathParam("eventId") Long eventId, @HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		eventController.deleteEvent(eventId, getProfIdFromJwt(auth));
		return Response.status(204).build();
	}

	/**
	 * get id of a prof from jwt
	 *
	 * @param token jwt token
	 * @return id of the prof
	 */
	private Long getProfIdFromJwt(String token) {
		return jwt.getData(token).get("id", Long.class);
	}

	/**
	 * check if token is valid
	 *
	 * @param token token for validation
	 */
	private void isValidToken(String token) {
		if (token == null || !jwt.verify(StringUtils.remove(token, "Bearer"))) {
			throw new UnauthorizedException();
		}
	}
}
