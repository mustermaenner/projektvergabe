// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.entity;

import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.*;

/**
 * Student entity class
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@NotNull @Column(nullable = false)
	private String firstName;

	@NotNull @Column(nullable = false)
	private String lastName;

	@NotNull @Column(nullable = false, unique = true)
	private String mail;

	@NotNull @Column(nullable = false, unique = true)
	private int matriculationNr;

	@NotNull @Column(unique = true, nullable = false)
	private String studentIdentifier;

	private String street;
	private String city;
	private Integer cityCode;
	private String country;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Student student = (Student) o;
		return id == student.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
