// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

/**
 * DTO for enrollments
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Data
@Builder
@AllArgsConstructor
public class EnrollmentDto {
	// student part
	@NotEmpty @NonNull private String firstName;
	@NotEmpty @NonNull private String lastName;
	@NotEmpty @Email @NonNull private String mail;
	@NotNull private int matriculationNr;
	@NotEmpty @NonNull private String studentIdentifier;

	// raw enrollment part
	private Long eventId;
	@Valid @NonNull private List<EnrollmentPriorityDto> priorities;
	private boolean contract;
	private String street;
	private String city;
	private Integer cityCode;
	private String country;
	private Long finalProject;
	private String uuidForChanges;

	@Positive @Max(14)
	private int semester;

	public EnrollmentDto() {}
}
