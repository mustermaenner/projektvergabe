// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.entity;

import java.util.Objects;
import javax.persistence.*;
import lombok.*;

/**
 * JwtSecret entity class
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @version %I%, %G%
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@Entity
@NoArgsConstructor
@Builder
public class JwtSecret {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	Long id;

	@Column(nullable = false, length = 10000)
	String secret;

	@Column(nullable = false)
	String salt;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		JwtSecret jwtSecret = (JwtSecret) o;
		return id == jwtSecret.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
