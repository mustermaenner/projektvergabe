// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import com.google.common.annotations.VisibleForTesting;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import lombok.NonNull;
import one.util.streamex.StreamEx;
import th.ro.dto.CreateEventDto;
import th.ro.dto.EventDto;
import th.ro.dto.EventProfDto;
import th.ro.entity.Event;
import th.ro.entity.Prof;
import th.ro.exceptions.ConflictException;
import th.ro.repository.EventRepository;
import th.ro.repository.ProfRepository;

/**
 * Event Controller
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @see th.ro.resource.EventResource
 * @version %I%, %G%
 */
@ApplicationScoped
public class EventController {

	@Inject EventRepository eventRepository;
	@Inject ProfRepository profRepository;

	/**
	 * list all Events
	 *
	 * @return list of EventDtos
	 */
	public List<EventDto> listAllEvents() {
		return StreamEx.of(eventRepository.listAll())
				.filter(it -> it.getOpenUntil().after(new Date()))
				.map(
						it ->
								EventDto.builder()
										.id(it.getId())
										.openUntil(it.getOpenUntil())
										.description(it.getDescription())
										.name(it.getName())
										.build())
				.toList();
	}

	/**
	 * list all Events with check if events are closed
	 *
	 * @param withClosed check if event is closed
	 * @param profId id of the prof
	 * @return list of EventProfDtos
	 */
	public List<EventProfDto> listAllEvents(boolean withClosed, Long profId) {
		List<Event> events = eventRepository.listAll();
		events =
				StreamEx.of(events)
						.filter(
								it ->
										it.getProfs().stream()
												.map(Prof::getId)
												.toList()
												.contains(profId))
						.toList();
		if (!withClosed) {
			events = StreamEx.of(events).filter(it -> it.getOpenUntil().after(new Date())).toList();
		}
		return StreamEx.of(events).map(this::transformToEventProfDto).toList();
	}

	/**
	 * save an event
	 *
	 * @param eventDto EventDto
	 * @param profIdid of the Prof
	 * @return EventProfDto
	 */
	public EventProfDto saveEvent(CreateEventDto eventDto, Long profId) {
		if (eventRepository.find("name", eventDto.getName()).firstResult() != null) {
			throw new ConflictException("Event already exists");
		}
		List<Prof> profs =
				StreamEx.of(eventDto.getProfs())
						.map(it -> profRepository.findById(it))
						.append(profRepository.findById(profId))
						.nonNull()
						.distinct(Prof::getId)
						.toList();
		var event =
				Event.builder()
						.name(eventDto.getName())
						.description(eventDto.getDescription())
						.password(eventDto.getPassword())
						.openUntil(eventDto.getOpenUntil())
						.profs(profs)
						.build();
		eventRepository.persist(event);
		return transformToEventProfDto(event);
	}

	/**
	 * update an Event
	 *
	 * @param eventId id of the event
	 * @param eventDto EventDto
	 * @param profId id of the Prof
	 * @return EventProfDto
	 */
	public EventProfDto updateEvent(Long eventId, CreateEventDto eventDto, Long profId) {
		Event event = eventRepository.find("id", eventId).firstResult();
		if (event == null) {
			throw new NotFoundException("Project not found");
		}
		boolean isInEvent =
				StreamEx.of(event.getProfs()).anyMatch(it -> Objects.equals(it.getId(), profId));
		if (!isInEvent) {
			throw new ForbiddenException("Prof not in Event");
		}
		if (eventDto.getName() != null) event.setName(eventDto.getName());
		if (eventDto.getDescription() != null) event.setDescription(eventDto.getDescription());
		if (eventDto.getPassword() != null) event.setPassword(eventDto.getPassword());
		if (eventDto.getOpenUntil() != null) event.setOpenUntil(eventDto.getOpenUntil());
		event.setProfs(
				StreamEx.of(eventDto.getProfs())
						.map(it -> profRepository.findById(it))
						.append(profRepository.findById(profId))
						.distinct(Prof::getId)
						.nonNull()
						.toList());
		eventRepository.persist(event);
		return transformToEventProfDto(event);
	}

	/**
	 * delete an Event
	 *
	 * @param eventId id of the event
	 * @param profId id of the prof
	 */
	public void deleteEvent(Long eventId, Long profId) {
		Event eventForDelete = eventRepository.findById(eventId);
		boolean isInEvent =
				StreamEx.of(eventForDelete.getProfs())
						.anyMatch(it -> Objects.equals(it.getId(), profId));
		if (!isInEvent) {
			throw new ForbiddenException("Prof not in Event");
		}
		eventRepository.delete(eventForDelete);
	}

	/**
	 * transform an event to an EventProfDto
	 *
	 * @param event Event
	 * @return EventProfDto
	 */
	private EventProfDto transformToEventProfDto(@NonNull Event event) {
		return EventProfDto.builder()
				.password(event.getPassword())
				.id(event.getId())
				.openUntil(event.getOpenUntil())
				.description(event.getDescription())
				.name(event.getName())
				.profs(event.getProfs().stream().map(Prof::getId).toList())
				.build();
	}

	@VisibleForTesting
	void setEventRepository(EventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}

	@VisibleForTesting
	void setProfRepository(ProfRepository profRepository) {
		this.profRepository = profRepository;
	}
}
