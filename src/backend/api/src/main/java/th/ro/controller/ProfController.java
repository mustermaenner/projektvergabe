// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import io.quarkus.elytron.security.common.BcryptUtil;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import one.util.streamex.StreamEx;
import th.ro.common.Jwt;
import th.ro.common.Transform;
import th.ro.dto.CreateProfDto;
import th.ro.dto.ProfDto;
import th.ro.entity.Event;
import th.ro.entity.Prof;
import th.ro.repository.EventRepository;
import th.ro.repository.ProfRepository;

/**
 * Prof Controller
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @see th.ro.resource.ProfResource
 * @version %I%, %G%
 */
@ApplicationScoped
public class ProfController {

	@Inject Jwt jwt;

	@Inject ProfRepository profRepository;

	@Inject EventRepository eventRepository;

	/**
	 * list all Profs
	 *
	 * @return list of ProfDtos
	 */
	public List<ProfDto> listAllProfs() {
		return StreamEx.of(profRepository.listAll()).map(Transform.profToProfDto).toList();
	}

	/**
	 * save a Prof
	 *
	 * @param prof Prof
	 * @return ProfDto
	 */
	public ProfDto saveProf(CreateProfDto prof) {
		if (profRepository.count("name", prof.getName()) != 0) {
			throw new ClientErrorException("Name Already Used", Response.Status.CONFLICT);
		}
		if (profRepository.count("mail", prof.getMail()) != 0) {
			throw new ClientErrorException("Mail Already Used", Response.Status.CONFLICT);
		}
		var profEntity =
				Prof.builder()
						.name(prof.getName())
						.mail(prof.getMail())
						.roles(prof.getRole())
						.password(BcryptUtil.bcryptHash(prof.getPassword()))
						.build();
		profRepository.persist(profEntity);
		return Transform.profToProfDto.apply(profEntity);
	}

	/**
	 * list profs with the id of an event
	 *
	 * @param id id of the event
	 * @return list of ProfDtos
	 */
	public List<ProfDto> listProfsWithEventId(Long id) {
		Event event = eventRepository.findById(id);
		if (event == null) {
			throw new NotFoundException("Event was not found");
		}
		return event.getProfs().stream().map(Transform.profToProfDto).toList();
	}

	/**
	 * add prof to an event
	 *
	 * @param profId id of the prof
	 * @param eventId id of the event
	 * @return ProfDto
	 */
	public ProfDto addProfToEvent(Long profId, Long eventId) {
		Prof prof = profRepository.findById(profId);
		if (prof == null) {
			throw new NotFoundException("Prof was not found");
		}
		Event event = eventRepository.findById(eventId);
		if (event == null) {
			throw new NotFoundException("Event was not found");
		}
		event.getProfs().add(prof);

		return Transform.profToProfDto.apply(prof);
	}

	/**
	 * remove a prof from an event
	 *
	 * @param profId id of the prof
	 * @param eventId id of the event
	 */
	public void removeProfFromEvent(Long profId, Long eventId) {
		Event event = eventRepository.findById(eventId);
		Prof prof = profRepository.findById(profId);
		event.getProfs().remove(prof);
		eventRepository.persist(event);
	}

	/**
	 * get jwt for a prof
	 *
	 * @param name name of the prof
	 * @return jwt for prof
	 */
	public String jwtForProf(String name) {
		Prof prof = profRepository.find("name", name).firstResult();
		if (prof == null) {
			throw new NotFoundException("Prof was not found");
		}
		return jwt.sign(
				Map.of("name", prof.getName(), "role", prof.getRoles(), "id", prof.getId()));
	}

	/**
	 * renew token
	 *
	 * @param auth auth string
	 * @return renewed token
	 */
	public String renewToken(String auth) {
		String name = jwt.getData(auth).get("name", String.class);
		return jwtForProf(name);
	}
}
