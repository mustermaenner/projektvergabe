// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import lombok.Builder;
import lombok.Data;

/**
 * DTO for profs
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @version %I%, %G%
 */
@Builder
@Data
public class ProfDto {
	private Long id;

	private String name;

	private String mail;

	private String roles;
}
