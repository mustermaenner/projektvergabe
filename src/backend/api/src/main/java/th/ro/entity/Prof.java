// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.entity;

import io.quarkus.security.jpa.Password;
import io.quarkus.security.jpa.Roles;
import io.quarkus.security.jpa.UserDefinition;
import io.quarkus.security.jpa.Username;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;

/**
 * Prof entity class
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Entity
@UserDefinition
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Prof {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@NotNull @Column(nullable = false, unique = true)
	@Username
	private String name;

	@NotNull @Column(unique = true, nullable = false)
	private String mail;

	@NotNull @Column(nullable = false)
	@Password
	private String password;

	@NotNull @ColumnDefault(value = "false")
	@Roles
	private String roles;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(
			name = "prof_event",
			joinColumns = @JoinColumn(name = "prof_id"),
			inverseJoinColumns = @JoinColumn(name = "event_id"))
	@ToString.Exclude
	private List<Event> events;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Prof prof = (Prof) o;
		return id == prof.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
