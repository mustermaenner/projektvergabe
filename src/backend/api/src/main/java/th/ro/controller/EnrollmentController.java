// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import distribution_algorithm.Distribute;
import distribution_algorithm.TooFewProjectsWithoutContract;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import one.util.streamex.StreamEx;
import org.apache.commons.math3.util.Pair;
import th.ro.common.Transform;
import th.ro.dto.AutoEnrollmentDto;
import th.ro.dto.EnrollmentDto;
import th.ro.dto.ProjectDto;
import th.ro.entity.*;
import th.ro.repository.*;

/**
 * Enrollment Controller
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @see th.ro.resource.EnrollmentResource
 * @version %I%, %G%
 */
@ApplicationScoped
public class EnrollmentController {

	@Inject EnrollmentRepository enrollmentRepository;

	@Inject EnrollmentPriorityRepository enrollmentPriorityRepository;

	@Inject StudentRepository studentRepository;

	@Inject ProjectRepository projectRepository;

	@Inject EventRepository eventRepository;

	/**
	 * get enrolled students
	 *
	 * @param eventId id of the event
	 * @param repoAssigned check if repo is assigned
	 * @param profId id of the prof
	 * @return enrolled students
	 */
	public List<EnrollmentDto> getEnrolledStudents(
			Long eventId, boolean repoAssigned, Long profId) {
		profHasAccess(eventId, profId);
		List<Enrollment> enrollments = null;
		if (repoAssigned) {
			enrollments =
					StreamEx.of(enrollmentRepository.list("event_id", eventId))
							.filter(it -> it.getFinalProject() != null)
							.toList();
		} else {
			enrollments =
					StreamEx.of(enrollmentRepository.list("event_id", eventId))
							.filter(it -> it.getFinalProject() == null)
							.toList();
		}

		return StreamEx.of(enrollments).map(Transform.enrollmentToEnrollmentDto).toList();
	}

	/**
	 * overwrite enrolled students
	 *
	 * @param newStudentList new Student List
	 * @param eventId id of the event
	 * @param profId id of the prof
	 * @return overwritten list of enrolled students
	 */
	public List<EnrollmentDto> overwriteEnrolledStudents(
			List<EnrollmentDto> newStudentList, Long eventId, Long profId) {
		profHasAccess(eventId, profId);
		StreamEx.of(newStudentList)
				.forEach(
						it -> {
							// 1. Update Student Information
							Student student =
									studentRepository.byStudentIdentifier(it.getMatriculationNr());
							if (student == null) {
								throw new NotFoundException("Student wasn't found");
							}
							studentRepository.updateOrInsert(
									Transform.studentFromEnrollmentDto.apply(it));
							// 2. Find enrollment for student
							Enrollment enrollment =
									enrollmentRepository.findByStudentIdAndEventId(
											student.getId(), eventId);
							// 3. Delete all enrollment prios for Student
							enrollmentPriorityRepository.delete(
									"enrollment_id", enrollment.getId());
							// 4. Create new enrollment prios for Student
							it.getPriorities()
									.forEach(
											it1 -> {
												enrollmentPriorityRepository.persist(
														EnrollmentPriority.builder()
																.project(
																		projectRepository.findById(
																				it1.getProjectId()))
																.enrollment(enrollment)
																.priority(it1.getPriority())
																.build());
											});
						});
		// 5. Fetch all saved data from DB
		return StreamEx.of(enrollmentRepository.list("event_id", eventId))
				.map(Transform.enrollmentToEnrollmentDto)
				.toList();
	}

	/**
	 * enroll Student
	 *
	 * @param eventId id of the event
	 * @param projectId id of the project
	 * @param studentId id of the student
	 * @param profId id of the prof
	 * @return enrolled student
	 */
	public EnrollmentDto enrollStudent(Long eventId, Long projectId, Long studentId, Long profId) {
		profHasAccess(eventId, profId);
		Enrollment enrollment = enrollmentRepository.findByStudentIdAndEventId(studentId, eventId);
		if (enrollment == null) {
			throw new NotFoundException("Student or Event not found");
		}
		Project project = projectRepository.findById(projectId);
		if (project == null) {
			throw new NotFoundException("Project not found");
		}
		enrollment.setFinalProject(project);
		enrollmentRepository.persist(enrollment);
		return Transform.enrollmentToEnrollmentDto.apply(enrollment);
	}

	/**
	 * delete an enrollment
	 *
	 * @param eventId id of the event
	 * @param studentId id of the student
	 * @param profId id of the prof
	 */
	// delete complete enrollment
	public void deleteEnrollment(Long eventId, Long studentId, Long profId) {
		profHasAccess(eventId, profId);
		// 1. find student
		var student = studentRepository.findById((long) studentId);
		if (student == null) throw new NotFoundException("student not found");
		// 2. find enrollment to alter
		var enrollment = enrollmentRepository.findByStudentIdAndEventId(studentId, eventId);
		if (enrollment == null) {
			throw new NotFoundException("User not in Event");
		}
		// 3. delete enrollment
		enrollmentPriorityRepository.delete("enrollment_id", enrollment.getId());
		enrollmentRepository.delete(enrollment);
		// 4. delete student if not needed anymore
		studentRepository.deleteIfUnused(student.getMatriculationNr());
	}

	/**
	 * auto enrollment with distribution algorithm
	 *
	 * @param eventId id of the event
	 * @param profId id of the prof
	 * @param maxStudentPerProject max count of students per project
	 * @param minStudentPerProject min count of students per project
	 * @see Distribute
	 * @return list of auto enrolled students
	 */
	public List<AutoEnrollmentDto> autoEnroll(
			Long eventId, Long profId, int maxStudentPerProject, int minStudentPerProject) {
		profHasAccess(eventId, profId);
		List<Enrollment> enrollments = enrollmentRepository.list("event_id", eventId);
		List<distribution_algorithm.Project> projectsForAlg =
				StreamEx.of(projectRepository.list("event_id", eventId))
						.map(
								it ->
										new distribution_algorithm.Project(
												it.getId(), it.isWithContract()))
						.toList();
		List<distribution_algorithm.Student> studentsForAlg =
				StreamEx.of(enrollments)
						.map(
								it -> {
									List<distribution_algorithm.Project> projects =
											StreamEx.of(it.getEnrollmentPriorities())
													.sortedBy(EnrollmentPriority::getPriority)
													.map(
															it1 ->
																	StreamEx.of(projectsForAlg)
																			.findFirst(
																					it2 ->
																							Objects
																									.equals(
																											it2
																													.ID(),
																											it1.getProject()
																													.getId()))
																			.get())
													.toImmutableList();
									return new distribution_algorithm.Student(
											it.getStudent().getId(),
											it.isContract(),
											ImmutableList.copyOf(projects));
								})
						.toList();
		List<Pair<distribution_algorithm.Project, List<distribution_algorithm.Student>>>
				distribution = List.of();
		try {
			distribution =
					new Distribute()
							.distributeStudents(
									studentsForAlg,
									projectsForAlg,
									maxStudentPerProject,
									minStudentPerProject);
		} catch (TooFewProjectsWithoutContract e) {
			throw new BadRequestException("Too TooFewProjectsWithoutContract", e);
		}
		List<AutoEnrollmentDto> result = new LinkedList<>();
		for (Pair<distribution_algorithm.Project, List<distribution_algorithm.Student>> pair :
				distribution) {
			ProjectDto projectDto =
					Transform.projectToProjectDto.apply(
							projectRepository.findById(pair.getFirst().ID()));
			StreamEx.of(pair.getSecond())
					.map(it -> studentRepository.findById(it.studentId()))
					.map(Transform.studentToStudentDto)
					.forEach(
							it ->
									result.add(
											AutoEnrollmentDto.builder()
													.student(it)
													.project(projectDto)
													.build()));
		}
		return result;
	}

	/**
	 * check if prof has access
	 *
	 * @param eventId id of the event
	 * @param profId id of the prof
	 */
	private void profHasAccess(Long eventId, Long profId) {
		Event event = eventRepository.findById(eventId);
		if (event == null) {
			throw new NotFoundException("Event not found");
		}
		if (event.getProfs().stream().noneMatch(it -> Objects.equals(it.getId(), profId))) {
			throw new ForbiddenException("Prof not in Event");
		}
	}

	@VisibleForTesting
	void setEventRepository(EventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}
}
