// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * DTO for creating a prof
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @version %I%, %G%
 */
@Data
@Builder
@AllArgsConstructor
public class CreateProfDto {
	String name;
	String mail;
	String password;
	String role;

	public CreateProfDto() {}
}
