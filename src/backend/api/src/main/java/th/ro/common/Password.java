// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.common;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import org.wildfly.security.password.PasswordFactory;
import org.wildfly.security.password.interfaces.BCryptPassword;
import org.wildfly.security.password.spec.EncryptablePasswordSpec;
import org.wildfly.security.password.spec.IteratedSaltedPasswordAlgorithmSpec;

/**
 * Password class for hashing password with BCrypt
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
public class Password {

	/**
	 * Method for hashing password with BCrypt
	 *
	 * @param password password string
	 * @return hashed password
	 * @throws NoSuchAlgorithmException algorithm is requested but is not available in the
	 *     environment
	 * @throws InvalidKeySpecException invalid key specifications
	 */
	public static String hashPasswordWithBCrypt(String password)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		PasswordFactory passwordFactory =
				PasswordFactory.getInstance(BCryptPassword.ALGORITHM_BCRYPT);
		int iterationCount = 10;

		byte[] salt = new byte[BCryptPassword.BCRYPT_SALT_SIZE];
		SecureRandom random = new SecureRandom();
		random.nextBytes(salt);

		IteratedSaltedPasswordAlgorithmSpec iteratedAlgorithmSpec =
				new IteratedSaltedPasswordAlgorithmSpec(iterationCount, salt);
		EncryptablePasswordSpec encryptableSpec =
				new EncryptablePasswordSpec(password.toCharArray(), iteratedAlgorithmSpec);

		BCryptPassword original =
				(BCryptPassword) passwordFactory.generatePassword(encryptableSpec);

		byte[] hash = original.getHash();

		Base64.Encoder encoder = Base64.getEncoder();
		return encoder.encodeToString(hash);
	}
}
