// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import io.quarkus.security.UnauthorizedException;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.StringUtils;
import th.ro.common.Transform;
import th.ro.dto.EnrollmentDto;
import th.ro.dto.EnrollmentPriorityDto;
import th.ro.entity.Enrollment;
import th.ro.entity.EnrollmentPriority;
import th.ro.entity.Event;
import th.ro.exceptions.ConflictException;
import th.ro.repository.*;

/**
 * Student Controller
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @see th.ro.resource.StudentResource
 * @version %I%, %G%
 */
@ApplicationScoped
public class StudentController {

	@Inject EnrollmentRepository enrollmentRepository;

	@Inject StudentRepository studentRepository;

	@Inject EventRepository eventRepository;

	@Inject EnrollmentPriorityRepository enrollmentPriorityRepository;

	@Inject ProjectRepository projectRepository;

	/**
	 * find student information
	 *
	 * @param eventId id of the event
	 * @param studentId id of the student
	 * @param password Password
	 * @return EnrollmentDto
	 */
	public EnrollmentDto findStudentInformation(String eventId, String studentId, String password) {
		Enrollment enrollment =
				enrollmentRepository.findByStudentIdAndEventId(
						Long.parseLong(studentId), Long.parseLong(eventId));
		if (enrollment == null) {
			throw new NotFoundException("Not item found with studentId and eventId");
		}
		if (!StringUtils.equals(enrollment.getUuidForChange(), password)) {
			throw new ForbiddenException("Password does not match");
		}
		return Transform.enrollmentToEnrollmentDto.apply(enrollment);
	}

	/**
	 * add a student
	 *
	 * @param enrollmentDTO EnrollmentDto
	 * @param password Password
	 * @return EnrollmentDto
	 */
	public EnrollmentDto addStudent(EnrollmentDto enrollmentDTO, String password) {
		// 0.
		if (StreamEx.of(enrollmentDTO.getPriorities())
				.map(EnrollmentPriorityDto::getPriority)
				.distinct(2)
				.findAny()
				.isPresent()) {
			throw new BadRequestException(
					"Priority list can not contain the same Priorities more than once");
		}

		// 1. check & get event
		Event event = eventRepository.findById(enrollmentDTO.getEventId());
		if (event == null) {
			throw new NotFoundException("Event could not be found");
		}
		if (!StringUtils.equals(event.getPassword(), password)) {
			throw new UnauthorizedException("Password is not Correct");
		}
		// 2. check & get projects
		for (EnrollmentPriorityDto priority : enrollmentDTO.getPriorities()) {
			var project = projectRepository.findById(priority.getProjectId());
			if (project == null)
				throw new NotFoundException(
						"project of priority list not found: " + priority.getProjectId());
			if (!project.getEvent().equals(event))
				throw new BadRequestException(
						"Project is in wrong Event: " + priority.getProjectId());
		}

		// 3. persist student from enrollment
		var student =
				studentRepository.updateOrInsert(
						Transform.studentFromEnrollmentDto.apply(enrollmentDTO));

		// 4. check if student already enrolled
		if (enrollmentRepository.findByStudentIdAndEventId(student.getId(), event.getId())
				!= null) {
			throw new ConflictException("Student already enrolled");
		}

		// 5. create enrollment and persist
		var enrollment = Transform.createEnrollment(event, student, null);
		enrollment.setContract(enrollmentDTO.isContract());
		enrollment.setSemester(enrollmentDTO.getSemester());
		enrollmentRepository.persist(enrollment);

		// 6. create priorities and persist
		List<EnrollmentPriority> enrollmentPriorities =
				Transform.priorityFromDto(
						enrollmentDTO,
						enrollment,
						projectRepository.list("event_id", event.getId()));
		enrollmentPriorityRepository.persist(enrollmentPriorities);

		// 7. return enrollment from db as dto
		return Transform.enrollmentToEnrollmentDto.apply(
				enrollmentRepository.findById(enrollment.getId()));
	}

	/**
	 * delete an Enrollment
	 *
	 * @param eventId id of the event
	 * @param studentId id of the student
	 * @param password Password
	 */
	public void deleteEnrollment(Long eventId, Long studentId, String password) {
		Enrollment enrollment = enrollmentRepository.findByStudentIdAndEventId(studentId, eventId);
		if (enrollment == null) {
			throw new NotFoundException("Enrollment not found");
		}
		if (!StringUtils.equals(enrollment.getUuidForChange(), password)) {
			throw new ForbiddenException("Password does not match");
		}
		enrollmentRepository.delete(enrollment);
	}
}
