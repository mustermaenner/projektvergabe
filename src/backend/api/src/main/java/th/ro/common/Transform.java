// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.common;

import java.util.*;
import java.util.function.Function;
import javax.ws.rs.BadRequestException;
import one.util.streamex.StreamEx;
import th.ro.dto.*;
import th.ro.entity.*;

/**
 * Transform class used to used for DTO transformation
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
public final class Transform {

	public static Function<Enrollment, EnrollmentDto> enrollmentToEnrollmentDto =
			enrollment -> {
				Objects.requireNonNull(enrollment);
				List<EnrollmentPriorityDto> enrollmentPrioritiesDto =
						StreamEx.of(
										Optional.ofNullable(enrollment.getEnrollmentPriorities())
												.orElse(List.of()))
								.map(
										it ->
												EnrollmentPriorityDto.builder()
														.projectId(it.getProject().getId())
														.priority(it.getPriority())
														.build())
								.sortedBy(EnrollmentPriorityDto::getPriority)
								.toList();
				EnrollmentDto.EnrollmentDtoBuilder enrollmentDtoBuilder =
						EnrollmentDto.builder()
								.firstName(enrollment.getStudent().getFirstName())
								.lastName(enrollment.getStudent().getLastName())
								.mail(enrollment.getStudent().getMail())
								.matriculationNr(enrollment.getStudent().getMatriculationNr())
								.studentIdentifier(enrollment.getStudent().getStudentIdentifier())
								// mandatory enrollment part
								.eventId(enrollment.getEvent().getId())
								.priorities(enrollmentPrioritiesDto)
								.contract(enrollment.isContract())
								.finalProject(
										Optional.ofNullable(enrollment.getFinalProject())
												.map(Project::getId)
												.orElse(null))
								.uuidForChanges(enrollment.getUuidForChange())
								.semester(enrollment.getSemester());
				// optional student part for contracts
				if (enrollment.isContract()) {
					enrollmentDtoBuilder
							.street(enrollment.getStudent().getStreet())
							.city(enrollment.getStudent().getCity())
							.cityCode(enrollment.getStudent().getCityCode())
							.country(enrollment.getStudent().getCountry());
				}

				return enrollmentDtoBuilder.build();
			};

	public static Function<EnrollmentDto, Student> studentFromEnrollmentDto =
			enrollmentDto ->
					Student.builder()
							// required
							.firstName(enrollmentDto.getFirstName())
							.lastName(enrollmentDto.getLastName())
							.mail(enrollmentDto.getMail())
							.matriculationNr(enrollmentDto.getMatriculationNr())
							.studentIdentifier(enrollmentDto.getStudentIdentifier())
							// contract details
							.street(enrollmentDto.getStreet())
							.city(enrollmentDto.getCity())
							.cityCode(enrollmentDto.getCityCode())
							.country(enrollmentDto.getCountry())
							.build();

	public static Function<Student, StudentDto> studentToStudentDto =
			student ->
					StudentDto.builder()
							.id(student.getId())
							.firstName(student.getFirstName())
							.lastName(student.getLastName())
							.mail(student.getMail())
							.matriculationNr(student.getMatriculationNr())
							.studentIdentifier(student.getStudentIdentifier())
							.street(student.getStreet())
							.city(student.getCity())
							.cityCode(student.getCityCode())
							.country(student.getCountry())
							.build();

	public static Function<Project, ProjectDto> projectToProjectDto =
			project ->
					ProjectDto.builder()
							.id(project.getId())
							.eventId(project.getEvent().getId())
							.isWithContract(project.isWithContract())
							.customer(project.getCustomer())
							.description(project.getDescription())
							.name(project.getName())
							.build();

	public static Function<Prof, ProfDto> profToProfDto =
			prof ->
					ProfDto.builder()
							.id(prof.getId())
							.roles(prof.getRoles())
							.mail(prof.getMail())
							.name(prof.getName())
							.build();

	/**
	 * get the priority from EnrollmentDTO
	 *
	 * @param enrollmentDto EnrollmentDto
	 * @param enrollment Enrollment
	 * @param projects Projects
	 * @return result
	 */
	public static List<EnrollmentPriority> priorityFromDto(
			EnrollmentDto enrollmentDto, Enrollment enrollment, List<Project> projects) {
		if (enrollment == null) {
			throw new BadRequestException(
					"could not create enrollment, either one of enrollment, event or student is not set");
		}

		var result = new ArrayList<EnrollmentPriority>();
		var priorityCheck = new ArrayList<Integer>();

		for (var p : enrollmentDto.getPriorities()) {
			// 1. check if priority is valid
			var priority = p.getPriority();
			if (priority < 0) throw new BadRequestException("priorities can not be negative");
			if (priorityCheck.contains(priority))
				throw new BadRequestException("priorities can not be dublicated");
			priorityCheck.add(priority);
			Optional<Project> optionalProject =
					StreamEx.of(projects)
							.findFirst(it -> Objects.equals(it.getId(), p.getProjectId()));
			Project project = null;
			if (optionalProject.isPresent()) {
				project = optionalProject.get();
			}
			// 2. add priority to result
			result.add(
					EnrollmentPriority.builder()
							.enrollment(enrollment)
							.project(project)
							.priority(priority)
							.build());
		}

		return result;
	}

	/**
	 * create an Enrollment
	 *
	 * @param event Event
	 * @param student Student
	 * @param finalProject final Project
	 * @return Enrollment
	 */
	public static Enrollment createEnrollment(Event event, Student student, Project finalProject) {
		// TODO: validate/sanitize enrollment

		if (event == null || student == null) {
			throw new BadRequestException(
					"could not create enrollment, either one of event or student is not set");
		}
		var enrollment =
				Enrollment.builder()
						.student(student)
						.event(event)
						.uuidForChange(UUID.randomUUID().toString());
		if (finalProject != null) {
			enrollment.finalProject(finalProject);
		}

		return enrollment.build();
	}
}
