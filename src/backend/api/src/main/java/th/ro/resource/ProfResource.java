// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import io.quarkus.security.UnauthorizedException;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.ParameterIn;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirements;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.security.SecuritySchemes;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import th.ro.common.Jwt;
import th.ro.controller.ProfController;
import th.ro.dto.CreateProfDto;
import th.ro.dto.JwtTokenDto;
import th.ro.dto.ProfDto;

/**
 * Resource class for profs
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Path("/profs")
@Produces(MediaType.APPLICATION_JSON) // not necessary, default value
@Consumes(MediaType.APPLICATION_JSON) // not necessary, default value
@SecuritySchemes(
		value = {
			@SecurityScheme(
					securitySchemeName = "bearer",
					type = SecuritySchemeType.HTTP,
					scheme = "bearer",
					bearerFormat = "jwt"),
			@SecurityScheme(
					securitySchemeName = "basic",
					type = SecuritySchemeType.HTTP,
					scheme = "basic")
		})
@Tag(name = "Prof Resource", description = "Everything that has to do with the Prof")
public class ProfResource {

	@Inject Jwt jwt;

	@Inject ProfController profController;

	/**
	 * Get all profs
	 *
	 * @param auth auth string
	 * @return list of profs
	 */
	@GET
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(description = "List all Profs", summary = "Get all Profs", operationId = "getProfs")
	@APIResponse(
			responseCode = "200",
			description = "Ok",
			content =
					@Content(
							schema =
									@Schema(
											implementation = ProfDto.class,
											type = SchemaType.ARRAY)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	public Response getAllProfs(@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto = profController.listAllProfs();
		return Response.status(200).entity(dto).build();
	}

	/**
	 * Login endpoint
	 *
	 * @param securityContext security context
	 * @return JwtTokenDto
	 */
	@GET
	@Path("/login")
	@SecurityRequirements(@SecurityRequirement(name = "basic"))
	@Operation(description = "Login Prof", summary = "Login Prof", operationId = "loginProf")
	@APIResponse(
			responseCode = "200",
			description = "Ok",
			content = @Content(schema = @Schema(implementation = JwtTokenDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	public JwtTokenDto login(@Context SecurityContext securityContext) {
		return new JwtTokenDto(
				profController.jwtForProf(securityContext.getUserPrincipal().getName()));
	}

	/**
	 * Renew a token
	 *
	 * @param auth auth string
	 * @return JwtTokenDto
	 */
	@GET
	@Path("/renewtoken")
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Renew Token for Prof",
			summary = "Renew Token",
			operationId = "renewToken")
	@APIResponse(
			responseCode = "200",
			description = "Ok",
			content = @Content(schema = @Schema(implementation = JwtTokenDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	public JwtTokenDto renewToken(@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		return new JwtTokenDto(profController.renewToken(auth));
	}

	/**
	 * Save a prof
	 *
	 * @param prof prof
	 * @param auth auth string
	 * @return saved prof
	 */
	@POST
	@Transactional
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Add a new Prof",
			summary = "Create new Prof",
			operationId = "createProf")
	@RequestBody(
			description = "The new Prof",
			required = true,
			content = @Content(schema = @Schema(implementation = CreateProfDto.class)))
	@APIResponse(
			responseCode = "201",
			description = "Created",
			content = @Content(schema = @Schema(implementation = ProfDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "403", description = "No Admin")
	public Response saveProf(CreateProfDto prof, @HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		isAdmin(auth);
		var dto = profController.saveProf(prof);
		return Response.status(201).entity(dto).build();
	}

	/**
	 * Get profs with id of the event
	 *
	 * @param id id of the event
	 * @param auth auth string
	 * @return list of profs
	 */
	@GET
	@Path("/events/{eventId}")
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Get Profs for an Event",
			summary = "Get Profs for Event",
			operationId = "getProfsEvent")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Profs should be listed",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(
			responseCode = "200",
			description = "Ok",
			content =
					@Content(
							schema =
									@Schema(
											implementation = ProfDto.class,
											type = SchemaType.ARRAY)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Event not found")
	public Response getProfWithEventId(
			@PathParam("eventId") Long id, @HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto = profController.listProfsWithEventId(id);
		return Response.status(200).entity(dto).build();
	}

	/**
	 * Add a prof to an event
	 *
	 * @param profId id of the prof
	 * @param eventId id of the event
	 * @param auth auth string
	 * @return prof
	 */
	@PUT
	@Transactional
	@Path("/{profId}/events/{eventId}")
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Add Prof to event",
			summary = "Add Prof to Event",
			operationId = "addProfToEvent")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Prof should be added",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "profId",
			description = "The Id of the Prof that should be added to an event",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(
			responseCode = "200",
			description = "Ok",
			content = @Content(schema = @Schema(implementation = ProfDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Event or Prof not found")
	public Response addProfToEvent(
			@PathParam("profId") Long profId,
			@PathParam("eventId") Long eventId,
			@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto = profController.addProfToEvent(profId, eventId);
		return Response.status(200).entity(dto).build();
	}

	/**
	 * Remove a prof from an event
	 *
	 * @param profId id of the prof
	 * @param eventId id of the event
	 * @param auth auth string
	 * @return no content status
	 */
	@DELETE
	@Transactional
	@Path("/{profId}/events/{eventId}")
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Remove Prof from event",
			summary = "Remove Prof from event",
			operationId = "removeProFromEvent")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Prof should be removed",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "profId",
			description = "The Id of the Prof that should be removed from an event",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(responseCode = "204", description = "Gone")
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Event or Prof not found")
	public Response removeProfFromEvent(
			@PathParam("profId") Long profId,
			@PathParam("eventId") Long eventId,
			@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		profController.removeProfFromEvent(profId, eventId);
		return Response.status(204).build();
	}

	/**
	 * check if token is valid
	 *
	 * @param token token for validation
	 */
	private void isValidToken(String token) {
		if (token == null || !jwt.verify(StringUtils.remove(token, "Bearer"))) {
			throw new UnauthorizedException();
		}
	}

	/**
	 * check if user is an admin
	 *
	 * @param token token for checkup
	 */
	private void isAdmin(String token) {
		if (token == null
				|| !jwt.getData(StringUtils.remove(token, "Bearer"))
						.get("role", String.class)
						.equals("admin")) {
			throw new ForbiddenException();
		}
	}
}
