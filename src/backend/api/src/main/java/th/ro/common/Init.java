// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.common;

import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.logging.Log;
import io.quarkus.runtime.StartupEvent;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import th.ro.entity.Prof;
import th.ro.repository.ProfRepository;

/**
 * Init class for persisting the Admin Prof into the database
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @version %I%, %G%
 */
@ApplicationScoped
public class Init {

	private static final String DEFAULT_USERNAME_PASSWORD = "ShouldBeThisStringIfNotSet";

	@ConfigProperty(name = "prof.name")
	String name;

	@ConfigProperty(name = "prof.password")
	String password;

	@Inject ProfRepository profRepository;

	/**
	 * Initialization method for persisting the Admin Prof into the databse
	 *
	 * @param ev startup event
	 */
	@Transactional
	void onStart(@Observes StartupEvent ev) {
		if (StringUtils.equals(name, DEFAULT_USERNAME_PASSWORD)
				|| StringUtils.equals(password, DEFAULT_USERNAME_PASSWORD)) {
			Log.info("No Default User or Password has been set as an environment variable");
			return;
		}
		Optional<Prof> optionalProf = profRepository.find("name", name).firstResultOptional();
		if (optionalProf.isPresent()) {
			return;
		}
		Prof prof =
				Prof.builder()
						.roles("admin")
						.name(name)
						.password(BcryptUtil.bcryptHash(password))
						.mail("mail@mail.de")
						.build();
		profRepository.persist(prof);
	}
}
