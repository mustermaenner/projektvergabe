// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import javax.enterprise.context.ApplicationScoped;
import th.ro.entity.Enrollment;

/**
 * A repository class for enrollments
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@ApplicationScoped
public class EnrollmentRepository implements PanacheRepository<Enrollment> {

	/**
	 * This method returns an enrollment
	 *
	 * @param studentId id of the student
	 * @param eventId id of the event
	 * @return enrollment
	 */
	public Enrollment findByStudentIdAndEventId(Long studentId, Long eventId) {
		return find(
						"student_id = :studentId and event_id = :eventId",
						Parameters.with("studentId", studentId).and("eventId", eventId))
				.firstResult();
	}
}
