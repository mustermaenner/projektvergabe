// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import io.quarkus.security.UnauthorizedException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.StringUtils;
import th.ro.dto.CreateProjectDto;
import th.ro.dto.ProjectDto;
import th.ro.entity.Event;
import th.ro.entity.Prof;
import th.ro.entity.Project;
import th.ro.repository.EventRepository;
import th.ro.repository.ProjectRepository;

/**
 * Project Controller
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @see th.ro.resource.ProjectResource
 * @version %I%, %G%
 */
@ApplicationScoped
public class ProjectController {

	@Inject ProjectRepository projectRepository;
	@Inject EventRepository eventRepository;

	/**
	 * get all project for an event and check if prof id is null
	 *
	 * @param eventId id of the event
	 * @param profId id of the prof
	 * @return list of ProjectDtos
	 */
	public List<ProjectDto> getAllProjectsForEvent(Long eventId, Long profId) {
		if (profId != null) {
			getEvent(eventId, profId);
		}
		return getAllProjectsForEvent(eventId);
	}

	/**
	 * get all projects for an event and check if password is valid
	 *
	 * @param eventId id of the event
	 * @param password password
	 * @return list of ProjectDtos
	 */
	public List<ProjectDto> getAllProjectsForEvent(Long eventId, String password) {
		Optional<Event> optionalEvent = eventRepository.findByIdOptional(eventId);
		if (optionalEvent.isEmpty()) {
			throw new NotFoundException("Event not found");
		}
		if (!StringUtils.equals(optionalEvent.get().getPassword(), password)) {
			throw new UnauthorizedException("Password is not Correct");
		}
		return getAllProjectsForEvent(eventId);
	}

	/**
	 * get all projects for an event
	 *
	 * @param eventId id of the event
	 * @return list of ProjectDtos
	 */
	private List<ProjectDto> getAllProjectsForEvent(Long eventId) {
		if (eventRepository.findById(eventId) == null) {
			throw new NotFoundException("Event not found");
		}
		List<Project> projects = projectRepository.find("event_id", eventId).list();
		return StreamEx.of(projects)
				.map(
						it ->
								ProjectDto.builder()
										.eventId(it.getEvent().getId())
										.id(it.getId())
										.name(it.getName())
										.description(it.getDescription())
										.customer(it.getCustomer())
										.isWithContract(it.isWithContract())
										.build())
				.select(ProjectDto.class)
				.toList();
	}

	/**
	 * create a project
	 *
	 * @param projectDto ProjectDto
	 * @param profId id of the prof
	 * @return ProjectDto
	 */
	public ProjectDto createProject(CreateProjectDto projectDto, Long profId) {
		Project project = projectRepository.getProjectByName(projectDto);
		if (project != null) {
			throw new ClientErrorException("Name already Exists", Response.Status.CONFLICT);
		}
		Event event = getEvent(projectDto.getEventId(), profId);
		project =
				Project.builder()
						.name(projectDto.getName())
						.description(projectDto.getDescription())
						.event(event)
						.customer(projectDto.getCustomer())
						.isWithContract(projectDto.isWithContract())
						.build();
		projectRepository.persist(project);
		return ProjectDto.convertToProjectDto(projectDto, project.getId());
	}

	/**
	 * delete a project
	 *
	 * @param projectId id of the project
	 * @param profId id of the prof
	 */
	public void deleteProject(Long projectId, Long profId) {
		Project project = projectRepository.findById(projectId);
		if (StreamEx.of(project.getEvent().getProfs())
				.map(Prof::getId)
				.noneMatch(it -> Objects.equals(it, profId))) {
			throw new ForbiddenException("Prof not in Event");
		}
		projectRepository.delete(project);
	}

	/**
	 * update a project
	 *
	 * @param projectIdid of the project
	 * @param projectDto ProjectDto
	 * @param profId id of the prof
	 * @return ProjectDto
	 */
	public ProjectDto updateProject(Long projectId, CreateProjectDto projectDto, Long profId) {
		Project project = projectRepository.find("id", projectId).firstResult();
		if (project == null) {
			throw new NotFoundException("Project not found");
		}
		Event event = getEvent(projectDto.getEventId(), profId);
		project.setName(projectDto.getName());
		project.setEvent(event);
		project.setCustomer(projectDto.getCustomer());
		project.setDescription(projectDto.getDescription());
		project.setWithContract(projectDto.isWithContract());
		projectRepository.persist(project);
		return ProjectDto.convertToProjectDto(projectDto, project.getId());
	}

	/**
	 * get an Event
	 *
	 * @param eventId id of the event
	 * @param profId id of the prof
	 * @return Event
	 */
	private Event getEvent(Long eventId, Long profId) {
		Event event = eventRepository.findById(eventId);
		if (event == null) {
			throw new NotFoundException("Event not found");
		}
		if (StreamEx.of(event.getProfs())
				.map(Prof::getId)
				.noneMatch(it -> Objects.equals(it, profId))) {
			throw new ForbiddenException("Prof not in Event");
		}
		return event;
	}
}
