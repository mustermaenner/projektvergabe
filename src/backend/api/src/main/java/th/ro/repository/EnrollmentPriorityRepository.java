// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import th.ro.entity.EnrollmentPriority;

/**
 * A repository class enrollment priorities
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@ApplicationScoped
public class EnrollmentPriorityRepository implements PanacheRepository<EnrollmentPriority> {

	/**
	 * This method returns a list of enrollment priorities
	 *
	 * @param enrollmentID id of the enrollment
	 * @return list of enrollment priorities
	 */
	public List<EnrollmentPriority> findByEnrollmentId(Long enrollmentID) {
		return this.find("enrollment_id", enrollmentID).list();
	}
}
