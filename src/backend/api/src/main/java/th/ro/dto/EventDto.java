// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import java.util.Date;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

/**
 * DTO for events
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @version %I%, %G%
 */
@Data
@Builder
public class EventDto {
	@NotNull Long id;
	@NotNull String name;
	String description;
	@NotNull Date openUntil;
}
