// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * DTO for events combined with prof
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @version %I%, %G%
 */
@Data
@Builder
@AllArgsConstructor
public class EventProfDto {
	@NotNull Long id;
	@NotNull String name;
	String description;
	@NotNull Date openUntil;

	@NotNull String password;
	List<Long> profs;

	public EventProfDto() {}
}
