// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.common;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.quarkus.logging.Log;
import io.quarkus.runtime.StartupEvent;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import th.ro.entity.JwtSecret;
import th.ro.repository.JwtSecetRepository;

/**
 * Jwt class regarding the JWT secret
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@ApplicationScoped
public class Jwt {

	@ConfigProperty(name = "aes.secret")
	String encyptionKey;

	@Inject JwtSecetRepository jwtSecetRepository;

	private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;
	private static final long DURATION = 60;
	private static String secret;
	private static Key key;

	/**
	 * sign up Prof
	 *
	 * @param content content of Prof
	 * @return JWT serialized string
	 */
	public String sign(Map<String, Object> content) {
		return Jwts.builder()
				.addClaims(content)
				.setIssuedAt(Date.from(Instant.now()))
				.setExpiration(Date.from(Instant.now().plus(DURATION, ChronoUnit.MINUTES)))
				.signWith(key, SIGNATURE_ALGORITHM)
				.compact();
	}

	/**
	 * verify if a token is valid
	 *
	 * @param token token to be verified
	 * @return true or false
	 */
	public boolean verify(String token) {
		token = StringUtils.remove(token, "Bearer ");
		try {
			Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
			return true;
		} catch (JwtException exception) {
			return false;
		}
	}

	/**
	 * get data from token
	 *
	 * @param token jwt token
	 * @return data from token
	 */
	public Claims getData(String token) {
		token = StringUtils.remove(token, "Bearer ");
		return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
	}

	/**
	 * get Jwt secret token
	 *
	 * @return Jwt secret token
	 */
	private String getSecret() {
		Optional<JwtSecret> optionalJwtSecret = jwtSecetRepository.findAll().firstResultOptional();
		String secret;
		if (optionalJwtSecret.isEmpty()) {
			byte[] newKey = new byte[1024];
			byte[] salt = new byte[200];
			try {
				SecureRandom.getInstanceStrong().nextBytes(newKey);
				SecureRandom.getInstanceStrong().nextBytes(salt);
			} catch (NoSuchAlgorithmException e) {
				System.exit(1);
			}
			secret = new String(newKey);
			String encryptedSecret = Aes.encrypt(secret, encyptionKey, new String(salt));
			JwtSecret jwtSecret =
					JwtSecret.builder().secret(encryptedSecret).salt(new String(salt)).build();
			jwtSecetRepository.persist(jwtSecret);
		} else {
			secret =
					Aes.decrypt(
							optionalJwtSecret.get().getSecret(),
							encyptionKey,
							optionalJwtSecret.get().getSalt());
		}
		assert secret != null;
		if (secret.getBytes().length < 512) {
			Log.error("Key is to short");
			System.exit(1);
		}
		return secret;
	}

	/**
	 * method which initialized secret on start
	 *
	 * @param ev startup event
	 */
	@Transactional
	void onStart(@Observes StartupEvent ev) {
		secret = getSecret();
		key =
				new SecretKeySpec(
						Base64.getEncoder().encode(secret.getBytes()),
						SIGNATURE_ALGORITHM.getJcaName());
	}
}
