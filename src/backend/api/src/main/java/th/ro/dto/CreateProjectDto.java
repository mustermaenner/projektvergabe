// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * DTO for creating a project
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Data
@Builder
@AllArgsConstructor
public class CreateProjectDto {
	private Long eventId;
	private String name;
	private String description;
	private String customer;
	private boolean isWithContract;

	public CreateProjectDto() {}
}
