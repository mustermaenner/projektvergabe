// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * DTO for creating an event
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Data
@Builder
@AllArgsConstructor
public class CreateEventDto {
	@NotNull String name;
	String description;
	@NotNull String password;
	@NotNull Date openUntil;
	List<Long> profs;

	public CreateEventDto() {}
}
