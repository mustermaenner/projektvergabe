// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.*;
import lombok.*;

/**
 * Enrollment entity class
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Enrollment {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@Column(unique = true)
	private String uuidForChange;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "student_id")
	@ToString.Exclude
	private Student student;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id")
	@ToString.Exclude
	private Event event;

	private boolean contract;

	private int semester;

	// Project assigned by Prof
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project_id")
	@ToString.Exclude
	private Project finalProject;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "enrollment")
	@ToString.Exclude
	private List<EnrollmentPriority> enrollmentPriorities;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Enrollment that = (Enrollment) o;
		return id == that.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
