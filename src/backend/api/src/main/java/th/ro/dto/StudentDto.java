// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import lombok.Builder;
import lombok.Data;

/**
 * DTO for students
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @version %I%, %G%
 */
@Data
@Builder
public class StudentDto {
	private Long id;

	private String firstName;

	private String lastName;

	private String mail;

	private int matriculationNr;

	private String studentIdentifier;

	private String street;
	private String city;
	private Integer cityCode;
	private String country;
}
