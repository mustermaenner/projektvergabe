// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.ParameterIn;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeIn;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirements;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.security.SecuritySchemes;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import th.ro.controller.StudentController;
import th.ro.dto.EnrollmentDto;

/**
 * Resource class for students
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Path("/events")
@Produces(MediaType.APPLICATION_JSON) // not necessary, default value
@Consumes(MediaType.APPLICATION_JSON) // not necessary, default value
@SecuritySchemes({
	@SecurityScheme(
			securitySchemeName = "token",
			type = SecuritySchemeType.APIKEY,
			apiKeyName = "password",
			in = SecuritySchemeIn.HEADER),
	@SecurityScheme(
			securitySchemeName = "student-token",
			type = SecuritySchemeType.APIKEY,
			apiKeyName = "studentPassword",
			in = SecuritySchemeIn.HEADER)
})
@Tag(
		name = "Student Resource",
		description = "Where the student can Post his enrollment priorities")
public class StudentResource {

	@Inject StudentController studentController;

	/**
	 * Get a student
	 *
	 * @param eventId id of the event
	 * @param studentId id of the student
	 * @param password password
	 * @return student
	 */
	@GET
	@Path("/{eventId}/studentenrollment")
	@SecurityRequirements(value = {@SecurityRequirement(name = "student-token")})
	@Operation(
			description = "Get Enrollment for Student",
			summary = "Get Enrollment",
			operationId = "getEnrollment")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Student wants the Information from",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "studentId",
			description = "The Id of the Student from which the Information is requested",
			required = true,
			in = ParameterIn.QUERY,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(
			responseCode = "200",
			description = "Ok",
			content = @Content(schema = @Schema(implementation = EnrollmentDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Event or Student not Found")
	public Response getStudent(
			@PathParam("eventId") String eventId,
			@QueryParam("studentId") String studentId,
			@HeaderParam("studentPassword") String password) {
		var dto = studentController.findStudentInformation(eventId, studentId, password);
		return Response.status(200).entity(dto).build();
	}

	/**
	 * Create a student
	 *
	 * @param eventId id of the event
	 * @param newStudent new student data
	 * @param password password
	 * @return created student
	 */
	@POST
	@Path("/{eventId}/studentenrollment")
	@SecurityRequirements(value = {@SecurityRequirement(name = "token")})
	@Operation(
			description = "Create Enrollment for Student",
			summary = "Create Enrollment",
			operationId = "createEnrollmentStudent")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Student wants to create the Enrollment",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@RequestBody(
			description = "The Student Information and his project priorities",
			required = true,
			content = @Content(schema = @Schema(implementation = EnrollmentDto.class)))
	@APIResponse(
			responseCode = "201",
			description = "Created",
			content = @Content(schema = @Schema(implementation = EnrollmentDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Event or Project not Found")
	@APIResponse(responseCode = "409", description = "Student already enrolled")
	@Transactional
	public Response createStudent(
			@PathParam("eventId") String eventId,
			@Valid EnrollmentDto newStudent,
			@HeaderParam("password") String password) {
		var dto = studentController.addStudent(newStudent, password);
		return Response.status(201).entity(dto).build();
	}

	/**
	 * Delete an enrollment
	 *
	 * @param eventId id of the event
	 * @param studentId id of the student
	 * @param password password
	 * @return no content status
	 */
	@DELETE
	@Path("/{eventId}/studentenrollment/{studentId}")
	@SecurityRequirements(value = {@SecurityRequirement(name = "student-token")})
	@Operation(
			description = "Delete Enrollment for Student",
			summary = "Delete Enrollment",
			operationId = "deleteEnrollmentStudent")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Student should be deleted from",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "studentId",
			description = "The Id of the Event where the Student should be deleted from",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "studentId",
			description = "The Id of the Student who should be deleted",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(responseCode = "204", description = "Gone")
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Enrollment not Found")
	@Transactional
	public Response deleteEnrollment(
			@PathParam("eventId") Long eventId,
			@PathParam("studentId") Long studentId,
			@HeaderParam("studentPassword") String password) {
		studentController.deleteEnrollment(eventId, studentId, password);
		return Response.status(204).build();
	}
}
