// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import io.quarkus.security.UnauthorizedException;
import java.util.List;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.ParameterIn;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeIn;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirements;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.security.SecuritySchemes;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import th.ro.common.Jwt;
import th.ro.controller.ProjectController;
import th.ro.dto.CreateProjectDto;
import th.ro.dto.ProjectDto;

/**
 * Resource class for projects
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Path("/events")
@Produces(MediaType.APPLICATION_JSON) // not necessary, default value
@Consumes(MediaType.APPLICATION_JSON) // not necessary, default value
@SecuritySchemes(
		value = {
			@SecurityScheme(
					securitySchemeName = "bearer",
					type = SecuritySchemeType.HTTP,
					scheme = "bearer",
					bearerFormat = "jwt"),
			@SecurityScheme(
					securitySchemeName = "token",
					type = SecuritySchemeType.APIKEY,
					apiKeyName = "password",
					in = SecuritySchemeIn.HEADER),
		})
@Tag(name = "Project Resource", description = "Everything that has to do with Projects")
public class ProjectResource {

	@Inject ProjectController projectController;
	@Inject Jwt jwt;

	/**
	 * Get all projects for a specific event
	 *
	 * @param eventId id of the event
	 * @param auth auth string
	 * @param password password
	 * @return list of projects
	 */
	@GET
	@Path("/{eventId}/projects")
	@SecurityRequirements(
			value = {@SecurityRequirement(name = "bearer"), @SecurityRequirement(name = "token")})
	@Operation(
			description = "Get Projects for Event",
			summary = "Get Projects",
			operationId = "getProjects")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Projects are in",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(
			responseCode = "200",
			description = "Ok",
			content =
					@Content(
							schema =
									@Schema(
											implementation = ProjectDto.class,
											type = SchemaType.ARRAY)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Event not Found")
	@APIResponse(responseCode = "403", description = "Forbidden")
	public Response getAllProjectsForEvent(
			@PathParam("eventId") Long eventId,
			@HeaderParam("Authorization") String auth,
			@HeaderParam("password") String password) {
		List<ProjectDto> response = null;
		if (auth != null && jwt.verify(auth)) {
			response = projectController.getAllProjectsForEvent(eventId, getProfIdFromJwt(auth));
		}
		if (response == null) {
			response = projectController.getAllProjectsForEvent(eventId, password);
		}
		return Response.status(200).entity(response).build();
	}

	/**
	 * Create a project
	 *
	 * @param projectDto ProjectDto
	 * @param auth auth string
	 * @return created project
	 */
	@POST
	@Path("/projects")
	@Transactional
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Create Project for Event",
			summary = "Create Project",
			operationId = "CreateProject")
	@RequestBody(
			description = "The new Project",
			required = true,
			content = @Content(schema = @Schema(implementation = CreateProjectDto.class)))
	@APIResponse(
			responseCode = "201",
			description = "Created",
			content = @Content(schema = @Schema(implementation = ProjectDto.class)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Event not Found")
	@APIResponse(responseCode = "409", description = "Project already Exists")
	public Response createProject(
			CreateProjectDto projectDto, @HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto = projectController.createProject(projectDto, getProfIdFromJwt(auth));
		return Response.status(201).entity(dto).build();
	}

	/**
	 * Update a project
	 *
	 * @param projectId id of the project
	 * @param projectDto new data for the project
	 * @param auth auth string
	 * @return updated project
	 */
	@PATCH
	@Path("/projects/{projectId}")
	@Transactional
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Patch Project",
			summary = "Patch Project",
			operationId = "patchProject")
	@Parameter(
			name = "projectId",
			description = "The Id of the Project that should be patched",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(
			responseCode = "200",
			description = "Ok",
			content =
					@Content(
							schema =
									@Schema(
											implementation = ProjectDto.class,
											type = SchemaType.ARRAY)))
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Project not Found")
	public Response updateProject(
			@PathParam("projectId") Long projectId,
			CreateProjectDto projectDto,
			@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto = projectController.updateProject(projectId, projectDto, getProfIdFromJwt(auth));
		return Response.status(200).entity(dto).build();
	}

	/**
	 * Delete a project
	 *
	 * @param projectId id of the project
	 * @param auth auth string
	 * @return no content status
	 */
	@DELETE
	@Path("/projects/{projectId}")
	@Transactional
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Delete a Project",
			summary = "Delete a Project",
			operationId = "deleteProject")
	@Parameter(
			name = "projectId",
			description = "The Id of the Project that should be deleted",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(responseCode = "204", description = "Gone")
	@APIResponse(responseCode = "401", description = "Unauthorized")
	@APIResponse(responseCode = "404", description = "Project not Found")
	public Response deleteProject(
			@PathParam("projectId") Long projectId, @HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		projectController.deleteProject(projectId, getProfIdFromJwt(auth));
		return Response.status(204).build();
	}

	/**
	 * get id of a prof from jwt
	 *
	 * @param token jwt token
	 * @return id of the prof
	 */
	private Long getProfIdFromJwt(String token) {
		return token == null ? null : jwt.getData(token).get("id", Long.class);
	}

	/**
	 * check if token is valid
	 *
	 * @param token token for validation
	 */
	private void isValidToken(String token) {
		if (token == null || !jwt.verify(StringUtils.remove(token, "Bearer"))) {
			throw new UnauthorizedException();
		}
	}
}
