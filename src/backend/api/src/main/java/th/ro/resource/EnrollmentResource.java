// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import io.quarkus.security.UnauthorizedException;
import java.util.List;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.ParameterIn;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirements;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.security.SecuritySchemes;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import th.ro.common.Jwt;
import th.ro.controller.EnrollmentController;
import th.ro.dto.AutoEnrollmentDto;
import th.ro.dto.EnrollmentDto;

/**
 * Resource class for enrollments
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Produces(MediaType.APPLICATION_JSON) // not necessary, default value
@Consumes(MediaType.APPLICATION_JSON) // not necessary, default value
@Path("/events")
@SecuritySchemes(
		value = {
			@SecurityScheme(
					securitySchemeName = "bearer",
					type = SecuritySchemeType.HTTP,
					scheme = "bearer",
					bearerFormat = "jwt"),
		})
@Tag(name = "Enrollment Resource", description = "For the Prof to handle enrollments")
public class EnrollmentResource {

	@Inject EnrollmentController enrollmentController;

	@Inject Jwt jwt;

	/**
	 * Endpoint for returning enrolled students
	 *
	 * @param eventId id of the event
	 * @param repoAssigned is student assigned or not
	 * @param auth auth string
	 * @return list of enrolled students
	 */
	@GET
	@Path("/{eventId}/enrollment")
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "get list of students enrolled into projects",
			summary = "Get list of enrollments",
			operationId = "listEnrollments")
	@Parameter(
			name = "repo_assigned",
			required = true,
			description =
					"If True only Students how have a final Project will be returned.\\\n"
							+ "If false only Students with no final Project will be returned",
			in = ParameterIn.QUERY,
			schema = @Schema(type = SchemaType.BOOLEAN))
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Students should be returned",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(
			responseCode = "200",
			content =
					@Content(
							schema =
									@Schema(
											implementation = EnrollmentDto.class,
											type = SchemaType.ARRAY)))
	@APIResponse(name = "Unauthorized", responseCode = "401", description = "Token invalid")
	@APIResponse(name = "Forbidden", responseCode = "403", description = "No Access to Event")
	@APIResponse(name = "Not found", responseCode = "404", description = "Event not Found")
	public Response getEnrolledStudents(
			@PathParam("eventId") Long eventId,
			@QueryParam("repo_assigned") boolean repoAssigned,
			@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto =
				enrollmentController.getEnrolledStudents(
						eventId, repoAssigned, getProfIdFromJwt(auth));
		return Response.status(200).entity(dto).build();
	}

	/**
	 * Overwrite a list of enrolled students
	 *
	 * @param eventId id of the event
	 * @param studentDtos new data for enrolled students
	 * @param auth auth string
	 * @return overwritten list of enrolled students
	 */
	@POST
	@Path("/{eventId}/enrollment")
	@Transactional
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "overwrite list of students enrolled",
			summary = "overwrite enrollments",
			operationId = "overwriteEnrollments")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Students should be overwritten",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@RequestBody(
			required = true,
			description = "List of Students with enrollments",
			content =
					@Content(
							schema =
									@Schema(
											implementation = EnrollmentDto.class,
											type = SchemaType.ARRAY)))
	@APIResponse(
			responseCode = "200",
			content =
					@Content(
							schema =
									@Schema(
											implementation = EnrollmentDto.class,
											type = SchemaType.ARRAY)))
	@APIResponse(name = "Unauthorized", responseCode = "401", description = "Token invalid")
	@APIResponse(name = "Forbidden", responseCode = "403", description = "No Access to Event")
	@APIResponse(name = "Not found", responseCode = "404", description = "Event not Found")
	public Response overwriteEnrolledStudents(
			@PathParam("eventId") Long eventId,
			@Valid List<EnrollmentDto> studentDtos,
			@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto =
				enrollmentController.overwriteEnrolledStudents(
						studentDtos, eventId, getProfIdFromJwt(auth));
		return Response.status(200).entity(dto).build();
	}

	/**
	 * Enroll a student
	 *
	 * @param eventId id of the event
	 * @param projectId id of the project
	 * @param studentId id of the student
	 * @param auth auth string
	 * @return new enrolled student
	 */
	@PUT
	@Path("/{eventId}/enrollment")
	@Transactional
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Enroll student to project => Student has final Project",
			summary = "enroll student to a project",
			operationId = "enrollStudent")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Student should be enrolled",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "projectId",
			description = "The Id of the Project to witch the Student should be enrolled to",
			required = true,
			in = ParameterIn.QUERY,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "studentId",
			description = "The Id of the Student witch should be enrolled",
			required = true,
			in = ParameterIn.QUERY,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(
			responseCode = "200",
			content = @Content(schema = @Schema(implementation = EnrollmentDto.class)))
	@APIResponse(name = "Unauthorized", responseCode = "401", description = "Token invalid")
	@APIResponse(name = "Forbidden", responseCode = "403", description = "No Access to Event")
	@APIResponse(name = "Not found", responseCode = "404", description = "Event not Found")
	public Response enrollStudent(
			@PathParam("eventId") Long eventId,
			@QueryParam("projectId") Long projectId,
			@QueryParam("studentId") Long studentId,
			@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		var dto =
				enrollmentController.enrollStudent(
						eventId, projectId, studentId, getProfIdFromJwt(auth));
		return Response.status(201).entity(dto).build();
	}

	/**
	 * Delete an enrollment
	 *
	 * @param eventId id of the event
	 * @param studentId id of the student
	 * @param auth auth string
	 * @return no content status
	 */
	@DELETE
	@Path("/{eventId}/enrollment")
	@Transactional
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Remove student from Event",
			summary = "Delete Student from Project",
			operationId = "deEnrollStudent")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Student should be deleted",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "studentId",
			description = "The Id of the Student witch should be deleted from Project",
			required = true,
			in = ParameterIn.QUERY,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(responseCode = "204", description = "Gone")
	@APIResponse(name = "Unauthorized", responseCode = "401", description = "Token invalid")
	@APIResponse(name = "Forbidden", responseCode = "403", description = "No Access to Event")
	@APIResponse(name = "Not found", responseCode = "404", description = "Event not Found")
	public Response deleteEnrollment(
			@PathParam("eventId") Long eventId,
			@QueryParam("studentId") Long studentId,
			@HeaderParam("Authorization") String auth) {
		isValidToken(auth);
		enrollmentController.deleteEnrollment(eventId, studentId, getProfIdFromJwt(auth));
		return Response.status(204).build();
	}

	/**
	 * Trigger the auto enrollment
	 *
	 * @param eventId id of the event
	 * @param auth auth string
	 * @param maxStudentPerProject max count of students per project
	 * @param minStudentPerProject min count of students per project
	 * @return result of the auto enrollment
	 */
	@GET
	@Path("/{eventId}/auto_enrollment")
	@SecurityRequirements(value = {@SecurityRequirement(name = "bearer")})
	@Operation(
			description = "Make a suggestion to how the Students should be enrolled",
			summary = "Auto-enroll Students",
			operationId = "autoEnroll")
	@Parameter(
			name = "eventId",
			description = "The Id of the Event where the Students should be auto-enrolled",
			required = true,
			in = ParameterIn.PATH,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "maxStudentPerProject",
			description = "The maximum amount of persons per Project",
			required = true,
			in = ParameterIn.QUERY,
			schema = @Schema(type = SchemaType.INTEGER))
	@Parameter(
			name = "minStudentPerProject",
			description = "The minimum amount of persons per Project",
			required = true,
			in = ParameterIn.QUERY,
			schema = @Schema(type = SchemaType.INTEGER))
	@APIResponse(
			responseCode = "200",
			content =
					@Content(
							schema =
									@Schema(
											implementation = AutoEnrollmentDto.class,
											type = SchemaType.ARRAY)))
	@APIResponse(name = "Unauthorized", responseCode = "401", description = "Token invalid")
	@APIResponse(name = "Forbidden", responseCode = "403", description = "No Access to Event")
	@APIResponse(name = "Not found", responseCode = "404", description = "Event not Found")
	public Response autoEnrollment(
			@PathParam("eventId") Long eventId,
			@HeaderParam("Authorization") String auth,
			@QueryParam("maxStudentPerProject") int maxStudentPerProject,
			@QueryParam("minStudentPerProject") int minStudentPerProject) {
		isValidToken(auth);
		if (maxStudentPerProject < minStudentPerProject) {
			throw new BadRequestException(
					"maxStudentPerProject has to be bigger than minStudentPerProject");
		}
		List<AutoEnrollmentDto> autoEnroll =
				enrollmentController.autoEnroll(
						eventId,
						getProfIdFromJwt(auth),
						maxStudentPerProject,
						minStudentPerProject);
		return Response.status(Response.Status.OK).entity(autoEnroll).build();
	}

	/**
	 * check if token is valid
	 *
	 * @param token token for validation
	 */
	private void isValidToken(String token) {
		if (token == null || !jwt.verify(StringUtils.remove(token, "Bearer"))) {
			throw new UnauthorizedException();
		}
	}

	/**
	 * get id of a prof from jwt
	 *
	 * @param token jwt token
	 * @return id of the prof
	 */
	private Long getProfIdFromJwt(String token) {
		return jwt.getData(token).get("id", Long.class);
	}
}
