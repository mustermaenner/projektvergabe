// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import th.ro.entity.Event;
import th.ro.entity.Prof;

/**
 * A repository class for events
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@ApplicationScoped
public class EventRepository implements PanacheRepository<Event> {

	@Inject ProfRepository profRepository;

	/**
	 * Finding the event by the name
	 *
	 * @param name name of the event entity
	 * @return event
	 */
	public Event findByName(String name) {
		return find("name", name).firstResult();
	}

	/**
	 * Finding the event by the id
	 *
	 * @param id id of the event entity
	 * @return event
	 */
	public Event findById(Long id) {
		return find("id", id).firstResult();
	}

	/**
	 * Finding all events
	 *
	 * @param open to find open or closed events
	 * @param profID id of the prof
	 * @return list of events
	 */
	public List<Event> findAllEvents(boolean open, Long profID) {
		Date currentDate = new Date();
		Prof prof = profRepository.findById(profID);
		List<Event> allEventsWithMatchingProfID = prof.getEvents();
		List<Event> hiddenEvents = new ArrayList<>();

		for (Event event : allEventsWithMatchingProfID) {
			if (currentDate.compareTo(event.getOpenUntil())
					> 0) { // currentDate is after OpenUntil Date == event is closed
				allEventsWithMatchingProfID.remove(event.getId());
				hiddenEvents.add(event);
			} else {
				hiddenEvents.add(event);
			}
		}

		if (open) {
			return allEventsWithMatchingProfID;
		} else {
			return hiddenEvents;
		}
	}

	// see https://quarkus.io/guides/hibernate-orm-panache#most-useful-operations-2 for more
	// information
}
