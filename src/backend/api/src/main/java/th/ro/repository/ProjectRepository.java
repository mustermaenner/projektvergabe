// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import javax.enterprise.context.ApplicationScoped;
import th.ro.dto.CreateProjectDto;
import th.ro.entity.Project;

/**
 * A repository class for projects
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@ApplicationScoped
public class ProjectRepository implements PanacheRepository<Project> {

	/**
	 * Get a project by his name
	 *
	 * @param projectDto dto for creating a project
	 * @return project
	 */
	public Project getProjectByName(CreateProjectDto projectDto) {
		return find("name", projectDto.getName()).firstResult();
	}
}
