// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import javax.enterprise.context.ApplicationScoped;
import th.ro.entity.JwtSecret;

/**
 * A repository class for the JwtSecret
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@ApplicationScoped
public class JwtSecetRepository implements PanacheRepository<JwtSecret> {}
