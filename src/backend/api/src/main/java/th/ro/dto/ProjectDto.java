// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.dto;

import lombok.Builder;
import lombok.Data;

/**
 * DTO for projects
 *
 * @author Okan Karaoglan
 * @author Alexander Asbeck
 * @author Martin Huber
 * @version %I%, %G%
 */
@Data
@Builder
public class ProjectDto {

	private Long eventId;
	private String name;
	private String description;
	private String customer;
	private Long id;
	private boolean isWithContract;

	public static ProjectDto convertToProjectDto(CreateProjectDto createProjectDto, Long id) {
		return ProjectDto.builder()
				.eventId(createProjectDto.getEventId())
				.name(createProjectDto.getName())
				.customer(createProjectDto.getCustomer())
				.id(id)
				.description(createProjectDto.getDescription())
				.isWithContract(createProjectDto.isWithContract())
				.build();
	}
}
