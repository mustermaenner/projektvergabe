/*
 * Copyright (c) 2012, 2017 Oracle and/or its affiliates. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package th.ro.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

/**
 * A runtime exception indicating a resource requested by a client was {@link
 * Response.Status#CONFLICT not found} on the server.
 *
 * @author Sergey Beryozkin
 * @author Marek Potociar
 * @since 2.0
 */
public class ConflictException extends ClientErrorException {

	/** Construct a new "conflict" exception. */
	public ConflictException() {
		super(Response.Status.CONFLICT);
	}

	/**
	 * Construct a new "conflict" exception.
	 *
	 * @param message the detail message (which is saved for later retrieval by the {@link
	 *     #getMessage()} method).
	 */
	public ConflictException(String message) {
		super(message, Response.Status.CONFLICT);
	}
}
