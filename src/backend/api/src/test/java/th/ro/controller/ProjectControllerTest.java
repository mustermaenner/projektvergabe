// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.dto.CreateProjectDto;
import th.ro.dto.ProjectDto;
import th.ro.entity.Event;
import th.ro.entity.Prof;
import th.ro.entity.Project;
import th.ro.repository.EventRepository;
import th.ro.repository.ProjectRepository;

@QuarkusTest
public class ProjectControllerTest {

	static List<Prof> testProfList = new ArrayList<>();
	static List<Event> testEventList = new ArrayList<>();

	static Prof prof =
			Prof.builder()
					.name("name")
					.id(1L)
					.password("password")
					.mail("mail")
					.roles("testRole")
					.build();

	static Event event =
			Event.builder()
					.id(1L)
					.description("test")
					.name("test")
					.openUntil(new Date())
					.password("password")
					.build();

	static CreateProjectDto createProjectDto =
			CreateProjectDto.builder()
					.name("name")
					.description("test")
					.eventId(1L)
					.customer("testCustomer")
					.build();

	static Project testProject =
			Project.builder()
					.name("name")
					.description("test")
					.id(1L)
					.event(event)
					.customer("testCustomer")
					.isWithContract(true)
					.build();

	@Inject ProjectController projectController;

	@BeforeAll
	public static void setup() {

		testProfList.add(prof);
		testEventList.add(event);
		event.setProfs(testProfList);
		prof.setEvents(testEventList);

		ProjectRepository projectRepository = Mockito.mock(ProjectRepository.class);
		EventRepository eventRepository = Mockito.mock(EventRepository.class);

		Mockito.when(eventRepository.findById(1L)).thenReturn(event);
		Mockito.when(projectRepository.getProjectByName(createProjectDto)).thenReturn(null);

		QuarkusMock.installMockForType(eventRepository, EventRepository.class);
		QuarkusMock.installMockForType(projectRepository, ProjectRepository.class);
	}

	@Test
	public void testCreateProject() {

		ProjectDto expectedProjectDto = ProjectDto.convertToProjectDto(createProjectDto, null);
		ProjectDto actualProjectDto = projectController.createProject(createProjectDto, 1L);

		Assertions.assertEquals(expectedProjectDto, actualProjectDto);
	}
}
