// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import static io.restassured.RestAssured.given;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.common.Jwt;
import th.ro.controller.ProfController;
import th.ro.dto.CreateProfDto;
import th.ro.dto.ProfDto;

@QuarkusTest
@TestHTTPEndpoint(ProfResource.class)
public class ProfResourceTest {

	@BeforeAll
	public static void setup() {
		List<ProfDto> profList = new ArrayList<>();
		CreateProfDto profDto = new CreateProfDto();
		ProfDto prof = ProfDto.builder().build();
		Claims claims = new DefaultClaims();
		claims.put("id", 1);
		claims.put("role", "admin");

		ProfController mock = Mockito.mock(ProfController.class);
		Jwt jwtMock = Mockito.mock(Jwt.class);

		Mockito.when(jwtMock.verify("token")).thenReturn(true);
		Mockito.when(jwtMock.getData("token")).thenReturn(claims);
		Mockito.when(mock.listAllProfs()).thenReturn(profList);
		Mockito.when(mock.saveProf(profDto)).thenReturn(prof);
		Mockito.when(mock.listProfsWithEventId(1L)).thenReturn(profList);
		Mockito.when(mock.addProfToEvent(1L, 1L)).thenReturn(prof);

		QuarkusMock.installMockForType(mock, ProfController.class);
		QuarkusMock.installMockForType(jwtMock, Jwt.class);
	}

	@Test
	public void testGetProfs() {
		given().contentType("application/json")
				.header("Authorization", "token")
				.when()
				.get()
				.then()
				.statusCode(200);
	}

	@Test
	public void testAddProf() {
		given().contentType("application/json")
				.header("Authorization", "token")
				.when()
				.post()
				.then()
				.statusCode(201);
	}

	@Test
	public void testGetProfWithEvent() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.header("Authorization", "token")
				.when()
				.get("/events/{eventId}")
				.then()
				.statusCode(200);
	}

	@Test
	public void testAddProfToEvent() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.pathParam("profId", "1")
				.header("Authorization", "token")
				.when()
				.put("/{profId}/events/{eventId}")
				.then()
				.statusCode(200);
	}

	@Test
	public void testDeleteProf() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.pathParam("profId", "1")
				.header("Authorization", "token")
				.when()
				.delete("/{profId}/events/{eventId}")
				.then()
				.statusCode(204);
	}
}
