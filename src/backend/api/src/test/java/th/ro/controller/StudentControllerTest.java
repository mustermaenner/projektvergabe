// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import java.util.Date;
import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.common.Transform;
import th.ro.dto.EnrollmentDto;
import th.ro.entity.Enrollment;
import th.ro.entity.Event;
import th.ro.entity.Student;
import th.ro.repository.EnrollmentRepository;

@QuarkusTest
public class StudentControllerTest {

	static Student student =
			Student.builder()
					.id(1L)
					.firstName("Test")
					.lastName("Test")
					.city("Teststadt")
					.country("Testland")
					.matriculationNr(123234)
					.mail("testmail@test.com")
					.street("Teststraße")
					.studentIdentifier("test")
					.cityCode(23)
					.build();

	static Event event =
			Event.builder()
					.id(1L)
					.description("test")
					.name("test")
					.openUntil(new Date())
					.password("password")
					.build();
	static Enrollment enrollment =
			Enrollment.builder()
					.id(1L)
					.uuidForChange("password")
					.student(student)
					.event(event)
					.contract(true)
					.semester(7)
					.build();

	@Inject StudentController studentController;

	@BeforeAll
	public static void setup() {
		EnrollmentRepository enrollmentRepository = Mockito.mock(EnrollmentRepository.class);

		Mockito.when(enrollmentRepository.findByStudentIdAndEventId(1L, 1L)).thenReturn(enrollment);
		Mockito.when(enrollmentRepository.findByStudentIdAndEventId(2L, 2L)).thenReturn(null);

		QuarkusMock.installMockForType(enrollmentRepository, EnrollmentRepository.class);
	}

	@Test
	public void testFindStudentInformation() {
		EnrollmentDto testEnrollmentDto = Transform.enrollmentToEnrollmentDto.apply(enrollment);

		EnrollmentDto enrollmentDto =
				studentController.findStudentInformation("1", "1", "password");

		Assertions.assertEquals(testEnrollmentDto, enrollmentDto);
	}

	@Test
	public void testFindStudentInformationWithNotFoundException() {
		Assertions.assertThrows(
				NotFoundException.class,
				() -> {
					studentController.findStudentInformation("2", "2", "password");
				});
	}

	@Test
	public void testFindStudentInformationWithForbiddenException() {
		Assertions.assertThrows(
				ForbiddenException.class,
				() -> {
					studentController.findStudentInformation("1", "1", "wrongPassword");
				});
	}

	@Test
	public void testDeleteEnrollmentNotFoundException() {
		Assertions.assertThrows(
				NotFoundException.class,
				() -> {
					studentController.deleteEnrollment(2L, 2L, "password");
				});
	}

	@Test
	public void testDeleteEnrollmentForbiddenException() {
		Assertions.assertThrows(
				ForbiddenException.class,
				() -> {
					studentController.findStudentInformation("1", "1", "wrongPassword");
				});
	}
}
