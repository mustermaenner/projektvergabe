// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import static io.restassured.RestAssured.given;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.common.Jwt;
import th.ro.controller.ProjectController;
import th.ro.dto.CreateProjectDto;
import th.ro.dto.ProjectDto;

@QuarkusTest
@TestHTTPEndpoint(ProjectResource.class)
public class ProjectResourceTest {

	@BeforeAll
	public static void setup() {
		List<ProjectDto> projectDtoList = new ArrayList<>();
		CreateProjectDto projectDto = new CreateProjectDto();
		Claims claims = new DefaultClaims();
		claims.put("id", 1);

		ProjectController mock = Mockito.mock(ProjectController.class);
		Jwt jwtMock = Mockito.mock(Jwt.class);

		Mockito.when(jwtMock.verify("token")).thenReturn(true);
		Mockito.when(jwtMock.getData("token")).thenReturn(claims);
		Mockito.when(mock.getAllProjectsForEvent(1L, 1L)).thenReturn(projectDtoList);
		Mockito.when(mock.createProject(projectDto, 1L))
				.thenReturn(ProjectDto.convertToProjectDto(projectDto, 1L));
		Mockito.when(mock.updateProject(1L, projectDto, 1L))
				.thenReturn(ProjectDto.convertToProjectDto(projectDto, 1L));

		QuarkusMock.installMockForType(mock, ProjectController.class);
		QuarkusMock.installMockForType(jwtMock, Jwt.class);
	}

	@Test
	public void testGetProjects() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.header("Authorization", "token")
				.when()
				.get("/{eventId}/projects")
				.then()
				.statusCode(200);
	}

	@Test
	public void testAddProject() {
		given().contentType("application/json")
				.header("Authorization", "token")
				.when()
				.post("/projects")
				.then()
				.statusCode(201);
	}

	@Test
	public void testPatchProject() {
		given().contentType("application/json")
				.pathParam("projectId", "1")
				.header("Authorization", "token")
				.when()
				.patch("/projects/{projectId}")
				.then()
				.statusCode(200);
	}

	@Test
	public void testDeleteProject() {
		given().contentType("application/json")
				.pathParam("projectId", "1")
				.header("Authorization", "token")
				.when()
				.delete("/projects/{projectId}")
				.then()
				.statusCode(204);
	}
}
