// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import static io.restassured.RestAssured.given;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.controller.StudentController;
import th.ro.dto.EnrollmentDto;

@QuarkusTest
@TestHTTPEndpoint(StudentResource.class)
public class StudentResourceTest {

	@BeforeAll
	public static void setup() {
		StudentController mock = Mockito.mock(StudentController.class);
		EnrollmentDto enrollmentDto = new EnrollmentDto();

		Mockito.when(mock.findStudentInformation("1", "1", "password")).thenReturn(enrollmentDto);
		Mockito.when(mock.addStudent(enrollmentDto, "password")).thenReturn(enrollmentDto);

		QuarkusMock.installMockForType(mock, StudentController.class);
	}

	@Test
	public void testGetStudents() {
		given().pathParam("eventId", "1")
				.queryParam("studentId", "1")
				.header("studentPassword", "password")
				.when()
				.get("/{eventId}/studentenrollment")
				.then()
				.statusCode(200);
	}

	@Test
	public void testAddStudent() {
		given().pathParam("eventId", "1")
				.header("studentPassword", "password")
				.contentType("application/json")
				.when()
				.post("/{eventId}/studentenrollment")
				.then()
				.statusCode(201);
	}

	@Test
	public void testDeleteStudent() {
		given().pathParam("eventId", "1")
				.pathParam("studentId", "1")
				.header("studentPassword", "password")
				.when()
				.delete("/{eventId}/studentenrollment/{studentId}")
				.then()
				.statusCode(204);
	}
}
