// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import static io.restassured.RestAssured.given;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.common.Jwt;
import th.ro.controller.EventController;
import th.ro.dto.CreateEventDto;
import th.ro.dto.EventDto;
import th.ro.dto.EventProfDto;

@QuarkusTest
@TestHTTPEndpoint(EventResource.class)
public class EventResourceTest {

	@BeforeAll
	public static void setup() {
		List<EventDto> eventDtoList = new ArrayList<>();
		CreateEventDto eventDto = new CreateEventDto();
		List<EventProfDto> eventProfDtoList = new ArrayList<>();
		EventProfDto eventProfDto = new EventProfDto();
		EventController mock = Mockito.mock(EventController.class);
		Jwt jwtMock = Mockito.mock(Jwt.class);
		Claims claims = new DefaultClaims();
		claims.put("id", 1);

		Mockito.when(jwtMock.verify("token")).thenReturn(true);
		Mockito.when(jwtMock.getData("token")).thenReturn(claims);
		Mockito.when(mock.listAllEvents()).thenReturn(eventDtoList);
		Mockito.when(mock.listAllEvents(true, 1L)).thenReturn(eventProfDtoList);
		Mockito.when(mock.saveEvent(eventDto, 1L)).thenReturn(eventProfDto);
		Mockito.when(mock.updateEvent(1L, eventDto, 1L)).thenReturn(eventProfDto);

		QuarkusMock.installMockForType(mock, EventController.class);
		QuarkusMock.installMockForType(jwtMock, Jwt.class);
	}

	@Test
	public void testListEvents() {
		given().contentType("application/json").when().get().then().statusCode(200);
	}

	@Test
	public void testListProfs() {
		given().contentType("application/json")
				.header("Authorization", "token")
				.queryParam("withClosed", true)
				.when()
				.get("/prof")
				.then()
				.statusCode(200);
	}

	@Test
	public void testAddEvent() {
		given().contentType("application/json")
				.header("Authorization", "token")
				.when()
				.post()
				.then()
				.statusCode(201);
	}

	@Test
	public void testUpdateEvent() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.header("Authorization", "token")
				.when()
				.patch("/{eventId}")
				.then()
				.statusCode(200);
	}

	@Test
	public void testDeleteEvent() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.header("Authorization", "token")
				.when()
				.delete("/{eventId}")
				.then()
				.statusCode(204);
	}
}
