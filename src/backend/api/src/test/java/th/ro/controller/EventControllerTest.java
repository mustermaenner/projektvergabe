// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import com.google.common.collect.ImmutableList;
import io.quarkus.test.junit.QuarkusTest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.dto.EventDto;
import th.ro.dto.EventProfDto;
import th.ro.entity.Event;
import th.ro.entity.Prof;
import th.ro.repository.EventRepository;
import th.ro.repository.ProfRepository;

@QuarkusTest
public class EventControllerTest {
	static List<Event> eventList = ImmutableList.of();

	static List<Prof> profList = new ArrayList<>();

	static List<Long> profIdList = new ArrayList<>();

	static Prof prof = Prof.builder().id(1L).build();

	static Event event =
			Event.builder()
					.id(1L)
					.description("test")
					.name("testName")
					.openUntil(new Date())
					.build();

	static EventDto eventDto =
			EventDto.builder()
					.id(1L)
					.description("test")
					.name("testName")
					.openUntil(new Date())
					.build();

	static EventProfDto eventProfDto =
			EventProfDto.builder()
					.id(1L)
					.openUntil(new Date())
					.description("test")
					.name("testName")
					.build();

	@Inject EventController eventController;

	@BeforeEach
	public void setup() {
		EventRepository eventRepository = Mockito.mock(EventRepository.class);
		ProfRepository profRepository = Mockito.mock(ProfRepository.class);

		profList.add(prof);
		event.setProfs(profList);
		eventList = ImmutableList.of(event);
		profIdList.add(prof.getId());

		Mockito.when(eventRepository.listAll()).thenReturn(eventList);

		eventController.setEventRepository(eventRepository);
		eventController.setProfRepository(profRepository);
	}

	@Test
	public void testListAllEventsByEqualOpenUntil() {
		List<EventDto> expectedEventDtoList = new ArrayList<>();
		List<EventDto> actualEventDtoList = eventController.listAllEvents();

		Assertions.assertEquals(expectedEventDtoList, actualEventDtoList);
	}

	@Test
	public void testListAllEventsWithClosed() {
		List<EventProfDto> actualEventProfDtoList = eventController.listAllEvents(true, 1L);
		List<EventProfDto> expectedEventProfDtoList = new ArrayList<>();
		eventProfDto.setProfs(profIdList);
		expectedEventProfDtoList.add(eventProfDto);

		Assertions.assertEquals(expectedEventProfDtoList, actualEventProfDtoList);
	}
}
