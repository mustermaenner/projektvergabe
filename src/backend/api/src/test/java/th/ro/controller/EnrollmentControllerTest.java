// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import io.quarkus.test.junit.QuarkusTest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.entity.*;
import th.ro.repository.EventRepository;

@QuarkusTest
public class EnrollmentControllerTest {
	static Student student =
			Student.builder()
					.id(1L)
					.firstName("firstName")
					.lastName("lastName")
					.mail("mail@mail.com")
					.matriculationNr(123123)
					.studentIdentifier("blub")
					.build();

	static Event event = Event.builder().id(1L).password("pass").openUntil(new Date()).build();

	static Enrollment enrollment =
			Enrollment.builder()
					.id(1L)
					.uuidForChange("1")
					.student(student)
					.contract(true)
					.finalProject(new Project())
					.build();

	static Prof prof =
			Prof.builder().name("test").roles("role").password("pass").mail("mail").build();

	static List<Prof> profList = new ArrayList<>();

	@Inject EnrollmentController enrollmentController;

	@BeforeEach
	public void setup() {
		prof.setId(1L);
		event.setProfs(profList);
		enrollment.setEvent(event);

		EventRepository eventRepository = Mockito.mock(EventRepository.class);
		Mockito.when(eventRepository.findById(1L)).thenReturn(event);
		Mockito.when(eventRepository.findById(2L)).thenReturn(null);
		enrollmentController.setEventRepository(eventRepository);
	}

	@Test
	public void testEnrollStudentProfNotInEvent() {
		Assertions.assertThrows(
				ForbiddenException.class, () -> enrollmentController.enrollStudent(1L, 1L, 1L, 1L));
	}

	@Test
	public void testEnrollStudentEventNotFound() {
		Assertions.assertThrows(
				NotFoundException.class, () -> enrollmentController.enrollStudent(2L, 1L, 1L, 1L));
	}
}
