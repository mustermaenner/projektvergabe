// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.resource;

import static io.restassured.RestAssured.given;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.common.Jwt;
import th.ro.controller.EnrollmentController;
import th.ro.dto.AutoEnrollmentDto;
import th.ro.dto.EnrollmentDto;

@QuarkusTest
@TestHTTPEndpoint(EnrollmentResource.class)
public class EnrollmentResourceTest {

	@BeforeAll
	public static void setup() {
		List<EnrollmentDto> enrollmentDtoList = new ArrayList<>();
		List<AutoEnrollmentDto> autoEnrollmentDtoList = new ArrayList<>();
		EnrollmentDto enrollmentDto = new EnrollmentDto();
		EnrollmentController mock = Mockito.mock(EnrollmentController.class);
		Jwt jwtMock = Mockito.mock(Jwt.class);
		Claims claims = new DefaultClaims();
		claims.put("id", 1);

		Mockito.when(jwtMock.verify("token")).thenReturn(true);
		Mockito.when(jwtMock.getData("token")).thenReturn(claims);
		Mockito.when(mock.getEnrolledStudents(1L, true, 1L)).thenReturn(enrollmentDtoList);
		Mockito.when(mock.overwriteEnrolledStudents(enrollmentDtoList, 1L, 1L))
				.thenReturn(enrollmentDtoList);
		Mockito.when(mock.enrollStudent(1L, 1L, 1L, 1L)).thenReturn(enrollmentDto);
		Mockito.when(mock.autoEnroll(1L, 1L, 5, 1)).thenReturn(autoEnrollmentDtoList);

		QuarkusMock.installMockForType(mock, EnrollmentController.class);
		QuarkusMock.installMockForType(jwtMock, Jwt.class);
	}

	@Test
	public void testGetEnrolledStudents() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.queryParam("repo_assigned", true)
				.header("Authorization", "token")
				.when()
				.get("/{eventId}/enrollment")
				.then()
				.statusCode(200);
	}

	@Test
	public void testOverwriteEnrolledStudents() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.header("Authorization", "token")
				.when()
				.post("/{eventId}/enrollment")
				.then()
				.statusCode(200);
	}

	@Test
	public void testEnrollStudent() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.queryParam("projectId", "1")
				.queryParam("studentId", "1")
				.header("Authorization", "token")
				.when()
				.put("/{eventId}/enrollment")
				.then()
				.statusCode(201);
	}

	@Test
	public void testDeleteStudent() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.queryParam("studentId", "1")
				.header("Authorization", "token")
				.when()
				.delete("/{eventId}/enrollment")
				.then()
				.statusCode(204);
	}

	@Test
	public void testAutoEnrollmentEndpoint() {
		given().contentType("application/json")
				.pathParam("eventId", "1")
				.queryParam("maxStudentPerProject", "5")
				.queryParam("minStudentPerProject", "1")
				.header("Authorization", "token")
				.when()
				.get("/{eventId}/auto_enrollment")
				.then()
				.statusCode(200);
	}
}
