// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package th.ro.controller;

import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import th.ro.dto.ProfDto;
import th.ro.entity.Event;
import th.ro.entity.Prof;
import th.ro.repository.EventRepository;
import th.ro.repository.ProfRepository;

@QuarkusTest
public class ProfControllerTest {

	static ProfDto profDto =
			ProfDto.builder().name("name").id(1L).mail("mail").roles("testRole").build();

	static Prof prof =
			Prof.builder()
					.name("name")
					.id(1L)
					.mail("mail")
					.password("text")
					.roles("testRole")
					.build();

	static Event event =
			Event.builder()
					.id(1L)
					.description("test")
					.name("test")
					.openUntil(new Date())
					.password("password")
					.build();

	@Inject ProfController profController;

	@BeforeEach
	public void setup() {

		List<Prof> profList = new ArrayList<>();
		profList.add(prof);
		event.setProfs(profList);

		EventRepository eventRepository = Mockito.mock(EventRepository.class);
		ProfRepository profRepository = Mockito.mock(ProfRepository.class);

		Mockito.when(eventRepository.findById(1L)).thenReturn(event);
		Mockito.when(profRepository.findById(1L)).thenReturn(prof);

		QuarkusMock.installMockForType(eventRepository, EventRepository.class);
		QuarkusMock.installMockForType(profRepository, ProfRepository.class);
	}

	@Test
	public void testListProfWithEventId() {
		List<ProfDto> expectedProfList = new ArrayList<>();
		expectedProfList.add(profDto);

		List<ProfDto> actualProfList = profController.listProfsWithEventId(1L);

		Assertions.assertEquals(expectedProfList, actualProfList);
	}

	@Test
	public void testAddProfToEvent() {
		Assertions.assertEquals(profDto, profController.addProfToEvent(1L, 1L));
	}
}
