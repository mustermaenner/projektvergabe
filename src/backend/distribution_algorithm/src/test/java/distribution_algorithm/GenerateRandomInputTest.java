// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package distribution_algorithm;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;
import java.util.List;
import one.util.streamex.StreamEx;
import org.apache.commons.math3.util.Pair;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GenerateRandomInputTest {

GenerateRandomInput underTest = new GenerateRandomInput();

Arguments[] test_getRandomProject() {
	return new Arguments[] {
	Arguments.of(
		"Max 5 Projects with Contract, Enough Projects", generateProjects(10, 2), true, 5, 5),
	Arguments.of(
		"Max 10 Projects with Contract, Enough Projects", generateProjects(10, 2), true, 10, 10),
	Arguments.of(
		"Max 5 Projects without Contract, Enough Projects, 2 without Contract",
		generateProjects(10, 2),
		false,
		5,
		2),
	Arguments.of(
		"Max 5 Projects without Contract, Enough Projects, 8 without Contract",
		generateProjects(10, 8),
		false,
		5,
		5),
	Arguments.of(
		"Max 5 Projects with Contract, not Enough Projects", generateProjects(4, 2), true, 5, 4),
	Arguments.of("Max 5 Projects with Contract, no Projects", generateProjects(0, 0), true, 5, 0),
	Arguments.of(
		"Max 5 Projects without Contract, not Enough Projects",
		generateProjects(4, 4),
		false,
		5,
		4),
	Arguments.of(
		"Max 5 Projects without Contract, no Projects", generateProjects(0, 0), false, 5, 0),
	};
}

@ParameterizedTest(name = "{0}")
@MethodSource
void test_getRandomProject(
	String description,
	List<Project> projects,
	boolean isContractAllowed,
	int selectableProjects,
	int actualAmount) {
	// when:
	List<Project> result =
		underTest.getRandomProject(projects, isContractAllowed, selectableProjects);

	// then
	assertEquals(actualAmount, result.size());
	if (!isContractAllowed) {
	for (Project p : result) {
		assertFalse(p.isWithContract());
	}
	}
}

Arguments[] test_generateRandomDataSet() {
	return new Arguments[] {
	Arguments.of("10 Students 5 Projects, Selectable Projects 5", 10, 5, 5, 10),
	Arguments.of("10 Students 0 Projects, Selectable Projects 5", 10, 5, 0, 10),
	Arguments.of("10 Students 10 Projects, Selectable Projects 5", 10, 10, 10, 10),
	Arguments.of("10 Students 5 Projects, Selectable Projects 10", 10, 10, 5, 10),
	Arguments.of("0 Students 10 Projects, Selectable Projects 10", 0, 10, 10, 0),
	};
}

@ParameterizedTest(name = "{0}")
@MethodSource
void test_generateRandomDataSet(
	String description,
	int amountOfStudents,
	int selectableProjects,
	int amountOfProjects,
	int actualAmount) {
	// when:
	Pair<List<Student>, List<Project>> result =
		underTest.generateRandomDataSet(
			amountOfStudents, selectableProjects, amountOfProjects, 0.25, 0.25);

	// then
	assertEquals(actualAmount, result.getFirst().size());
	assertFalse(
		StreamEx.of(result.getFirst())
			.remove(Student::isSigningContract)
			.anyMatch(it -> it.selectedProjects().stream().anyMatch(Project::isWithContract)));
}

List<Project> generateProjects(int howMany, int withoutContract) {
	List<Project> projects = new ArrayList<>();
	for (int i = 0; i < howMany; i++) {
	projects.add(new Project((long) i, i >= withoutContract));
	}
	return projects;
}
}
