// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package distribution_algorithm;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import one.util.streamex.StreamEx;
import org.apache.commons.math3.util.Pair;

/**
 * Class that generates an amount of Students and Projects for Tests only
 *
 * @author Alexander Asbeck
 */
class GenerateRandomInput {

private final Random random = new Random();

public static void main(String[] args) throws TooFewProjectsWithoutContract {
	GenerateRandomInput input = new GenerateRandomInput();
	Pair<List<Student>, List<Project>> studentsAndProjects =
		input.generateRandomDataSet(61, 5, 14, 0.2, 0.13);
	Distribute distribute = new Distribute();
	List<Pair<Project, List<Student>>> output =
		distribute.distributeStudents(
			studentsAndProjects.getFirst(), studentsAndProjects.getSecond(), 5, 4);
	output.forEach(
		it -> {
		StringBuilder builder = new StringBuilder();
		builder.append(it.getFirst()).append(": ");
		it.getSecond()
			.forEach(
				it2 ->
					builder
						.append(it2.studentId())
						.append(" with Prio: ")
						.append(it2.selectedProjects().indexOf(it.getFirst()) + 1)
						.append(";"));
		System.out.println(builder);
		});
}

public Pair<List<Student>, List<Project>> generateRandomDataSet(
	int amountOfStudents,
	int selectableProjects,
	int amountOfProjects,
	double chaneOfNonContractProject,
	double chaneOfStudentWantsNoContract) {
	List<Student> students = new LinkedList<>();
	List<Project> projects = generateProjects(amountOfProjects, chaneOfNonContractProject);
	for (int i = 0; i < amountOfStudents; i++) {
	boolean wantsContract = random.nextDouble() > chaneOfStudentWantsNoContract;
	ImmutableList<Project> studentProject =
		getRandomProject(projects, wantsContract, selectableProjects);
	students.add(new Student(random.nextInt(Integer.MAX_VALUE), wantsContract, studentProject));
	}
	return Pair.create(ImmutableList.copyOf(students), ImmutableList.copyOf(projects));
}

/**
* Generates amountOfProjects Projects
*
* @param amountOfProjects How many Projects
* @param chaneOfNonContractProject The Chance that a Project is without a Contract
* @return n Projects where x% are without a Contract an 1-x% are with a contract
*/
private List<Project> generateProjects(int amountOfProjects, double chaneOfNonContractProject) {
	List<Project> projects = new ArrayList<>();

	for (int i = 0; i < amountOfProjects; i++) {
	projects.add(new Project((long) i, random.nextDouble() > chaneOfNonContractProject));
	}
	return ImmutableList.copyOf(projects);
}

/**
* Gives a Random List with length selectableProjects of Projects from the projects list
*
* @param projects The possible Projects
* @param isContractAllowed are Projects with Contract allowed in the Return
* @param selectableProjects How many Projects can be selected Max
* @return A List with the Selected Projects
*/
@VisibleForTesting
ImmutableList<Project> getRandomProject(
	List<Project> projects, boolean isContractAllowed, int selectableProjects) {
	List<Project> studentProject = new LinkedList<>();
	if (!isContractAllowed) {
	projects = StreamEx.of(projects).remove(Project::isWithContract).toImmutableList();
	}
	for (int j = 0; j < selectableProjects && j < projects.size(); j++) {
	Project project;
	do {
		project = projects.get(random.nextInt(projects.size()));
	} while (studentProject.contains(project));
	studentProject.add(project);
	}
	return ImmutableList.copyOf(studentProject);
}
}
