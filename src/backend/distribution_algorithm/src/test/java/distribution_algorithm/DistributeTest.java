// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package distribution_algorithm;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import one.util.streamex.StreamEx;
import org.apache.commons.math3.util.Pair;
import org.junit.jupiter.api.Test;

class DistributeTest {

GenerateRandomInput input = new GenerateRandomInput();
Distribute distribute = new Distribute();

@Test
void testStudentOnlyInOneProject() throws TooFewProjectsWithoutContract {
	// given:
	Pair<List<Student>, List<Project>> studentsAndProjects =
		input.generateRandomDataSet(61, 5, 14, 0.5, 0.13);
	List<Pair<Project, List<Student>>> output =
		distribute.distributeStudents(
			studentsAndProjects.getFirst(), studentsAndProjects.getSecond(), 5, 4);

	// when
	List<Pair<Project, List<Student>>> result =
		distribute.distributeStudents(
			studentsAndProjects.getFirst(), studentsAndProjects.getSecond(), 5, 4);

	// expected:
	assertEquals(
		0, StreamEx.of(result).map(Pair::getSecond).flatCollection(it -> it).distinct(2).count());
}

@Test
void testNoMoreStudentsThanMax() throws TooFewProjectsWithoutContract {
	// given:
	Pair<List<Student>, List<Project>> studentsAndProjects =
		input.generateRandomDataSet(61, 5, 14, 0.5, 0.13);
	int maxStudentsPerProject = 5;

	// when
	List<Pair<Project, List<Student>>> result =
		distribute.distributeStudents(
			studentsAndProjects.getFirst(),
			studentsAndProjects.getSecond(),
			maxStudentsPerProject,
			4);

	// expected:
	assertEquals(
		0,
		StreamEx.of(result)
			.map(Pair::getSecond)
			.map(List::size)
			.remove(it -> it <= maxStudentsPerProject)
			.count());
}

@Test
void testMoreStudentsThanMin() throws TooFewProjectsWithoutContract {
	// given:
	Pair<List<Student>, List<Project>> studentsAndProjects =
		input.generateRandomDataSet(61, 5, 14, 0.5, 0.13);
	int minStudentsPerProject = 4;

	// when
	List<Pair<Project, List<Student>>> result =
		distribute.distributeStudents(
			studentsAndProjects.getFirst(),
			studentsAndProjects.getSecond(),
			5,
			minStudentsPerProject);

	// expected:
	assertEquals(
		0,
		StreamEx.of(result)
			.map(Pair::getSecond)
			.map(List::size)
			.remove(it -> it >= minStudentsPerProject)
			.count());
}

@Test
void testExceptionIfMoreStudentsWithoutContractThanProjects() {
	// given:
	Pair<List<Student>, List<Project>> studentsAndProjects =
		input.generateRandomDataSet(61, 5, 14, 0, 1);

	// expected:
	assertThrows(
		TooFewProjectsWithoutContract.class,
		() ->
			distribute.distributeStudents(
				studentsAndProjects.getFirst(), studentsAndProjects.getSecond(), 5, 4));
}
}
