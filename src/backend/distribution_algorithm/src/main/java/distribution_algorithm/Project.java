// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package distribution_algorithm;

/**
 * This is a Record for the Projects you can select
 *
 * @param ID id of the Project to Identify later
 * @param isWithContract information if the Project is with a contract
 * @author Alexander Asbeck
 */
public record Project(Long ID, boolean isWithContract) {}
