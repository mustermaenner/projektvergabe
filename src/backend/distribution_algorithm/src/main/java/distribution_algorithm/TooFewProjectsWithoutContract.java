// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package distribution_algorithm;

/**
 * Exception that is thrown if too many Students don't want to sign the contract
 *
 * @author Alexander Asbeck
 */
public class TooFewProjectsWithoutContract extends Throwable {}
