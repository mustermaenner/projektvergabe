// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package distribution_algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import one.util.streamex.StreamEx;
import org.apache.commons.math3.util.Pair;

/**
 * This Class is for Distributing Different Objects
 *
 * @author Alexander Asbeck
 */
public class Distribute {

/**
* This Method Distributes Students Across their chosen Projects
*
* @param students List of Students
* @param maxStudentsPerProject Maximus persons per Project
* @return List with Pairs of Students and IDs
* @throws TooFewProjectsWithoutContract If there are to many Students who don't want to sign a
*     contact
*/
public List<Pair<Project, List<Student>>> distributeStudents(
	List<Student> students,
	List<Project> projects,
	int maxStudentsPerProject,
	int minStudentPerProject)
	throws TooFewProjectsWithoutContract {
	List<ProjectWithStudent> bestDistribution = null;
	int bestDistributionPrio = Integer.MAX_VALUE;
	for (int i = 0; i < 4; i++) {
	List<Project> projectsCopy = new ArrayList<>(projects);
	Collections.shuffle(projectsCopy);
	List<ProjectWithStudent> currentDistribution =
		distributeAll(students, projectsCopy, maxStudentsPerProject, minStudentPerProject);
	int currentDistributionPrio = Math.toIntExact(calculatePrio(currentDistribution));
	if (currentDistributionPrio < bestDistributionPrio) {
		bestDistributionPrio = currentDistributionPrio;
		bestDistribution = currentDistribution;
	}
	}
	assert bestDistribution != null;
	bestDistribution = StreamEx.of(bestDistribution).sortedBy(it -> it.project.ID()).toList();
	return convertToOutput(bestDistribution);
}

/**
* Generates a List of Pairs from a List of {@link ProjectWithStudent}
*
* @param distribution the {@link ProjectWithStudent} that should be converted
* @return The output for the distributeStudents method
*/
private List<Pair<Project, List<Student>>> convertToOutput(
	List<ProjectWithStudent> distribution) {
	List<Pair<Project, List<Student>>> output = new ArrayList<>();
	for (ProjectWithStudent projectWithStudent : distribution) {
	List<Student> students =
		StreamEx.of(projectWithStudent.studentPrioList).map(Pair::getFirst).toList();
	output.add(Pair.create(projectWithStudent.project, students));
	}
	return output;
}

/**
* Calculates the Prio of the given Distribution, so it can be minimized
*
* @param currentDistribution A list of {@link ProjectWithStudent}
* @return the Prio value of the given distribution
*/
private long calculatePrio(List<ProjectWithStudent> currentDistribution) {
	return StreamEx.of(currentDistribution)
		.map(it -> it.studentPrioList)
		.flatCollection(it -> it.stream().toList())
		.map(Pair::getSecond)
		.mapToInt(it -> it)
		.sum();
}

/**
* Distributes the Students to a Project
*
* @param students List with all {@link Student}
* @param projects List with all {@link Project}
* @param maxStudentsPerProject How many Students can be in one Project
* @param minStudentPerProject How many Students have to be at least in a Project
* @return List of {@link ProjectWithStudent}
* @throws TooFewProjectsWithoutContract If there are to many Students who don't want to sign a
*     contact
*/
private List<ProjectWithStudent> distributeAll(
	List<Student> students,
	List<Project> projects,
	int maxStudentsPerProject,
	int minStudentPerProject)
	throws TooFewProjectsWithoutContract {
	List<Student> studentsCopy = new ArrayList<>(students);
	List<Project> projectsCopy = new ArrayList<>(projects);
	int projectsWithoutContract =
		Math.toIntExact(StreamEx.of(projectsCopy).remove(Project::isWithContract).count());
	int studentsWithoutContract =
		Math.toIntExact(StreamEx.of(studentsCopy).remove(Student::isSigningContract).count());
	int avgStudentsPerProject = studentsCopy.size() / projectsCopy.size();
	if ((projectsWithoutContract == 0 && studentsWithoutContract > 0)
		|| (projectsWithoutContract != 0
			&& studentsWithoutContract / projectsWithoutContract > maxStudentsPerProject)) {
	throw new TooFewProjectsWithoutContract();
	}
	while (projectsCopy.size() > 1
		&& StreamEx.of(projectsCopy).remove(Project::isWithContract).findAny().isPresent()
		&& studentsCopy.size() / projectsCopy.size() < minStudentPerProject) {
	projectsCopy = removeProjectWithFewestSelections(projectsCopy, studentsCopy);
	}
	List<ProjectWithStudent> projectWithStudentList =
		transformToProjectWithStudent(studentsCopy, projectsCopy, projectsWithoutContract);
	for (ProjectWithStudent projectWithStudent : projectWithStudentList) {
	projectWithStudent.studentPrioList =
		StreamEx.of(projectWithStudent.studentPrioList).limit(avgStudentsPerProject).toList();
	projectWithStudentList =
		removeStudentsOfOtherProjects(projectWithStudent, projectWithStudentList);
	}
	List<Student> studentsWithoutProject =
		findStudentsWithoutProject(students, projectWithStudentList);
	projectWithStudentList =
		distributeLastStudents(
			projectWithStudentList,
			studentsWithoutProject,
			projectsWithoutContract,
			maxStudentsPerProject,
			minStudentPerProject);
	return projectWithStudentList;
}

/**
* For Distributing the remaining Students
*
* @param projectWithStudentList the List with the current distribution
* @param studentsWithoutProject all Students that don't have a Project yet
* @param projectsWithoutContract amount of Projects without a Contract
* @param maxStudentsPerProject How many Students can be in one Project
* @param minStudentsPerProject How many Students have to be at least in a Project
* @return List where all Students have a Project
*/
private List<ProjectWithStudent> distributeLastStudents(
	List<ProjectWithStudent> projectWithStudentList,
	List<Student> studentsWithoutProject,
	int projectsWithoutContract,
	int maxStudentsPerProject,
	int minStudentsPerProject) {
	if (studentsWithoutProject.isEmpty()) {
	return projectWithStudentList;
	}
	List<ProjectWithStudent> projectWithStudentListCopy =
		StreamEx.of(projectWithStudentList).sortedBy(it -> it.studentPrioList.size()).toList();
	for (Student student : studentsWithoutProject) {
	if (student.isSigningContract()
		&& projectWithStudentListCopy.get(0).studentPrioList.size() < minStudentsPerProject) {
		int prio = projectWithStudentListCopy.size() * projectWithStudentListCopy.size();
		projectWithStudentListCopy.get(0).studentPrioList.add(Pair.create(student, prio));
		projectWithStudentListCopy =
			StreamEx.of(projectWithStudentList).sortedBy(it -> it.studentPrioList.size()).toList();
		continue;
	}
	for (int i = 0; i < student.selectedProjects().size(); i++) {
		Project project = student.selectedProjects().get(i);
		ProjectWithStudent projectWithStudent =
			StreamEx.of(projectWithStudentListCopy)
				.filter(it -> it.project.equals(project))
				.findFirst()
				.orElse(null);
		if (projectWithStudent != null
			&& projectWithStudent.studentPrioList.size() < maxStudentsPerProject) {
		int prio;
		if (student.isSigningContract()) {
			prio = i + projectsWithoutContract;
		} else {
			prio = i;
		}
		prio = prio * prio;
		projectWithStudent.studentPrioList.add(Pair.create(student, prio));
		break;
		}
	}
	}
	return projectWithStudentListCopy;
}

/**
* To Find the Students that haven't been assigned to a Project yet
*
* @param students List of all Students
* @param projectWithStudentList List with assigned Students
* @return List of all Students who haven't been assigned
*/
private List<Student> findStudentsWithoutProject(
	List<Student> students, List<ProjectWithStudent> projectWithStudentList) {
	return StreamEx.of(students)
		.remove(it -> projectWithStudentList.stream().anyMatch(it1 -> it1.containsStudent(it)))
		.toList();
}

/**
* To remove Projects if there are to many for all Students
*
* @param projects List of all Projects
* @param students List of all Students
* @return Reduced List
*/
private List<Project> removeProjectWithFewestSelections(
	List<Project> projects, List<Student> students) {
	List<Project> projectsCopy = new ArrayList<>(projects);
	int minSelection = Integer.MAX_VALUE;
	Project minSelectedProject = null;
	for (Project project : projectsCopy) {
	if (!project.isWithContract()) {
		continue;
	}
	int selected =
		Math.toIntExact(
			StreamEx.of(students).filter(it -> it.selectedProjects().contains(project)).count());
	if (selected < minSelection) {
		minSelection = selected;
		minSelectedProject = project;
	}
	}
	if (minSelectedProject != null) {
	projectsCopy.remove(minSelectedProject);
	}
	return projectsCopy;
}

/**
* To Remove Students assigned to a Project form all other Projects
*
* @param currentProject The Project is has received
* @param allProjects List of all Projects with Students
* @return List where Students have been removed
*/
private List<ProjectWithStudent> removeStudentsOfOtherProjects(
	ProjectWithStudent currentProject, List<ProjectWithStudent> allProjects) {
	List<ProjectWithStudent> allProjectCopy = new ArrayList<>(allProjects);
	int indexOfProject = allProjectCopy.indexOf(currentProject);
	allProjectCopy.remove(currentProject);
	List<Student> studentsToRemove =
		StreamEx.of(currentProject.studentPrioList).map(Pair::getFirst).toList();
	for (ProjectWithStudent projectWithStudent : allProjectCopy) {
	projectWithStudent.studentPrioList =
		StreamEx.of(projectWithStudent.studentPrioList)
			.remove(it -> studentsToRemove.contains(it.getFirst()))
			.toList();
	}
	allProjectCopy.add(indexOfProject, currentProject);
	return allProjectCopy;
}

/**
* Generate a List of {@link ProjectWithStudent} from a List of {@link Project} and a List of
* {@link Student}
*
* @param students Students with Project Prio
* @param projects List of Projects
* @param projectsWithoutContract amount of Projects without a contract
* @return List of {@link ProjectWithStudent} with a Priority List in for each Project
*/
private List<ProjectWithStudent> transformToProjectWithStudent(
	List<Student> students, List<Project> projects, int projectsWithoutContract) {
	List<ProjectWithStudent> projectWithStudentList = new ArrayList<>();
	for (Project project : projects) {
	List<Student> studentsWithProject =
		StreamEx.of(students)
			.filter(it -> it.selectedProjects().contains(project))
			.sortedBy(it -> it.selectedProjects().indexOf(project))
			.toList();
	List<Pair<Student, Integer>> studentPrioList =
		generateListOfStudentPrio(studentsWithProject, projectsWithoutContract, project);
	projectWithStudentList.add(new ProjectWithStudent(project, studentPrioList));
	}
	return projectWithStudentList;
}

/**
* Generate a List of Pair with the Prio and Student for a specific Project
*
* @param students List of Students
* @param projectsWithoutContract the amount of Projects without a contract
* @param project The current Project where the priority should be evaluated
* @return List of Pair with the Prio and Student for a specific Project
*/
private List<Pair<Student, Integer>> generateListOfStudentPrio(
	List<Student> students, int projectsWithoutContract, Project project) {
	List<Student> studentWithoutContract;
	List<Student> studentsWithContract;
	studentWithoutContract = StreamEx.of(students).remove(Student::isSigningContract).toList();
	studentsWithContract = StreamEx.of(students).filter(Student::isSigningContract).toList();
	List<Pair<Student, Integer>> studentPrioList = new ArrayList<>();
	for (Student student : studentWithoutContract) {
	int prio = student.selectedProjects().indexOf(project);
	// Better for distributing
	prio = prio * prio;
	studentPrioList.add(Pair.create(student, prio));
	}
	for (Student student : studentsWithContract) {
	int prio = student.selectedProjects().indexOf(project) + projectsWithoutContract;
	prio = prio * prio;
	studentPrioList.add(Pair.create(student, prio));
	}
	return studentPrioList;
}

/** A Class for keeping Track of the Students in a Project and their Priority */
private static class ProjectWithStudent {
	Project project;
	List<Pair<Student, Integer>> studentPrioList;

	public ProjectWithStudent(Project project, List<Pair<Student, Integer>> studentPrioList) {
	this.project = project;
	this.studentPrioList = studentPrioList;
	}

	/**
	 * If the studentPrioList contains a specific student
	 *
	 * @param student The Student that the list should contain
	 * @return true if Student was found else false
	 */
	boolean containsStudent(Student student) {
	return StreamEx.of(studentPrioList).anyMatch(it -> it.getKey().equals(student));
	}
}
}
