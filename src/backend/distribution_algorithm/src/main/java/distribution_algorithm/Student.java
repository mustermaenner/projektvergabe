// Copyright 2023 The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

package distribution_algorithm;

import com.google.common.collect.ImmutableList;

/**
 * This is a Record for a Student who can select multiple projects
 *
 * @param studentId the id of the Student to Identify later
 * @param isSigningContract information if the Student is willing to sign a contract
 * @param selectedProjects the selected Projects in priority Order => pos 0 is max Prio
 * @author Alexander Asbeck
 */
public record Student(
	long studentId, boolean isSigningContract, ImmutableList<Project> selectedProjects) {}
