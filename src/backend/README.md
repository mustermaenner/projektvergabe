# Backend

The backend is based on two Gradle projects:

- `distribution_algorithm` is dedicated to the algorithm that try to auto enroll students
- The `api` contains the rest-api-server that persist/manage data like enrollments, events, ... in a database.
  It's a [Quarkus](https://quarkus.io/) app. [read more ...](./api/README.md)

## Linting

Code-Style is enforced via spotless.

## Tooling

The backend is targeting java version `graalvm-ce-17`, so if you use other java versions make sure they also work.
