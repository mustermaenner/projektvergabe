# Contribution Guidelines

## Introduction

VSCode/VSCodium pre-configuration is shipped, so you just have to install the recommended add-ons.
Afterward you can just use the launch options.

If you prefer, you can handle [src/frontend](./src/frontend) and [src/backend](./src/backend) as separate projects in your IDE of choice.

## Code Style

Is enforced by linters, you should also have them installed and run locally before pushing code.
If you need more info just read the specific README's of [frontend](./src/frontend/README.md) or [backend](./src/backend/README.md).

## Changing Rest-API

If you need to alter, add or delete backend endpoints, make sure you also update the [OpenAPI Specification](./openapi/index.yaml)

## Copyright

Code that you contribute should use the standard copyright header:

```none
// Copyright <year> The Mustermänner Team. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

```

Files in the repository contain copyright from the year they are added
to the year they are last changed. If the copyright author is changed,
just paste the header below the old one.

## Deployment

In the root folder is a docker-compose, witch can also be used to build the images from source with `docker-compose build`,
shows the default way to spin up the complete project with its subcomponents.
