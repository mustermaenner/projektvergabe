# Projektvergabe

[![License: Apache 2](https://img.shields.io/badge/License-Apache2-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)
[![status-badge](https://ci.codeberg.org/api/badges/mustermaenner/Projektvergabe/status.svg)](https://ci.codeberg.org/mustermaenner/Projektvergabe)

For universities that want to use a website to enroll students for events that contain projects.
It ensures an orderly division of projects, the project are assigned by priority the students choose.

<a href="https://codeberg.org/mustermaenner/Projektvergabe">
 <img alt="Get it on Codeberg" src="https://codeberg.org/Codeberg/GetItOnCodeberg/media/branch/main/get-it-on-neon-blue.png" height="60">
</a>

For **technical details** look at [CONTRIBUTING.md](./CONTRIBUTING.md)
